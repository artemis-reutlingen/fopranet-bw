# Praxisregister

[Praxisregister](https://gitlab.com/artemis-reutlingen/praxisregister)
is a customly developed Customer-Relationship-Management-System (CRMS) 
optimized for collaborative management of practices in the context of 
multi-site and multi-centric research conducted with GPs. Currently it is 
designed especially for the German healthcare system, more specifically for 
the project [FoPraNet-BW](https://www.fopranet-bw.de/). In the future, this 
project may be adapted for more generic scenarios.

## Getting Started

### Prerequisites

This project is built using Python 3.11.2 and Django 4.2.7 It is recommended to use 
Docker to setup this project and its dependencies for development as well as for deployment.

### Setup

- Prerequisites: [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/)
- Create a copy of file "template.env" and rename it to ".env"
- Adapt .env file for your specific setup scenario (take extra care for deployment!)

```
# for specifying the docker network subnet look into the .env file / docker-compose.yml
# default subnet is 172.44.0.0/16
# gateway is 172.44.0.1:8081, which is forwarding to the nginx container

# build images/containers
docker compose build

# run services in detached mode
docker-compose up -d
# migrations should be applied automatically, but you might have to execute
# docker compose up a second time when doing this initially

# access django/web container cli
docker exec -it praxisregister_django bash

# initialize the database with basic data (selectable options, counties, etc.)
python manage.py initialize_db

# argument npractices is an optional positive integer value, specifying the number
# of dummy practices that are created with random data
python manage.py mockup --npractices <no_of_practices>


# site should be running on http://172.44.0.1:8081/
# admin area: http://172.44.0.1:8081/headquarters
# Test users: tester0//x, tester1//x, ...
# Test admin user: admin//x

```

### Development

To simplify the development process, you can configure the project to automatically reload when you make code changes. To do this, set ``DJANGO_DEBUG_MODE=True`` in the .env-file. Then, start the PRM using the alternative development Docker Compose file:
```
docker compose -f docker-compose-dev.yaml up
```

## Contributing

Feedback and input of all forms is highly appreciated. Feel free to contact 
one of the authors or submit issues for feature requests or bugfixes.

## Authors

* **Oliver Bertram** *(initial work)* | oliver.bertram@reutlingen-university.de
* **Tobias Lauxmann** *(Further work)* | tobias.lauxmann@reutlingen-university.de

## License

This project is licensed under the MIT License — see
the [LICENSE.md](LICENSE.md) file for details.
