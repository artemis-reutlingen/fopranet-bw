# pull official base image
FROM python:3.11.2-slim

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV EMAIL_ENABLE 0
ENV EMAIL_USE_TLS 0
ENV EMAIL_USE_SSL 0

# update repository (gettext for Django i18n, libpq-dev & gcc for building postgres)
RUN apt update && apt install -y \
    gettext \
    libpq-dev \
    gcc 

# copy project
COPY . /usr/src

# set work directory
WORKDIR /usr/src/praxisregister_src

# install dependencies
RUN pip install --upgrade pip
RUN pip install psycopg2~=2.9.5
RUN pip install -r requirements.txt

# remove
RUN apt-get autoremove -y gcc

# copy startup script, set perms
COPY scripts/compose_start.sh /usr/src/compose_start.sh
RUN chmod 0755 /usr/src/compose_start.sh

# container entrypoint
CMD "/bin/bash" "/usr/src/compose_start.sh"