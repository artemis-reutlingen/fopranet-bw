import os
import sys
from pathlib import Path

from django.contrib.messages import constants as messages
from django.urls import reverse_lazy

from config.utils import str_to_bool, str_to_list

LAST_SOFTWARE_UPDATE = "Stand: 27.02.2025"
CURRENT_SOFTWARE_VERSION = "0.7.11"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# For Django to find the apps in the custom subfolder "apps"
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

SECRET_KEY = os.environ.get('SECRET_KEY', 'j&2pc+=*+$t&%17^#&o7(==5d$)d2s(9yq(ma^3#21xwps(29!')
DEBUG = str_to_bool(os.environ.get('DEBUG', 'True'))
ALLOWED_HOSTS = ['localhost', 'web', '127.0.0.1']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'users.apps.UsersConfig',
    'practices.apps.PracticesConfig',
    'institutes.apps.InstitutesConfig',
    'studies.apps.StudiesConfig',
    'events.apps.EventsConfig',
    'todos.apps.TodosConfig',
    'processes.apps.ProcessesConfig',
    'addresses.apps.AddressesConfig',
    'persons.apps.PersonsConfig',
    'contactnotes.apps.ContactnotesConfig',
    'exporter.apps.ExporterConfig',
    'accreditation.apps.AccreditationConfig',
    'contacttimes.apps.ContacttimesConfig',
    'documents.apps.DocumentsConfig',
    'mail.apps.MailConfig',
    'importer.apps.ImporterConfig',
    'track_changes.apps.TrackChangesConfig',
    'practice_importer.apps.PracticeImporterConfig',
    'feedback.apps.FeedbackConfig',
    'kbv_importer.apps.KbvImporterConfig',
    'crispy_forms',
    'crispy_bootstrap5',
    'django_filters',
    'taggit',
    'taggit_selectize',
    'guardian',
    'bootstrap5',
    'fontawesomefree',
    'django_tables2',
    'django_property_filter',
    'import_export',
    'utils',
    'changelogs',
    'django_ckeditor_5',
    'schedule',
    # 'django_extensions', only for local use, allows creating ER diagram
]

INTERNAL_IPS = [
    '127.0.0.1',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'login_required.middleware.LoginRequiredMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'utils.context_processors.last_software_update'
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('SQL_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': os.environ.get('SQL_DATABASE', os.path.join(BASE_DIR, 'db.sqlite3')),
        'USER': os.environ.get('SQL_USER', 'user'),
        'PASSWORD': os.environ.get('SQL_PASSWORD', 'password'),
        'HOST': os.environ.get('SQL_HOST', 'localhost'),
        'PORT': os.environ.get('SQL_PORT', '5432'),
        'ATOMIC_REQUESTS': True,
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'users.User'

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

# LANGUAGE_CODE = 'en-us'
LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale')]
LANGUAGE_CODE = 'de-de'
TIME_ZONE = 'Europe/Berlin'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = 'static/'
STATIC_ROOT = 'static-files/'

CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"
CRISPY_TEMPLATE_PACK = "bootstrap5"

# MESSAGES STUFF
MESSAGE_TAGS = {
    messages.DEBUG: 'info',
    messages.INFO: 'info',
    messages.SUCCESS: 'success',
    messages.WARNING: 'warning',
    messages.ERROR: 'danger',
}

LOGIN_URL = reverse_lazy('users:login')

LOGIN_REQUIRED_IGNORE_PATHS = [
    r'/users/who-are-you/$',
    r'/headquarters/'
]

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

TAGGIT_TAGS_FROM_STRING = 'taggit_selectize.utils.parse_tags'
TAGGIT_STRING_FROM_TAGS = 'taggit_selectize.utils.join_tags'

TAGGIT_SELECTIZE = {
    'MINIMUM_QUERY_LENGTH': 2,
    'RECOMMENDATION_LIMIT': 10,
    'CSS_FILENAMES': ("taggit_selectize/css/selectize.django.css",),
    'JS_FILENAMES': ("taggit_selectize/js/selectize.js",),
    'DIACRITICS': True,
    'CREATE': True,
    'PERSIST': True,
    'OPEN_ON_FOCUS': True,
    'HIDE_SELECTED': True,
    'CLOSE_AFTER_SELECT': False,
    'LOAD_THROTTLE': 300,
    'PRELOAD': False,
    'ADD_PRECEDENCE': False,
    'SELECT_ON_TAB': False,
    'REMOVE_BUTTON': True,
    'RESTORE_ON_BACKSPACE': False,
    'DRAG_DROP': False,
    'DELIMITER': ','
}

CSRF_COOKIE_SECURE = str_to_bool(os.environ.get('CSRF_COOKIE_SECURE', 'False'))
CSRF_TRUSTED_ORIGINS = [os.environ.get("DOMAIN", "http://localhost"), "http://localhost:8081", "http://127.0.0.1:8081", "http://172.44.0.1:8081",
                        "https://prbw-staging.reutlingen-university.de", "https://praxisregister.forschungspraxennetz-bw.de",]
SESSION_COOKIE_SECURE = str_to_bool(os.environ.get('SESSION_COOKIE_SECURE', 'False'))

SESSION_COOKIE_AGE = 60 * 30  # logout after 30 minutes of inactivity
SESSION_SAVE_EVERY_REQUEST = True  # extend session on activity

# django-guardian
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # this is default
    'guardian.backends.ObjectPermissionBackend',
)
GUARDIAN_RENDER_403 = True

IMPORT_WITH_DJANGO_GUARDIAN = True

# tests with larger number of fields failed with the default value of 1000
# ProcessInstantiate view was raising TooManyFieldsSent exception
DATA_UPLOAD_MAX_NUMBER_FIELDS = 2 ** 16

# Media files (user uploaded images)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

UPLOAD_DIRECTORY = 'user-uploads'

# CKEditor
CKEDITOR_5_UPLOAD_PATH = os.path.join(MEDIA_ROOT, os.path.join(UPLOAD_DIRECTORY, "ckeditor-uploads"))
CKEDITOR_5_UPLOAD_URL = "/media/user-uploads/ckeditor-uploads/"
CKEDITOR_5_FILE_STORAGE = "mail.storage.CustomStorage"

customColorPalette = [
        {
            'color': 'hsl(4, 90%, 58%)',
            'label': 'Red'
        },
        {
            'color': 'hsl(340, 82%, 52%)',
            'label': 'Pink'
        },
        {
            'color': 'hsl(291, 64%, 42%)',
            'label': 'Purple'
        },
        {
            'color': 'hsl(262, 52%, 47%)',
            'label': 'Deep Purple'
        },
        {
            'color': 'hsl(231, 48%, 48%)',
            'label': 'Indigo'
        },
        {
            'color': 'hsl(207, 90%, 54%)',
            'label': 'Blue'
        },
    ]

CKEDITOR_5_CONFIGS = {
    'default': {
        'toolbar': ['heading', '|', 'bold', 'italic', 'link',
                    'bulletedList', 'numberedList', 'blockQuote', 'imageUpload', ],

    },
    'extends': {
        'blockToolbar': [
            'paragraph', 'heading1', 'heading2', 'heading3',
            '|',
            'bulletedList', 'numberedList',
            '|',
            'blockQuote',
        ],
        'toolbar': ['heading', '|', 'outdent', 'indent', '|',
                     'bold', 'italic', 'link', 'underline', 'strikethrough', 'code','subscript', 'superscript', 'highlight', '|', 
                     'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor','|' , 
                     'bulletedList', 'numberedList', 'todoList', 'insertTable', '|',  
                     'insertImage', 'blockQuote', 'removeFormat',
                    ],
        "image": {
            "toolbar": [
                "imageTextAlternative",
                "|",
                "imageStyle:alignLeft",
                "imageStyle:alignRight",
                "imageStyle:alignCenter",
                "imageStyle:side",
                "|",
                "toggleImageCaption",
                "|"
            ],
            "styles": [
                "full",
                "side",
                "alignLeft",
                "alignRight",
                "alignCenter",
            ],
        },
        'table': {
            'contentToolbar': [ 'tableColumn', 'tableRow', 'mergeTableCells',
            'tableProperties', 'tableCellProperties' ],
            'tableProperties': {
                'borderColors': customColorPalette,
                'backgroundColors': customColorPalette
            },
            'tableCellProperties': {
                'borderColors': customColorPalette,
                'backgroundColors': customColorPalette
            }
        },
        'heading' : {
            'options': [
                { 'model': 'paragraph', 'title': 'Paragraph', 'class': 'ck-heading_paragraph' },
                { 'model': 'heading1', 'view': 'h1', 'title': 'Heading 1', 'class': 'ck-heading_heading1' },
                { 'model': 'heading2', 'view': 'h2', 'title': 'Heading 2', 'class': 'ck-heading_heading2' },
                { 'model': 'heading3', 'view': 'h3', 'title': 'Heading 3', 'class': 'ck-heading_heading3' }
            ]
        }
    },
    'list': {
        'properties': {
            'styles': 'true',
            'startIndex': 'true',
            'reversed': 'true',
        }
    }
}

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_RENDERER_CLASSES': (
        # 'rest_framework.renderers.JSONRenderer',
        # 'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework_datatables.filters.DatatablesFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_datatables.pagination.DatatablesPageNumberPagination',
    'PAGE_SIZE': 50,
}

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_ENABLE = str_to_bool(os.environ.get('EMAIL_ENABLE', "False"))
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
EMAIL_PORT = os.environ.get('EMAIL_PORT', 2525)
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
EMAIL_USE_TLS = str_to_bool(os.environ.get('EMAIL_USE_TLS', "False"))
EMAIL_USER_SSL = str_to_bool(os.environ.get('EMAIL_USE_SSL', "False"))
EMAIL_SENDERS = str_to_list(os.environ.get('EMAIL_SENDERS'), separator = ";")

MAX_EMAIL_ATTACHMENTS_SIZE = 20 * 1024 * 1024  # 20 MB

#Logging 

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler'
        },
        'general_file': {
            'class': 'logging.FileHandler',
            'filename': 'general.log',
            'formatter': 'verbose'
        },
        'user_activity_file': {
            'class': 'logging.FileHandler',
            'filename': 'users.log',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'root': {
            'handlers': ['console', 'general_file'],
            'level': os.environ.get('DJANGO_LOG_LEVEL', 'INFO')
        },
        'user_activity': {
            'handlers' : ['user_activity_file'],
            'level': os.environ.get('DJANGO_LOG_LEVEL', 'INFO')
        },

    },
    'formatters': {
        'verbose': {
            'format': '{asctime} ({levelname}) - {name} - {message}',
            'style': '{'
        }
    }
}

#Debug mode
if DEBUG:
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]
    INSTALLED_APPS += [
        'debug_toolbar',
    ]
    INTERNAL_IPS = ['127.0.0.1', ]