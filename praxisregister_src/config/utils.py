def str_to_numeric_bool(value: str) -> int:
    value = value.lower()
    if value in ('y', 'yes', 't', 'true', 'on', '1'):
        return 1
    elif value in ('n', 'no', 'f', 'false', 'off', '0'):
        return 0
    else:
        raise ValueError("invalid truth value %r" % (value,))

def str_to_bool(value: str) -> bool:
    return bool(str_to_numeric_bool(value))

def str_to_list(value: str, separator: str = ","):
    if not value:
        return []
    return value.split(separator)
