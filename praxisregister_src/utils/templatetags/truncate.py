from django import template
import re

register = template.Library()

def truncate(line: str, num_characters: int) -> str:
    if len(line) > num_characters:
        return line[:num_characters] + "..."
    else:
        return line
    
@register.filter("truncatemultiline", is_safe = False)
def trunctemultiline(value: str, num_characters: int):
    if not isinstance(value, str):
        value = str(value)

    lines = re.split("\n| ", value)
    print(lines)
    lines = [truncate(line, num_characters) for line in lines]
    return "\n".join(lines)
