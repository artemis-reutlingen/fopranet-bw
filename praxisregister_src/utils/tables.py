from django_tables2 import CheckBoxColumn

class CheckBoxColumnWithTitle(CheckBoxColumn):
    @property
    def header(self):
        return self.verbose_name