
from django.http import Http404
from django.utils.translation import gettext_lazy as _

from django.http import JsonResponse
from django_ckeditor_5.forms import UploadFileForm
from django_ckeditor_5.views import image_verify, handle_uploaded_file, NoImageException
from django.contrib.auth.decorators import login_required
import logging

logger = logging.getLogger(__name__)
#The original code of the libray does not allow users without staff permission to upload images.
#We have to change the code to allow all users to upload images.
@login_required
def upload_file(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        try:
            image_verify(request.FILES["upload"])
        except NoImageException as ex:
            return JsonResponse({"error": {"message": f"{ex}"}})
        if form.is_valid():
            url = handle_uploaded_file(request.FILES["upload"])
            logger.info(f"Image uploaded: {url}")
            return JsonResponse({"url": url})
    raise Http404(_("Page not found."))