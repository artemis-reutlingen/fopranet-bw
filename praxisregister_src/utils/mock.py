import random

from faker import Faker

from addresses.models import County
from events.models import EventInfo, DateModel
from institutes.models import Institute
from persons.models import Person
from persons.types import GenderType
from practices.models import Practice, Site
from processes.models import ProcessDefinition
from studies.models import Study
from users.models import User
from practices.types import SiteType


def initialize_mockup_data(number_of_institutes: int, number_of_practices: int, number_of_test_users: int,
                           verbose: bool = False):
    """
    Extensive mockup utility function.
    Dummy data initialization for Counties, Institutes, Practices, Users, Studies, Events, Processes.
    Based on faker package.
    """
    fake = Faker('de_DE')

    def _create_institute():
        organization_name = fake.company()
        department_name = random.choice(
            [
                "Abt. Versorgungsforschung",
                "Allgemeinmedizin",
                "Fakultät für Allgemeinmedizin",
            ]
        )
        group_name = f"{organization_name}_{department_name}"
        Institute.objects.create_institute_with_group(
            organisation=organization_name,
            name=department_name,
            group_name=group_name
        )

    def _create_person_for_practice(practice_obj):
        return Person.objects.create(
            practice=practice_obj,
            title=random.choice(["Prof. Dr.", "Dr.", "Dr. med.", ""]),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            gender=random.choice([GenderType.MALE, GenderType.FEMALE, GenderType.DIVERSE]),
            year_of_birth=random.randint(1970, 1990),
            former_name=fake.last_name(),
            email=fake.ascii_email(),
            phone=fake.phone_number(),
        )

    def _create_practice():
        practice_name = f"{random.choice(practice_prefixes)} {fake.company()}"
        practice_ = Practice.objects.create(
            name=practice_name,
            assigned_institute=random.choice(Institute.objects.all())
        )

        practice_.assign_perms(practice_.assigned_institute)

        practice_.save()

        practice_.street = fake.street_name()
        practice_.house_number = fake.building_number()
        practice_.zip_code = fake.postcode()
        practice_.city = fake.city()
        practice_.county = random.choice(County.objects.all())
        practice_.save()

        practice_.contact_website = fake.url()
        practice_.contact_email = fake.ascii_email()
        practice_.identification_number = str(random.randint(123456, 999999))
        practice_.type = SiteType.PRIMARY
        practice_.save()
        return practice_

    def _create_user(fake_obj, username_, password_):
        u = User.objects.create(username=username_)
        u.set_password(password_)
        u.first_name = fake_obj.first_name()
        u.last_name = fake_obj.last_name()
        random_institute = random.choice(Institute.objects.all())
        u.assigned_institute = random_institute
        u.groups.add(random_institute.group)
        u.medical_title = "Dr."
        u.phone_number = fake_obj.phone_number()
        u.email = fake_obj.ascii_email()
        u.save()

    # create some institutes (with groups)
    if verbose:
        print(f"Creating {number_of_institutes} random institutes...")
    for institute_index in range(0, number_of_institutes):
        _create_institute()
    if verbose:
        print(f"List of created institutes:")
        for institute in Institute.objects.all():
            print(f"- {institute}")

    # create practices with some persons
    if verbose:
        print(f"Creating {number_of_practices} practices for random institutes ...")
    practice_prefixes = ['Praxis', 'Landarzt', 'Gesundheitszentrum', 'Allgemeinmedizinische Praxis']
    for _ in range(0, number_of_practices):
        practice = _create_practice()
        for _ in range(5):
            p = _create_person_for_practice(practice)
            p.assign_perms(practice.assigned_institute)
            if _ == 0:
                p.make_owner()
                p.make_doctor()
            else:
                random.choice([p.make_assistant, p.make_doctor])()  # randomly select callable and execute
        practice.save()

    # admin user
    if verbose:
        print("Creating super user ...")
    admin_user = User.objects.create_superuser(username="admin")
    admin_user.set_password("x")
    admin_user.save()

    # create some users
    if verbose:
        print(f"Creating {number_of_test_users} regular users ...")
    for user_index in range(0, number_of_test_users):
        _create_user(fake_obj=fake, username_=f"tester{user_index}", password_="x")

    # create some studies
    if verbose:
        print(f"Creating some studies ...")
    for _ in range(5):
        institute = random.choice(Institute.objects.all())
        study = Study.objects.create(
            title=f"Studying colors: {fake.color_name()}",
            start_date="2012-12-05",
            end_date="2015-12-05",
            assigned_institute=institute,
            description=fake.paragraph(nb_sentences=25),
        )
        study.assign_editor_perms_to_institute(institute)

    # create some process definitions
    if verbose:
        print(f"Creating some process definitions ...")
    for _ in range(100):
        institute = random.choice(Institute.objects.all())
        author = random.choice(User.objects.get_users_for_institute(institute=institute))

        process_definition = ProcessDefinition.objects.create_process_definition_for_user(
            title=fake.text(max_nb_chars=20),
            user=author,
        )
        process_definition.description = fake.paragraph(nb_sentences=25)

        no_of_steps = random.randint(3, 25)
        for __ in range(no_of_steps):
            process_definition.add_step_definition(
                step_description=fake.paragraph(nb_sentences=5),
                step_name=fake.text(max_nb_chars=20),
            )
        process_definition.is_draft = False
        process_definition.save()

        process_definition.assign_perms_for_author_and_institute(author=author)

    # create some events
    if verbose:
        print(f"Creating some events ...")
    for _ in range(50):
        institute = random.choice(Institute.objects.all())
        author = random.choice(User.objects.get_users_for_institute(institute=institute))

        event = EventInfo.objects.create(
            description=fake.paragraph(nb_sentences=25),
            title=fake.text(max_nb_chars=20),
            assigned_institute=institute,
            author=author,
        )
        date = DateModel.objects.create(
            date="2012-12-05",
            start_time="10:00",
            end_time="12:00",
            event=event,
        )

        event.assign_perms(user=author)

    if verbose:
        print("List of created users (default password is 'x'):")
        for user in User.objects.all():
            print(f"- {user.username}")
