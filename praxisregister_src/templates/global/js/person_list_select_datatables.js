// Populating the datatable via REST endpoint
$(document).ready(function () {
    let table = $('#persons_table').DataTable({
        columnDefs: [
            {
                // set first column to show checkboxes
                targets: 0,
                data: null,
                defaultContent: '',
                orderable: false,
                className: 'select-checkbox'
            },
            {
                // hide id column, it's just used to handle hidden_inputs of django form
                targets: 1,
                visible: false
            }
        ],
        select: {
            style: "multi",
            selector: 'td:first-child'
        },
        dom: 'Blfrtip',
        buttons: [
            'selectAll',
            'selectNone',
        ],
        language: {
            buttons: {
                selectAll: "Select all items",
                selectNone: "Select none"
            }
        },
        "serverSide": false,
        "ajax": "/api/persons/?format=datatables",
        "columns": [
            {"data": null}, // checkbox column
            {"data": 'id'}, // hidden
            {"data": "title"},
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "former_name"},
            {"data": "roles"},
            {"data": "practice.name", "name": "practice.name", defaultContent: "-"},
            {
                "data": "practice.assigned_institute.full_name",
                "full_name": "practice.assigned_institute.full_name",
                defaultContent: "-"
            },

        ]
    });

    table.on('buttons-action', function (e, buttonApi, dataTable, node, config) {
        // Differentiating what action to take for our hidden input fields (Select all / Select none)
        // ... by substring matching the classname of the datatables buttons ...
        // But: there MUST be a better way! Good luck :)

        if (node[0].className.indexOf("buttons-select-all") >= 0) {
            // setting disabled = false for all hidden inputs "selecting all"
            $('input:hidden[name="persons"]').each(function () {
                $(this).prop('disabled', null);
            })
            table.$('.checkbox1').prop('checked', this.checked);

        }
        if (node[0].className.indexOf("buttons-select-none") >= 0) {
            // setting disabled = true for all hidden inputs "deselecting all"
            $('input:hidden[name="persons"]').each(function () {
                $(this).prop('disabled', true);
            })
        }
    });

    // manipulating Django Form hidden inputs when selecting/deselecting single rows
    table.on('select', function (e, dt, type, indexes) {
        if (type === 'row') {
            let id = table.rows(indexes).data()[0]['id'];
            // necessary to go by value == id, not the id of the input field
            $('input:hidden[value=' + id + ']').prop('disabled', null);
        }
    });
    table.on('deselect', function (e, dt, type, indexes) {
        if (type === 'row') {
            let id = table.rows(indexes).data()[0]['id'];
            // necessary to go by value == id, not the id of the input field
            $('input:hidden[value=' + id + ']').prop('disabled', true);
        }
    });
});
