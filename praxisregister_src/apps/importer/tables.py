from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_tables2 import Column, Table
from importer.models import DataImport, ImportedModel, Mapper, MapperField


class MapperTable(Table):
    
    mapper_fields = Column(verbose_name=_('Mapper fields'), orderable=False, empty_values=())
    delete_mapper = Column(verbose_name=_('Delete mapper'), orderable=False, empty_values=())

    # Returns html for mapper fields with fontawsome icon
    def render_mapper_fields(self, record):
        return mark_safe(f'<a href="{reverse_lazy("importer:mapper-fields", kwargs={"pk": record.pk})}"><i class="fas fa-edit"></i></a>')
    
    # Returns html for delete mapper with fontawsome icon
    def render_delete_mapper(self, record):
        return mark_safe(f'<a href="{reverse_lazy("importer:mapper-delete", kwargs={"pk": record.pk})}"><i class="fas fa-trash"></i></a>')

    class Meta:
        model = Mapper
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("name", "content_type", "mapper_fields")

class MapperFieldTable(Table):
    
    delete_mapper_field = Column(verbose_name=_('Delete field'), orderable=False, empty_values=())

    def render_delete_mapper_field(self, record):
        return mark_safe(f'<a href="{reverse_lazy("importer:mapper-field-delete", kwargs={"pk": record.pk, "mapper_id": record.mapper.pk})}"><i class="fas fa-trash"></i></a>')

    class Meta:
        model = MapperField
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("import_fieldname", "internal_fieldname", "delete_mapper_field")

class ImportHistoryTable(Table):
    
    details = Column(verbose_name=_('Details'), orderable=False, empty_values=())

    def render_details(self, record):
        return mark_safe(f'<a href="{reverse_lazy("importer:import-history-detail", kwargs={"pk": record.pk})}"><i class="fas fa-eye"></i></a>') 

    class Meta:
        model = DataImport
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("imported_at", "imported_by", "success", "details")

class ImportHistoryDetailTable(Table):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set verbose name for changed model
        self.base_columns['changed_model'].verbose_name = kwargs["data"].first().model_name

    changed_model = Column(verbose_name=_('Changed model'), orderable=False, empty_values=())

    def render_diff(self, value, record):
        return mark_safe(value)
    
    def render_changed_model(self, value, record):
        return mark_safe(f'<a href="{reverse_lazy("importer:import-history-model", kwargs={"pk": record.pk})}">{record.imported_resource if record.imported_resource else record.model_name}</a>')
    
    class Meta:
        model = ImportedModel
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("import_type", "diff")

class ImportHistoryModelTable(Table):

    def render_diff(self, value, record):
        return mark_safe(value)
    
    class Meta:
        model = ImportedModel
        template_name = "django_tables2/bootstrap5.html"
        fields = ("import_type", "diff", "data_import__imported_at", "data_import__imported_by")