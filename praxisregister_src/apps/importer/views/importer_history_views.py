from typing import Any
import ast

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse
from django.utils import timezone
from django.views.generic import ListView
from django_tables2 import SingleTableMixin
from importer.forms import *
from importer.models import DataImport, ImportedModel
from importer.tables import (ImportHistoryDetailTable, ImportHistoryModelTable,
                             ImportHistoryTable)


class ImportList(SingleTableMixin, ListView):
    model = DataImport
    paginate_by = 20
    template_name = 'importer/tables/import_history.html'
    table_class = ImportHistoryTable
    ordering = ["-imported_at"]
  
    def get_queryset(self) -> QuerySet[Any]:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            return DataImport.objects.get_data_imports_for_user(self.request.user).order_by('-imported_at')
        return super.get_queryset().order_by('-imported_at')
    
class ImportDetailList(SingleTableMixin, ListView):
    model = ImportedModel
    paginate_by = 30
    template_name = 'importer/tables/import_history_detail.html'
    table_class = ImportHistoryDetailTable

    #Checking if the user has the permission to view the data import
    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            if not DataImport.objects.get(pk=self.kwargs.get("pk")).has_view_permission(request.user):
                raise PermissionDenied
        return super().get(request, *args, **kwargs)      

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        headers_text = DataImport.objects.get(pk=self.kwargs.get("pk")).headers
        context["headers"] = ast.literal_eval(headers_text)
        return context

    def get_queryset(self):
        return ImportedModel.objects.filter(data_import__id=self.kwargs.get("pk")).order_by('-object_id')

class ImportModelList(SingleTableMixin, ListView):
    model = ImportedModel
    paginate_by = 30
    template_name = 'importer/tables/history_model.html'
    table_class = ImportHistoryModelTable
   
    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            if not ImportedModel.objects.get(pk=self.kwargs.get("pk")).data_import.has_view_permission(request.user):
                raise PermissionDenied
        return super().get(request, *args, **kwargs)   

    def get_queryset(self):
        imported_model = ImportedModel.objects.get(pk=self.kwargs.get("pk"))
        return ImportedModel.objects.filter(object_id=imported_model.object_id).filter(content_type=imported_model.content_type).order_by('-object_id')

def create_data_import_object(user, groups):
    #Create a DataImport for import history
    data_import = DataImport.objects.create(
        imported_by=user,
        imported_at=timezone.now(),
        success=True,
    )
    for group in groups:
        data_import.groups.add(group)
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            data_import.assign_perms(group)
    return data_import