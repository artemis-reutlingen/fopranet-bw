from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import get_perms

from practices.models import Practice
from studies.models import StudyParticipation, Study
from utils.utils_test import setup_test_basics

class StudyModelsTest(TestCase):
    def setUp(self) -> None:
        self.study_a = Study(
            title="Random Study",
            start_date="2020-01-01",
            end_date="2020-12-31",
        )
        self.study_b = Study(
            title="Another Random Study",
            start_date="2021-01-01",
            end_date="2025-12-31",
        )

        self.practice_a = Practice(name="Practice A")
        self.practice_b = Practice(name="Practice B")
        self.practice_c = Practice(name="Practice C")

class StudyPermissionsTest(TestCase):
    def setUp(self) -> None:
        self.institute_a, self.group_a, self.user_a, [self.practice_a, self.practice_b] = setup_test_basics(
            diff_id="A"
        )
        self.institute_b, self.group_b, self.user_b, [self.practice_c, self.practice_d] = setup_test_basics(
            diff_id="B"
        )

    def test_institute_member_can_edit_or_delete_study(self):
        study = Study.objects.create(
            title="A Certain Study",
            assigned_institute=self.institute_a,
            start_date="2021-01-01",
            end_date="2025-12-31",
        )
        study.assign_editor_perms_to_institute(self.user_a.assigned_institute)

        users_perms = get_perms(self.user_a, study)
        self.assertIn('change_study', users_perms)
        self.assertIn('delete_study', users_perms)

    def test_non_institute_member_cannot_edit_or_delete_study(self):
        study = Study.objects.create(
            title="Another Certain Study",
            assigned_institute=self.institute_a,
            start_date="2021-01-01",
            end_date="2025-12-31",
        )
        study.assign_editor_perms_to_institute(self.institute_a)

        users_perms = get_perms(self.user_b, study)
        self.assertNotIn('change_study', users_perms)
        self.assertNotIn('delete_study', users_perms)
