"""
Contains the views for the list of people participating in a study
and for adding, editing and deleting person participations.
"""
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import QuerySet, Q
from django.db import transaction
from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic import DetailView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from django_tables2.config import RequestConfig
from crispy_forms.layout import Layout, Row, Column, Submit
from crispy_forms.helper import FormHelper

from practices.models import Practice
from persons.models import Person
from persons.filters import PeopleFilter
from persons.tables import SelectPeopleTable
from utils.view_mixins import ManagePersonPermissionRequired, ManagePracticePermissionRequired

from ..models import Study, PersonStudyParticipation, PersonStudyParticipation, StudyParticipation, HasSignedUpStatus
from ..filters import StudyPeopleParticipationFilter
from ..tables import StudyParticipatingPeopleTable, StudyInvitedPeopleTable, StudyConfirmedPeopleTable, StudyFullPeopleTable
from ..forms import ParticipatingPersonEditForm, PersonStudyParticipationForm
from . import study_bulk_people_log

class ParticipatingPeopleList(DetailView):
    model = Study

    # NOTE: This should be move to the filter class and configured in __init__ automatically.
    class _FormHelper(FormHelper):
        form_method = 'GET'
        layout = Layout(
            Row(
                Column('practice'),
                Column(Submit('submit', value=_('Apply filter')))
            )
        )

    template_name = 'studies/study_people_participations_list.html'
    context_object_name = 'study'

    def get_study_participants(self):
        permitted_person_ids = Person.objects.get_person_objects_for_user(self.request.user).values('id')
        return PersonStudyParticipation.objects.filter(
            study_participation__study = self.object, 
            person_id__in = permitted_person_ids
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        study_people_participations_filter = StudyPeopleParticipationFilter(
            self.request.GET,
            queryset = self.get_study_participants()
        )
        study_people_participations_filter.form.helper = ParticipatingPeopleList._FormHelper()
        all_table = StudyFullPeopleTable(study_people_participations_filter.qs, order_by = "person__last_name", prefix = "all-")
        RequestConfig(self.request, paginate={"per_page": 12}).configure(all_table)

        participants = study_people_participations_filter.qs.filter(Q(has_signed_up = HasSignedUpStatus.CONFIRMATION) | Q(has_participated = True))
        participants_table = StudyConfirmedPeopleTable(participants, order_by = "person__last_name", prefix = "participants-")
        RequestConfig(self.request, paginate={"per_page": 12}).configure(participants_table)

        others = study_people_participations_filter.qs.exclude(has_signed_up = HasSignedUpStatus.CONFIRMATION).exclude(has_participated = True)
        others_table = StudyInvitedPeopleTable(others, order_by = "person__last_name", prefix = "others-")
        RequestConfig(self.request, paginate={"per_page": 12}).configure(others_table)

        context['filter'] = study_people_participations_filter
        context['tables'] = {
            "all": all_table,
            "participants": participants_table,
            "others": others_table
        }
        context['active_participating_people'] = "active"

        return context
    

class AddParticipatingPerson(ManagePracticePermissionRequired, FormView):
    template_name = 'global/forms/generic.html'
    form_class = PersonStudyParticipationForm

    def get_study_participation(self):
        return StudyParticipation.objects.get(pk=self.kwargs['study_participation_id'])

    def get_permission_object(self):
        return self.get_study_participation().practice

    def form_valid(self, form):
        add_person_to_study(self.get_study_participation().study, form.cleaned_data['person'])
        return super().form_valid(form)
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["study_participation"] = self.get_study_participation()
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_title"] = _("Study participation for ") + self.get_study_participation().study.title
        return context

    def __str__(self) -> str:
        return f"{_('Participating person')}"

    def get_success_url(self):
        return reverse_lazy(
            'practices:study-participation-list',
            kwargs={'pk': self.get_study_participation().practice.id}
        )

class EditParticipatingPerson(ManagePersonPermissionRequired, UpdateView):
    form_class = ParticipatingPersonEditForm
    template_name = 'studies/edit_study_person_participation.html'
    model = PersonStudyParticipation

    def get_permission_object(self):
        return self.get_object().person

    def get_success_url(self):
        return reverse_lazy(
            'studies:people-participation-list',
            kwargs={'pk': self.get_object().study_participation.study.id}
        )
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["study"] = self.get_object().study_participation.study
        return context


class RemoveParticipatingPerson(ManagePersonPermissionRequired, SuccessMessageMixin, DeleteView):
    model = PersonStudyParticipation
    success_message = _('Removed person from this study.')

    def get_success_url(self):
        default_url = reverse_lazy("studies:people-participation-list", kwargs = {"pk": self.get_object().study_participation.study.pk})
        return self.request.META.get("HTTP_REFERER", default_url)

    def get_permission_object(self):
        return self.get_object().person


def add_person_to_study(study: Study, person: Person):
    from .participating_practices import add_practice_to_study
    practice_participation = add_practice_to_study(study, person.practice)
    person_participation, _ = PersonStudyParticipation.objects.update_or_create(
        study_participation = practice_participation,
        person = person
    )
    return person_participation


def add_people_to_study(study: Study, people: QuerySet[Person]):
    for person in people:
        add_person_to_study(study, person)

def bulk_create_participations(request, pk):
    study = get_object_or_404(Study, pk = pk)
    if not request.user.has_perm('studies.view_study', study):
        raise PermissionDenied
    
    participating_person_ids = PersonStudyParticipation.objects \
                                    .filter(study_participation__study = study) \
                                    .values_list('person_id', flat = True)
    
    available_people = Person.objects.get_person_objects_for_user(request.user) \
                                     .exclude(id__in = participating_person_ids)
    
    people_filter = PeopleFilter(request.GET, queryset = available_people, request = request)
    table = SelectPeopleTable(people_filter.qs)
    RequestConfig(request, paginate={"per_page": 12}).configure(table)

    if request.method == "POST":
        with transaction.atomic():
            selected_people = Person.objects.filter(id__in=request.POST.getlist("selection"))
            add_people_to_study(study, selected_people)

            # NOTE: THREE LINES OF CODE for a log entry. WHY
            class BulkCreateStudyParticipationsForPeople: pass
            participations_text = ",".join([str(person.id) for person in selected_people])
            study_bulk_people_log.send(sender = BulkCreateStudyParticipationsForPeople, username = request.user.username, study_id = study.id, participations = participations_text)

            messages.success(request, _("Successfully added people to the study"))
            return redirect("studies:people-participation-list", pk = pk)
    
    context = {
        "study": study,
        "table": table,
        "filter": people_filter,
        "active_add_people": "active"
    }
    return render(request, "studies/select_people.html", context)