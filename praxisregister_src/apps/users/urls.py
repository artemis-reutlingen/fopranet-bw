from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from users import views

app_name = 'users'

urlpatterns = [
    path('who-are-you/', LoginView.as_view(template_name='users/login_prompt.html'), name='login'),
    path('bye-bye/', LogoutView.as_view(next_page='landing-page'), name='logout'),
    path('<int:pk>/', views.UserUpdateView.as_view(), name='user-update'),
    path('change-password', views.change_password, name='change-password'),
    path('profile/', views.MemberPageView.as_view(), name='member-page'),
    path('create', views.CreateUserFormView.as_view(), name='create'),
]
