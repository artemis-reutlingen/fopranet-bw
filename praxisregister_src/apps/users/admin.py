from typing import Any
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.sessions.models import Session
from django.utils.translation import gettext_lazy as _
from django.contrib import admin

from users.models import User

#add extra fields to user admin
UserAdmin.list_display += ('assigned_institute',)
UserAdmin.fieldsets += (('Institute', {'fields': ('assigned_institute',)}),)
UserAdmin.add_fieldsets += ((_("Other fields"), {'fields': ('first_name', 'last_name', 'assigned_institute', 'email')}),)

#Customize save_model to add user to group of assigned institute
class MyUserAdmin(UserAdmin):
    def save_model(self, request: Any, obj: Any, form: Any, change: Any) -> None:
        super().save_model(request, obj, form, change)
        institute = form.cleaned_data.get('assigned_institute')
        obj.groups.add(institute.group) 

admin.site.register(User, MyUserAdmin)

class SessionAdmin(ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()

    list_display = ['session_key', '_session_data', 'expire_date']


admin.site.register(Session, SessionAdmin)
