from django.contrib.auth.models import AbstractUser, UserManager as DjangoUserManager
from django.db import models
from django.db.models import Q
from todos.models import ToDoStatus
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import get_users_with_perms

from feedback.models import Feedback
from institutes.models import Institute
from practices.models import Practice


class UserManager(DjangoUserManager):
    def create_user_for_institute(self, username: str, password: str, institute: Institute):
        """
        Custom user constructor. Directly associating the new user object with an institute.
        Use this method to create new users in general.
        """
        user = self.create(username=username)
        user.set_password(password)
        user.assigned_institute = institute
        user.groups.add(institute.group)
        user.save()
        return user

    def get_users_for_institute(self, institute):
        return self.exclude(is_staff=True).filter(assigned_institute=institute)

    @staticmethod
    def get_managing_users_for_practice(practice: Practice):
        return get_users_with_perms(practice, only_with_perms_in=['manage_practice'])


class User(AbstractUser):
    objects = UserManager()
    phone_number = models.CharField(verbose_name=_('Phone number'), max_length=255, null=True, blank=True, default="")
    receive_emails = models.BooleanField(null=False, blank=False, default=True)
    medical_title = models.CharField(verbose_name=_('Medical title'), max_length=255, null=False, blank=True)
    assigned_institute = models.ForeignKey('institutes.Institute', verbose_name=_('Institute'),
                                           on_delete=models.SET_NULL,
                                           null=True)

    @property
    def full_name(self):
        full_name = f"{self.first_name} {self.last_name}"
        return full_name if (self.first_name and self.last_name) else _("Unnamed Person")

    @property
    def full_name_with_title(self):
        return f"{self.medical_title} {self.full_name}"

    def has_unseen_todo(self):
        return ToDoStatus.objects.filter(user__id=self.id, seen_by_user=False).count() > 0
    
    def has_unseen_feedback(self):
        return Feedback.objects.filter(user__id=self.id, user_notification=True).count() > 0

    def __str__(self):
        return f"{self.medical_title} {self.full_name}"


    @property
    def group(self):
        return self.assigned_institute.group if self.assigned_institute else None
