from feedback.models import *
from django.utils.html import format_html
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from django.utils.html import format_html
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

class FeedbackAdmin(GuardedModelAdmin):
    model = Feedback
    list_display = ("id", "closed", "date", "topic", "step", "user", "reviewed")

class FeedbackCommentAdmin(GuardedModelAdmin):
    model = FeedbackComment
    list_display = ("id", "feedback_change", "user", "comment_date")
    change_form_template = 'global/forms/admin_change_form_ckeditor_support.html'

    @admin.display(ordering='feedback_change', description=_('Feedback'))
    def feedback_change(self, feedback_comment):
        if(feedback_comment.feedback):
            url = reverse(f"admin:feedback_feedback_change", args=(feedback_comment.feedback.id,))
            return format_html('<a href="{}">{}<a>', url, feedback_comment.feedback.topic)
        else:
            return 'No Feedback connection (Should not happen)'

class WorkflowAdmin(GuardedModelAdmin):
    model = Workflow
    list_display = ("topic", "number")

class WorkflowStepAdmin(GuardedModelAdmin):
    model = WorkflowStep
    list_display = ("workflow", "name", "number")

class WorkflowStepDoneAdmin(GuardedModelAdmin):
    model = WorkflowStepDone
    list_display = ("workflow_step", "user", "done", "done_at")

admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(FeedbackComment, FeedbackCommentAdmin)
admin.site.register(Workflow, WorkflowAdmin)
admin.site.register(WorkflowStep, WorkflowStepAdmin)
admin.site.register(WorkflowStepDone, WorkflowStepDoneAdmin)
