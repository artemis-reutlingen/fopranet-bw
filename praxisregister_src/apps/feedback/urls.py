from django.urls import path
from feedback.views import *
from django.views.generic import TemplateView

app_name = 'feedback'

urlpatterns = [
    path('', WorkflowDetail.as_view(), name='workflow-detail'),
    path('list', FeedbackList.as_view(), name='list'),
    path('create', FeedbackCreate.as_view(), name='create'),
    path('known-issues', TemplateView.as_view(template_name="feedback/feedback_known_issues.html"), name='know-issues'),
    path('update/<int:pk>', FeedbackEdit.as_view(), name='update'),
    path('detail/<int:pk>', FeedbackDetail.as_view(), name='detail'),
    path('delete/<int:pk>', FeedbackDelete.as_view(), name='delete'),
    path('done/<int:pk>', create_or_update_workflowstepdone, name='workflowstepdone'),
    path('file/<int:pk>/download', download_feedback_file, name='file-download'),
    path('file/<int:pk>/delete', delete_feedback_file, name='file-delete'),
    ]