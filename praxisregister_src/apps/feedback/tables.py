from django.utils.translation import gettext_lazy as _
from django_tables2 import Column, Table
from feedback.models import Feedback, FeedbackComment
from django.utils.safestring import mark_safe
from django.urls import reverse
from utils.templatetags.done_or_not_checkmark import doneornot

from django.db.models import QuerySet, OuterRef, Subquery
class FeedbackTable(Table):
    
    # user = Column(verbose_name=_("Author"), orderable=True)
    edit = Column(empty_values=(), verbose_name=_("Edit"), orderable=False)
    last_comment = Column(empty_values=(), verbose_name=_("Last comment"))
    closed = Column(verbose_name=_("Realized"))
    user_notification = Column(verbose_name=_("Message"))
    
    def render_detail(self, value, record):
        detail_url = reverse('feedback:detail', kwargs={'pk': record.id})
        return mark_safe(f'<a href={detail_url} class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i></a>')

    def render_edit(self, value, record):
        delete_url = reverse('feedback:delete', kwargs={'pk': record.id})
        update_url = reverse('feedback:update', kwargs={'pk': record.id})
        update_html = f'<a href={update_url} class="btn btn-outline-primary btn-sm"><i class="fa fa-edit"></i></a>'
        delete_html = f'<a href={delete_url} class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a>'
        return mark_safe(f'{update_html} {delete_html}')
 
    def render_topic(self, value, record):
        return mark_safe(f'<p class="text-nowrap">{value}</p>')
    
    def render_step(self, value, record):
        return mark_safe(f'<p class="text-nowrap">{value}</p>')
    
    def render_reviewed(self, value, record):
        return doneornot(value)
    
    def render_last_comment(self, value, record):
        feedback_comment = record.feedbackcomment_set.last()
        format = "%d.%m.%Y %H:%M:%S"
        return mark_safe(f'<p class="text-nowrap">{feedback_comment.comment_date.strftime(format) if feedback_comment else ""}</p>')

    def order_last_comment(self, queryset: QuerySet, is_descending):
        # Subquery that gets the latest comment date for a Feedback.
        latest_comment_subquery = FeedbackComment.objects \
            .filter(feedback = OuterRef('pk')) \
            .order_by('-comment_date') \
            .values('comment_date')[:1]
        # Adds the latest comment date to all Feedbacks in the queryset.
        queryset = queryset.alias(latest_comment_date=Subquery(latest_comment_subquery))
        queryset = queryset.order_by('latest_comment_date')
        if is_descending:
            queryset = queryset.reverse()
        return (queryset, True)

    def render_closed(self, value, record):
        return doneornot(value)
    
    def render_user_notification(self, value, record):
        detail_url = reverse('feedback:detail', kwargs={'pk': record.id})
        icon = '<i class="fa-regular fa-envelope fa-bounce"></i>' if value else '<i class="fa-regular fa-envelope-open"></i>'
        return mark_safe(f'<a href={detail_url} class="btn btn-outline-primary btn-sm">{icon}<a>')

    class Meta:
        model = Feedback
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("user_notification", "user", "topic", "step", "reviewed", "last_comment", "closed", "edit")