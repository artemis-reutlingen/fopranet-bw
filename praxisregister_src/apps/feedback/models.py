from django.db import models
from django.utils.translation import gettext_lazy as _
from django_ckeditor_5.fields import CKEditor5Field
from guardian.shortcuts import assign_perm, get_objects_for_user
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase

class FeedbackManager(models.Manager):
    def get_feedbacks_for_user(self, user):
        return get_objects_for_user(user, 'feedback.view_feedback')

class Feedback(models.Model):
    objects = FeedbackManager()

    topic = models.CharField(max_length=255, blank=False, verbose_name=_('Topic'))
    step = models.CharField(max_length=255, blank=False, verbose_name=_('Step'))
    unclarities = models.TextField(verbose_name=_('Unclarities'), blank=True, null=True)
    suggestions = models.TextField(verbose_name=_('Suggestions'), blank=True, null=True)
    error_reporting = models.TextField(verbose_name=_('Error reporting'), blank=True, null=True)
    file = models.FileField(verbose_name=_('File'), blank=True, null=True)

    reviewed = models.BooleanField(default=False, verbose_name=_('Viewed by developer'))
    closed = models.BooleanField(default=False, verbose_name=_('Closed'))
    user_notification = models.BooleanField(default=False, verbose_name=_('User notification'))

    date = models.DateTimeField(auto_now_add=True, verbose_name=_('Date'))
    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, verbose_name=_('User'))

    def assign_perms(self, user):
        assign_perm('feedback.view_feedback', user, self)
        assign_perm('feedback.change_feedback', user, self)
        assign_perm('feedback.delete_feedback', user, self)
        
    def __str__(self):
        new = _("New feedback")
        return f"{self.topic}: {self.step}" if self.topic else f"{new}"
    
    class Meta:
        verbose_name = _("Feedback")
        verbose_name_plural = _("Feedbacks")

# Explicit definition of Feedback object permissions. This ensures permission objects are also deleted when a feedback is deleted instead of being orphaned.
class FeedbackUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Feedback, on_delete = models.CASCADE)

class FeedbackGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Feedback, on_delete = models.CASCADE)


class FeedbackComment(models.Model):
    feedback = models.ForeignKey(Feedback, on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, verbose_name=_('User'))
    comment = CKEditor5Field(verbose_name=_("Comment"), blank=False, default="", config_name='extends')
    comment_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Date'))

    def __str__(self) -> str:
        return ""

class Workflow(models.Model):
    topic = models.CharField(max_length=255, blank=False, verbose_name=_('Topic'))
    number = models.IntegerField(blank=False, verbose_name=_('Number'), default=0)

    def __str__(self):
        return self.topic
    
class WorkflowStep(models.Model):
    workflow = models.ForeignKey(Workflow, on_delete=models.CASCADE)
    number = models.IntegerField(blank=False, verbose_name=_('Number'), default=0)
    name = models.CharField(max_length=255, blank=False, verbose_name=_('Name'))
    
    def __str__(self):
        return f"{self.workflow}: {self.name}"

class WorkflowStepDone(models.Model):
    workflow_step = models.ForeignKey(WorkflowStep, on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    done = models.BooleanField(default=False, verbose_name=_('Done'))
    done_at = models.DateTimeField(auto_now=True, verbose_name=_('Done at'), blank=True, null=True)
    