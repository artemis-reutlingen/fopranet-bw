import os
from typing import Any

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.forms.models import BaseModelForm
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import (CreateView, DeleteView, ListView, UpdateView, FormView)
from django_tables2 import SingleTableMixin
from feedback.forms import FeedbackForm, FeedbackCommentForm
from feedback.models import Feedback, Workflow, WorkflowStep, WorkflowStepDone, FeedbackComment
from feedback.tables import FeedbackTable
from utils.fields import ModelRenderer
from guardian.mixins import PermissionRequiredMixin


class WorkflowDetail(ListView):
    model = Workflow
    template_name = 'feedback/workflow_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        steps_done = WorkflowStepDone.objects.filter(user=self.request.user, done=True).values_list('workflow_step', flat=True)
        context['steps_done'] = steps_done
        return context
    
    def get_queryset(self):
        return Workflow.objects.all().order_by('number')

class FeedbackList(SingleTableMixin, ListView):
    model = Feedback
    template_name = 'feedback/feedback_list.html'
    paginate_by = 10
    table_class = FeedbackTable

    def get_queryset(self):
        return Feedback.objects.get_feedbacks_for_user(self.request.user).order_by('-date')

class FeedbackCreate(CreateView):
    model = Feedback
    form_class = FeedbackForm
    template_name = 'feedback/feedback_form.html'
    success_url = reverse_lazy('feedback:list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)
    
    def get_success_url(self) -> str:
        self.object.assign_perms(self.request.user)
        messages.success(self.request, _("Feedback successfully created."))
        return super().get_success_url()
    
class FeedbackEdit(PermissionRequiredMixin, UpdateView):
    model = Feedback
    form_class = FeedbackForm
    template_name = 'feedback/feedback_form.html'
    permission_required = "feedback.change_feedback"
    return_403 = True
    success_url = reverse_lazy('feedback:list')

    def get_queryset(self):
        return Feedback.objects.filter(user=self.request.user)
    
    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        context['download_url'] = reverse("feedback:file-download", kwargs={'pk': self.object.id})
        context['confirm_url'] = reverse("feedback:file-delete", kwargs={'pk': self.object.id})
        return context

class FeedbackDetail(PermissionRequiredMixin, FormView):
    form_class = FeedbackCommentForm
    template_name = 'feedback/feedback_detail.html'
    feedback_fields = ["user", "topic", "step", "unclarities", "suggestions", "error_reporting", "file"]
    permission_required = "feedback.change_feedback"
    return_403 = True

    def get(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        feedback = self.get_feedback()
        if feedback.user == request.user:
            feedback.user_notification = False
            feedback.save()
        return super().get(request, *args, **kwargs)
    
    def get_permission_object(self):
        return self.get_feedback()

    def get_feedback(self):
        return Feedback.objects.get(id=self.kwargs['pk'])
    
    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            feedback = self.get_feedback()
            form.instance.user = self.request.user
            form.instance.feedback = feedback
            form.save()
            if self.request.user.is_superuser:
                feedback.reviewed = True
                feedback.user_notification = True
                feedback.save()
            messages.success(self.request, _("Comment successfully added."))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modelRenderer"] = ModelRenderer(instance=self.get_feedback(), fields=self.feedback_fields)
        feedbackComments = FeedbackComment.objects.filter(feedback=self.get_feedback())
        context["feedbackComments"] = feedbackComments
        return context
    
    def get_success_url(self) -> str:
        return reverse("feedback:detail", kwargs={'pk': self.get_feedback().id})

class FeedbackDelete(PermissionRequiredMixin, DeleteView):
    model = Feedback
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = "feedback.delete_feedback"
    return_403 = True
    success_url = reverse_lazy('feedback:list')

    def get_queryset(self):
        return Feedback.objects.filter(user=self.request.user)

def create_or_update_workflowstepdone(request, pk):
    workflowstepdone = WorkflowStepDone.objects.filter(user=request.user, workflow_step=pk)
    if workflowstepdone.exists():
        workflowstepdone = workflowstepdone.first()
        workflowstepdone.done = not workflowstepdone.done
        workflowstepdone.save()
    else:
        workflow_step = WorkflowStep.objects.get(pk=pk)
        workflowstepdone = WorkflowStepDone.objects.create(user=request.user, workflow_step=workflow_step, done=True)
        workflowstepdone.save() 
    return redirect('feedback:workflow-detail')


def download_feedback_file(request, pk):
    feedback = Feedback.objects.get(id=pk)

    if not request.user.has_perm('feedback.view_feedback', feedback):
        raise PermissionDenied
    
    file = feedback.file
    file_name = os.path.basename(file.name)

    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={file_name}"
    response['X-Accel-Redirect'] = f"{file.url}"
    return response

def delete_feedback_file(request, pk):
    feedback = Feedback.objects.get(id=pk)

    if not request.user.has_perm('feedback.delete_feedback', feedback):
        raise PermissionDenied
    
    if request.user != feedback.user:
        raise PermissionDenied
    
    feedback.file.delete()
    feedback.save()
    messages.success(request, _("File successfully deleted."))

    return redirect('feedback:update', pk=pk)