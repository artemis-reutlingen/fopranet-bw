from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from changelogs.models import ChangelogNotification
from django.contrib import messages
from django.utils.translation import gettext_lazy as _


"""
Keep the latest changelog in this view, in order to reset the notification.
"""
def latest_changelog(request):
    user_notification, created = ChangelogNotification.objects.get_or_create(user=request.user)
    user_notification.animate_changelog=False
    user_notification.save()
    return render(request, "changelogs/changelog_0.7.0.html")

def changelog_notification(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    if request.method == "POST":
        ChangelogNotification.objects.update(animate_changelog=True)
        messages.success(request, _("Changelog notification animation has been set."))
    return render(request, "changelogs/changelog_notification.html")