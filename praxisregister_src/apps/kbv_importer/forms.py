from django import forms
from importer.forms import ImportFileForm
from django.utils.translation import gettext_lazy as _
from import_export.forms import ConfirmImportForm

class KBVMapperFieldsImportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        kbv_tuples = kwargs.pop("kbv_tuples", {})
        super().__init__(*args, **kwargs)

        for kbv_tuple in kbv_tuples:
            self.fields[kbv_tuple[0].name + kbv_tuple[1].name] = forms.CharField(label=f"{self.get_model_name(kbv_tuple[0])}: {kbv_tuple[1].verbose_name}", max_length=255, required=False)

    def get_model_name(self, mapper):
        model_class = mapper.content_type.model_class()
        return model_class._meta.verbose_name if model_class else mapper.content_type.model 

class KbvImportConfirmForm(ConfirmImportForm):

    def __init__(self, *args, **kwargs):
        groups = kwargs.pop("groups")
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = groups
        
    groups = forms.ModelMultipleChoiceField(widget=forms.MultipleHiddenInput, queryset=None)
