from practice_importer.resources import PracticeResource

class KbvResource(PracticeResource):

    def __init__(self, mapper, groups):
        super().__init__(mapper, groups, import_id="kbv_import_id", import_by="kbv_import_by")
    
    class Meta:
        skip_unchanged = True
        store_instance= False