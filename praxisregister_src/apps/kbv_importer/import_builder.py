from dataclasses import dataclass
from users.models import User
from import_export.resources import ModelResource
from import_export.forms import ConfirmImportForm
from importer.models import ChunkImport, DataImport
from django.contrib.auth.models import Group
from tablib import Dataset
from django.contrib.contenttypes.models import ContentType

@dataclass
class ImportObject:
    import_type: str = None # KBV, Practice, DB
    model_import_type: ContentType = None # Currently Practice or Person
    storage_dataset: Dataset = None
    sub_dataset: Dataset = None
    user: User = None
    data_import: DataImport = None
    resource: ModelResource = None
    mappers: [] = None
    main_content_type: ContentType = None
    chunk_import: ChunkImport = None
    import_url: str = None
    html_template: str = None

    confirm_form_instance: ConfirmImportForm = None
    confirm_form_class: ConfirmImportForm = None
    
    #From ConfirmForm
    file_name: str = None
    original_file_name: str = None
    input_format: int = None
    groups: Group = None

# compact setters
        
    def set_confirm_form_data(self, confirm_form):
        self.file_name = confirm_form.cleaned_data["import_file_name"]
        self.original_file_name = confirm_form.cleaned_data["original_file_name"]
        self.groups = confirm_form.cleaned_data["groups"]
        self.input_format = confirm_form.cleaned_data["input_format"]
        self.confirm_form_instance = confirm_form
        self.check_for_other_fields()
        return self

    def check_for_other_fields(self):
        if self.chunk_import and self.storage_dataset:
            self.sub_dataset = self.chunk_import.create_sub_dataset(self.storage_dataset)
        if self.confirm_form_class and self.user and self.file_name and self.original_file_name and self.input_format and self.groups:
            kwargs = {"groups": self.user.groups.all()}
            self.confirm_form_instance = self.confirm_form_class(
                initial={
                    "import_file_name": self.file_name,
                    "original_file_name": self.original_file_name,
                    "input_format": self.input_format,
                    "groups": self.groups,
                },
                **kwargs)
            if self.model_import_type:
                self.confirm_form_instance.initial["import_type"] = self.model_import_type


# individual setters for each attribute
    
    def set_confirm_form_class(self, confirm_form_class):  
        self.confirm_form_class = confirm_form_class
        self.check_for_other_fields()
        return self

    def set_import_type(self, import_type):
        self.import_type = import_type
        return self
    
    def set_model_import_type(self, model_import_type):
        self.model_import_type = model_import_type
        self.check_for_other_fields()
        return self

    def set_chunk_import(self, chunk_import):
        self.chunk_import = chunk_import
        self.check_for_other_fields()
        return self

    def set_storage_dataset(self, storage_dataset):
        self.storage_dataset = storage_dataset
        self.check_for_other_fields()
        return self

    def set_user(self, user):
        self.user = user
        self.check_for_other_fields()
        return self

    def set_data_import(self, data_import):
        self.data_import = data_import
        return self

    def set_resource(self, resource):
        self.resource = resource
        return self

    def set_mappers(self, mappers):
        self.mappers = mappers
        return self

    def set_main_content_type(self, main_content_type):
        self.main_content_type = main_content_type
        return self

    def set_file_name(self, file_name):
        self.check_for_other_fields()
        self.file_name = file_name
        return self

    def set_original_file_name(self, original_file_name):
        self.check_for_other_fields()
        self.original_file_name = original_file_name
        return self

    def set_groups(self, groups):
        self.groups = groups
        return self
    
    def set_input_format(self, input_format):
        self.input_format = input_format
        self.check_for_other_fields()
        return self
    
    def set_import_url(self, import_url):
        self.import_url = import_url
        return self
    
    def set_html_template(self, html_template):
        self.html_template = html_template
        return self