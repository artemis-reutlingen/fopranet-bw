from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import assign_perm

from institutes.models import Institute
from practices.models import Practice
from persons.models import Person
from events.models import EventInfo
from studies.models import Study
from processes.models import ProcessDefinition
from users.models import User

from datetime import date

class TestDataReset(TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.institute = Institute.objects.create_institute_with_group("GPBRN", "Institute", "GPBRN_Institut")
        self.user = User.objects.create_user(username = "Testuser", password = "Practice1234", assigned_institute = self.institute)
        self.practice = Practice.objects.create(name = "Testpractice", assigned_institute = self.institute)
        self.person = Person.objects.create(practice = self.practice, first_name = "Testperson")
        self.event = EventInfo.objects.create(title = "Event", assigned_institute = self.institute)
        self.study = Study.objects.create(title = "Study", start_date = date.today(), end_date = date.today(), assigned_institute = self.institute)
        self.process = ProcessDefinition.objects.create(name = "Process", author = self.user)

        self.reset_url = reverse("practice_importer:clear-data")

    def test_if_data_reset_is_complete(self):
        assign_perm("institutes.can_clear_data", self.user, self.institute)
        self.client.login(username = "Testuser", password = "Practice1234")

        response = self.client.get(self.reset_url)

        self.assertRedirects(response, reverse("practice_importer:importer-overview"), target_status_code = 200)
        self.assertNotIn(self.practice, Practice.objects.all())
        self.assertNotIn(self.person, Person.objects.all())
        self.assertNotIn(self.event, EventInfo.objects.all())
        self.assertNotIn(self.study, Study.objects.all())
        self.assertNotIn(self.process, ProcessDefinition.objects.all())

        self.assertIn(self.institute, Institute.objects.all())
        self.assertIn(self.user, User.objects.all())

    def test_if_data_reset_fails_without_permission(self):
        self.client.login(username = "Testuser", password = "Practice1234")

        response = self.client.get(self.reset_url)

        self.assertEqual(response.status_code, 403)

    def test_if_only_institute_data_is_affected(self):
        other_institute = Institute.objects.create_institute_with_group("GPBRN2", "Institute2", "GPBRN2_Institut2")
        other_user = User.objects.create_user(username = "Testuser2", password = "Practice1234", assigned_institute = other_institute)
        other_practice = Practice.objects.create(name = "Testpractice2", assigned_institute = other_institute)
        other_person = Person.objects.create(practice = other_practice, first_name = "Testperson2")
        other_event = EventInfo.objects.create(title = "Event2", assigned_institute = other_institute)
        other_study = Study.objects.create(title = "Study2", start_date = date.today(), end_date = date.today(), assigned_institute = other_institute)
        other_process = ProcessDefinition.objects.create(name = "Process2", author = other_user)

        assign_perm("institutes.can_clear_data", self.user, self.institute)
        self.client.login(username = "Testuser", password = "Practice1234")

        response = self.client.get(self.reset_url)

        self.assertIn(other_practice, Practice.objects.all())
        self.assertIn(other_person, Person.objects.all())
        self.assertIn(other_event, EventInfo.objects.all())
        self.assertIn(other_study, Study.objects.all())
        self.assertIn(other_process, ProcessDefinition.objects.all())
        self.assertIn(other_institute, Institute.objects.all())
        self.assertIn(other_user, User.objects.all())
