import logging

from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _
from django.db import transaction
from importer.models import Mapper, MapperField, MapperFieldChoice
from importer.views.importer_utils import get_field_by_field_name_dict
from practice_importer.constants import *
from practice_importer.forms import MapperFieldsImportForm

logger = logging.getLogger(__name__)

# Models with ForeignKey need an identifier to the mapper name to see its related model
def get_or_create_mapper(model, institute, model_identifier="", import_type=""):
    model_identifier = str(model_identifier._meta.model_name).lower() if model_identifier != "" else ""
    name = get_definied_mapper_name(model, institute, model_identifier) + (f"_{import_type}" if import_type != "" else "")
    return Mapper.objects.get_or_create(name=name, content_type=ContentType.objects.get_for_model(model._meta.model), import_type=import_type, institute=institute)

def get_definied_mapper_name(model, institute, model_identifier=""):
    return str(institute.id) + model_identifier + MAPPER_NAME_ADDITION + str(model._meta.model_name).lower()

"""
The Get method creates a form for a model and its related models (foreign key to the main_model).
The Post method creates a mapper for each model and creates mapper fields corresponding to the form input.
The created mappers are in relation to each other 

mapper_set = {mapper : field identifier}
foreignkey_set = {mapper : foreignkey field name}
"""
def default_mapper_fields_creation(request, mapper_of_main_model, mappers, foreignkey_dict, one_to_one_dict, redirect_url, custom_fields = {}):
    kwargs={"mappers": mappers, "custom_fields": custom_fields}

    if request.method == "POST":
        import_form = MapperFieldsImportForm(request.POST, **kwargs)
        if import_form.is_valid():
            # Add for field creation    
            for mapper in custom_fields.keys():
                mappers.append(mapper)
            
            with transaction.atomic():
                #Clean mapper fields
                MapperField.objects.filter(mapper__in=mappers).delete()
                for fieldname, importname in import_form.cleaned_data.items():
                    # Ignore empty fields
                    if importname is None or importname.strip() == "":
                        continue
                    # Create mapper fields
                    for mapper in mappers:
                        if fieldname.startswith(mapper.name):
                            createMapperField(mapper, fieldname, importname)
                            break

                for related_mapper, field_name in foreignkey_dict.items():
                    add_related_model_as_foreignkey_to_mapper(mapper_of_main_model, related_mapper, field_name)

                for related_mapper, field_name in one_to_one_dict.items():
                    add_related_model_as_one_to_one_field_to_mapper(mapper_of_main_model, related_mapper, field_name)
        
            return redirect(redirect_url)
        
    elif request.method == "GET":
        initial = {}
        #Load saved mapper fields
        for mapper in mappers:
            for field in mapper.mapperfield_set.all():
                initial[mapper.name + field.internal_fieldname] = field.import_fieldname
        
        for mapper, field in custom_fields.items():
            mapper_field = mapper.mapperfield_set.filter(internal_fieldname=field.name).first()
            if mapper_field:
                initial[mapper.name + mapper_field.internal_fieldname] = mapper_field.import_fieldname 

        context = {"form": MapperFieldsImportForm(initial=initial, **kwargs)}
        if mapper_of_main_model.content_type.model_class()._meta.model_name == "practice":
            context["practice"] = True
        return render(request, 'practice_importer/import_form.html', context)

def createMapperField(mapper, fieldname, importname):

    internal_fieldname = fieldname.replace(mapper.name, "")
    field_dict = get_field_by_field_name_dict(mapper.content_type)
    field = field_dict[internal_fieldname]

    logger.debug(f"Create MapperField for {mapper.name} with internal_fieldname: {internal_fieldname} and import_fieldname: {importname}")
    logger.debug(f"Field type: {field.get_internal_type()}")
    mapper_field = MapperField.objects.create(
        internal_fieldname=internal_fieldname,
        import_fieldname=importname,
        mapper=mapper,
        internal_type=field.get_internal_type()
    )
    # CharFields with choices need to save the choices for a ChoicesWidget (See resources.py in importer app)
    if field.get_internal_type() == "CharField" and field.choices:
        for item in field.choices:
            obj, created =  MapperFieldChoice.objects.get_or_create(key=item[0], value=item[1])
            obj.mapper_field.add(mapper_field)

def add_related_model_as_foreignkey_to_mapper(mapper_of_main_model, related_mapper, internal_fieldname):
    #If no mapper specific fields were filled by the user, we dont need to make a connection
    if related_mapper.mapperfield_set.all().count() > 0:
        mapper_field, created = MapperField.objects.get_or_create(
                    internal_fieldname=internal_fieldname,
                    #The import_fieldname is just temporary. During the import the fieldname will be changed to the correct pk
                    import_fieldname=FOREIGNKEY_IMPORT_FIELDNAME,
                    mapper=related_mapper,
                    internal_type="ForeignKey",
                    foreign_key_model=mapper_of_main_model.content_type
                )
        return mapper_field
    
def add_related_model_as_one_to_one_field_to_mapper(mapper_of_main_model, related_mapper, internal_fieldname):
    if related_mapper.mapperfield_set.all().count() > 0:
        mapper_field, created = MapperField.objects.get_or_create(
                    internal_fieldname=internal_fieldname,
                    import_fieldname=ONE_TO_ONE_IMPORT_FIELDNAME,
                    mapper=related_mapper,
                    internal_type="OneToOneField",
                    foreign_key_model=mapper_of_main_model.content_type
                )
        return mapper_field


