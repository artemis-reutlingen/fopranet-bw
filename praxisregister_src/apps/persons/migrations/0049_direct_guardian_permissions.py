# Generated by Django 4.2.7 on 2025-01-16 15:04

from django.db import migrations


def create_direct_user_permissions_from_old_model(apps, model, new_permission_model):
    """Goes through all instances of the generic user permissions for the given content_type and creates the corresponding new permission."""
    UserObjectPermission = apps.get_model("guardian", "UserObjectPermission")
    ContentType = apps.get_model('contenttypes', 'ContentType')
    model_content_type = ContentType.objects.get_for_model(model)

    # Gets a list of all primary keys of the model that still exist.
    valid_pks = list(model.objects.all().values_list("pk", flat=True))

    direct_permissions = []
    for generic_permission in UserObjectPermission.objects.filter(content_type = model_content_type):
        if int(generic_permission.object_pk) in valid_pks:
            # The new permission instance can only be created if the primary key is still valid and the related object hasn't been deleted.
            direct_permission = new_permission_model(
                content_object_id = generic_permission.object_pk,
                user = generic_permission.user,
                permission = generic_permission.permission
            )
            direct_permissions.append(direct_permission)
    new_permission_model.objects.bulk_create(direct_permissions)

def create_direct_group_permissions_from_old_model(apps, model, new_permission_model):
    """Goes through all instances of the generic group permissions for the given content_type and creates the corresponding new permission."""
    GroupObjectPermission = apps.get_model("guardian", "GroupObjectPermission")
    ContentType = apps.get_model('contenttypes', 'ContentType')
    model_content_type = ContentType.objects.get_for_model(model)
    valid_pks = list(model.objects.all().values_list("pk", flat=True))

    direct_permissions = []
    for generic_permission in GroupObjectPermission.objects.filter(content_type = model_content_type):
        if int(generic_permission.object_pk) in valid_pks:
            direct_permission = new_permission_model(
                content_object_id = generic_permission.object_pk,
                group = generic_permission.group,
                permission = generic_permission.permission
            )
            direct_permissions.append(direct_permission)
    new_permission_model.objects.bulk_create(direct_permissions)

def migrate_permissions(apps, schema_editor):
    """Migrates the user and group permissions of a model to new, custom permission models with direct foreign keys."""
    Person = apps.get_model('persons', 'Person')
    PersonUserObjectPermission = apps.get_model("persons", "PersonUserObjectPermission")
    PersonGroupObjectPermission = apps.get_model("persons", "PersonGroupObjectPermission")
    create_direct_user_permissions_from_old_model(apps, Person, PersonUserObjectPermission)
    create_direct_group_permissions_from_old_model(apps, Person, PersonGroupObjectPermission)


def create_generic_user_permissions_from_newer_model(apps, model, new_permission_model):
    """Creates generic object user permissions for a given direct model."""
    UserObjectPermission = apps.get_model("guardian", "UserObjectPermission")
    ContentType = apps.get_model('contenttypes', 'ContentType')
    model_content_type = ContentType.objects.get_for_model(model)
    generic_permissions = []

    for direct_permission in new_permission_model.objects.all():
        generic_permission = UserObjectPermission(
            content_type = model_content_type,
            object_pk = direct_permission.content_object.pk,
            permission = direct_permission.permission,
            user = direct_permission.user
        )
        generic_permissions.append(generic_permission)
    UserObjectPermission.objects.bulk_create(generic_permissions, ignore_conflicts = True)

def create_generic_group_permissions_from_newer_model(apps, model, new_permission_model):
    """Creates generic object group permissions for a given direct model."""
    GroupObjectPermission = apps.get_model("guardian", "GroupObjectPermission")
    ContentType = apps.get_model('contenttypes', 'ContentType')
    model_content_type = ContentType.objects.get_for_model(model)
    generic_permissions = []

    for direct_permission in new_permission_model.objects.all():
        generic_permission = GroupObjectPermission(
            content_type = model_content_type,
            object_pk = direct_permission.content_object.pk,
            permission = direct_permission.permission,
            group = direct_permission.group
        )
        generic_permissions.append(generic_permission)
    GroupObjectPermission.objects.bulk_create(generic_permissions, ignore_conflicts = True)

def reverse_migration(apps, schema_editor):
    Person = apps.get_model('persons', 'Person')
    PersonUserObjectPermission = apps.get_model("persons", "PersonUserObjectPermission")
    PersonGroupObjectPermission = apps.get_model("persons", "PersonGroupObjectPermission")
    create_generic_user_permissions_from_newer_model(apps, Person, PersonUserObjectPermission)
    create_generic_group_permissions_from_newer_model(apps, Person, PersonGroupObjectPermission)


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0048_personuserobjectpermission_and_more'),
    ]

    operations = [
        migrations.RunPython(code = migrate_permissions, reverse_code = reverse_migration)
    ]
