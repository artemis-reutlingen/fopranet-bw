# Generated by Django 3.2.7 on 2021-10-15 15:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0006_auto_20211015_1351'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudySpecificTrainingOption',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=256, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StudySpecificTraining',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('person', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='study_speficic_trainings', to='persons.person', verbose_name='Person')),
                ('type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='persons.studyspecifictrainingoption', verbose_name='Type')),
            ],
            options={
                'unique_together': {('type', 'person')},
            },
        ),
    ]
