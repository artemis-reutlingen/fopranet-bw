# Generated by Django 3.2.7 on 2021-10-11 16:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('persons', '0001_initial'),
        ('practices', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='practice',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='practices.practice'),
        ),
        migrations.AddField(
            model_name='ownerrole',
            name='person',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='role_owner', to='persons.person'),
        ),
        migrations.AddField(
            model_name='fopranettraining',
            name='person',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fopranet_trainings', to='persons.person', verbose_name='Person'),
        ),
        migrations.AddField(
            model_name='fopranettraining',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='persons.fopranettrainingoption', verbose_name='Type'),
        ),
        migrations.AddField(
            model_name='doctorrole',
            name='person',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='role_doctor', to='persons.person'),
        ),
        migrations.AddField(
            model_name='doctoradditionaltraining',
            name='doctor_role',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='additional_trainings', to='persons.doctorrole', verbose_name='Doctor Role'),
        ),
        migrations.AddField(
            model_name='assistantrole',
            name='person',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='role_assistant', to='persons.person'),
        ),
        migrations.AddField(
            model_name='assistantadditionalqualification',
            name='assistant_role',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='additional_qualifications', to='persons.assistantrole', verbose_name='Assistant Role'),
        ),
        migrations.AlterUniqueTogether(
            name='fopranettraining',
            unique_together={('type', 'person')},
        ),
    ]
