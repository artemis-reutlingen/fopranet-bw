# Generated by Django 4.1.7 on 2023-09-06 14:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('practices', '0054_alter_communicationdigitalwithcolleagues_options_and_more'),
        ('persons', '0035_alter_assistantrole_options_alter_doctorrole_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='practice',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='practices.practice', verbose_name='Practice'),
        ),
    ]
