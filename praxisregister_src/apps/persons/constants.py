from django.db import models
from django.utils.translation import gettext_lazy as _


class RoleActions(models.TextChoices):
    MAKE_OWNER = 'A_OWNER', _('Assign owner role')
    REMOVE_OWNER = 'R_OWNER', _('Remove owner role')

    MAKE_DOCTOR = 'A_DOCTOR', _('Assign doctor role')
    REMOVE_DOCTOR = 'R_DOCTOR', _('Remove doctor role')

    MAKE_ASSISTANT = 'A_ASSISTANT', _('Assign assistant role')
    REMOVE_ASSISTANT = 'R_ASSISTANT', _('Remove assistant role')

    MAKE_TANDEM_DOCTOR = 'A_T_DOCTOR', _('Assign tandem doctor role') 
    MAKE_TANDEM_ASSISTANT = 'A_T_ASSISTANT', _('Assign tandem assistant role')

    REMOVE_TANDEM_DOCTOR = 'R_T_DOCTOR', _('Remove tandem doctor role')
    REMOVE_TANDEM_ASSISTANT = 'R_T_ASSISTANT', _('Remove tandem assistant role')


class Roles(models.TextChoices):
    OWNER = 'OWNER', _('Owner')
    DOCTOR = 'DOCTOR', _('Doctor')
    ASSISTANT = 'ASSISTANT', _('Assistant')
    TANDEM_DOCTOR = 'TANDEM_DOCTOR', _('Tandem doctor')
    TANDEM_ASSISTANT = 'TANDEM_ASSISTANT', _('Tandem assistant')


class WorkRelationshipType(models.TextChoices):
    OWNER = 'OWNER', _('Owner')
    EMPLOYEE = 'EMPLOYEE', _('Employee')

class DoctorTitles(models.TextChoices):
    DRMED = 'DRMED', _('Dr. med.')
    DRMEDUNIV = 'DRMEDUNIV', _('Dr. med. univ.')
    DRMEDDENT = 'DRMEDDENT', _('Dr. med. dent.')
    MUDR = 'MUDR', _('MUDr.')
