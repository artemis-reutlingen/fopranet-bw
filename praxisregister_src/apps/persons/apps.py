from django.apps import AppConfig


class PersonsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'persons'

    def ready(self):
        """
        Registers signals when app is loaded.
        """
        from . import signals