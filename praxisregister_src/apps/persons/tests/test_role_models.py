from django.test import TestCase

from persons.exceptions import RoleAlreadyAssigned, MutualExclusiveRolesException
from persons.models import Person
from practices.models import Practice
from persons.types import GenderType
from persons.constants import Roles

class PersonRolesTests(TestCase):
    def setUp(self) -> None:
        self.practice = Practice.objects.create(name="RandomPractice in the City")

        self.person = Person.objects.create(
            practice=self.practice,
            title="Dr.",
            first_name="Max",
            last_name="Mustermann",
            gender=GenderType.MALE,
        )

    def test_make_owner(self):
        self.assertFalse(self.person.is_owner)
        self.person.make_owner()
        self.assertIsNot(self.person.role_owner, None)
        self.assertIn(Roles.OWNER, self.person.roles)
        self.assertTrue(self.person.is_owner)

    def test_cannot_assign_role_twice(self):
        self.person.make_owner()
        self.assertRaises(RoleAlreadyAssigned, self.person.make_owner)

    def test_make_doctor(self):
        self.assertFalse(self.person.is_doctor)
        self.person.make_doctor()
        self.assertIsNot(self.person.role_doctor, None)
        self.assertIn(Roles.DOCTOR, self.person.roles)
        self.assertTrue(self.person.is_doctor)

    def test_remove_owner(self):
        self.person.make_owner()
        self.person.remove_owner()
        self.assertFalse(hasattr(self.person, "role_owner"))

    def test_make_assistant(self):
        self.assertFalse(self.person.is_assistant)
        self.person.make_assistant()
        self.assertIsNot(self.person.role_assistant, None)
        self.assertIn(Roles.ASSISTANT, self.person.roles)
        self.assertTrue(self.person.is_assistant)

    def test_doctor_assistant_roles_are_mutex(self):
        self.person.make_assistant()
        self.assertRaises(MutualExclusiveRolesException, self.person.make_doctor)

        self.person.role_assistant = None  # reset
        self.person.save()

        self.person.make_doctor()
        self.assertRaises(MutualExclusiveRolesException, self.person.make_assistant)

    def test_new_owner_is_in_practice_owners_property(self):
        new_owner = Person.objects.create(
            practice=self.practice,
            title="Dr.",
            first_name="Big",
            last_name="Chef",
            gender=GenderType.MALE,
        )
        new_owner.make_owner()
        random_guy = Person.objects.create(
            practice=self.practice,
            first_name="Torsten",
            last_name="Türsteher",
            gender=GenderType.MALE,
        )
        self.assertIn(new_owner, self.practice.owners)
        self.assertNotIn(random_guy, self.practice.owners)
