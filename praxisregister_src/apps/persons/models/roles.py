from datetime import datetime

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from persons.types import YesNoUnknowType
from track_changes.models import TrackChangesMixin
from importer.models import ImportableMixin
from utils.validators import LengthValidator


class AbstractResearcherRole(TrackChangesMixin, ImportableMixin):
    """
    Abstract Researcher Role for persons in practices that fulfill researcher roles.
    Defining shared fields in one place to reduce code duplication.
    TODO: Use django-polymorphic for roles, that will make the code more maintainable.
    """
    research_experience_year_of_last_project_participation = models.PositiveIntegerField(
        verbose_name=_('Year of last project'),
        validators=[
            MinValueValidator(1900),
            MaxValueValidator(datetime.now().year)],  # This will create a migration every new year (e.g. 2021->2022)
        blank=True, null=True
    )

    class Meta:
        abstract = True

class DoctorRole(AbstractResearcherRole):
    """
    Represents the function of a person as a doctor and its associated data fields.
    """
    person = models.OneToOneField('persons.Person', null=True, on_delete=models.CASCADE, related_name="role_doctor")
    lanr = models.CharField(
        verbose_name=_('LANR'),
        max_length=255,
        blank=True,
        default=""
    )
    year_of_licensure = models.IntegerField(verbose_name=_('Year of licensure'), blank=True, null=True, validators=[MaxValueValidator(2200), MinValueValidator(1900)])
    efn = models.CharField(
        verbose_name=_('EFN'),
        max_length = 15,
        blank=True,
        default="",
        validators = [LengthValidator(15)]
    )

    #Attended research training in the last 3 years. Because of this we also need to add a date field. I would rather use the year of the training, but the questionaire does not allow it.
    attended_research_training = models.CharField(verbose_name=_("Attended research training in the last 3 years"), default=YesNoUnknowType.UNKNOWN, max_length=32, choices=YesNoUnknowType.choices,)
    last_update_of_attended_research_training = models.DateField(verbose_name="Last Update on attended research training", blank=True, null=True)

    def __str__(self) -> str:
        name = self.person.full_name_with_title + " - " if self.person else "" + _('Doctor')
        return f"{name}"
    
    class Meta:
        verbose_name = _("Doctor")
        verbose_name_plural = _("Doctors")

class AssistantRole(AbstractResearcherRole):
    """
    Represents the function of a person as an assistant and its associated data fields.
    """
    person = models.OneToOneField('persons.Person', null=True, on_delete=models.CASCADE, related_name="role_assistant")
    vocational_training_type = models.CharField(
        verbose_name=_('Vocational training'),
        default="", blank=True,
        max_length=512,
    )

    def __str__(self):
        name = self.person.full_name_with_title + " - " if self.person else "" + _('Assistant')
        return f"{name}"
    
    class Meta:
        verbose_name = _("Assistant")
        verbose_name_plural = _("Assistants")


class OwnerRole(ImportableMixin):
    """
    Role model that designates persons as owner of the corresponding practice.
    """
    person = models.OneToOneField('persons.Person', null=True, on_delete=models.CASCADE, related_name="role_owner")
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Owner")
        verbose_name_plural = _("Owners")
