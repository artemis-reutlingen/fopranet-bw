from import_export import resources
from import_export.fields import Field

from accreditation.models import AccreditationTypeChoices
from persons.models import Person
from practices.models import Practice
from django.utils.translation import gettext_lazy as _


class PracticeResource(resources.ModelResource):
    """
    Defines which fields are to be exported for practice objects.
    Unfortunately, there is no clear way how to add columns/fields dynamically in django-import-export.
    This would allow to make handling exporting accreditation statuses more idiomatic.
    See the dehydrate_<field> methods below.
    """
    name = Field(attribute='name', column_name=_('Name'))
    bsnr = Field(attribute='identification_number', column_name=_('BSNR'))
    owners = Field(attribute='owners_names_formatted', column_name=_('Owner(s)'))
    contact_phone = Field(attribute='contact_phone', column_name=_('Phone'))
    contact_phone_alt = Field(attribute='contact_phone_alt', column_name=_('Phone (alt)'))
    contact_fax = Field(attribute='contact_fax', column_name=_('Fax'))
    contact_email = Field(attribute='contact_email', column_name=_('Email'))
    address_street = Field(attribute='street', column_name=_('Street'))
    address_house_number = Field(attribute='house_number', column_name=_('House number'))
    address_zip = Field(attribute='zip_code', column_name=_('ZIP'))
    address_city = Field(attribute='city', column_name=_('City'))
    address_county = Field(attribute='county__full_name', column_name=_('County'))
    tandem_assistant = Field(attribute='research_assistant__full_name_with_title', column_name=_('Tandem assistant'))
    tandem_doctor = Field(attribute='research_doctor__full_name_with_title', column_name=_('Tandem doctor'))
    education_authorization = Field(attribute='doctor_has_education_authorization', column_name=_('Doctor has education authorization'))

    is_contract_signed = Field(attribute='is_contract_signed', column_name=_('Contract is signed'))
    contract_signed_date = Field(attribute='contract_signed_date', column_name=_('Contract signed date'))
    structure_urbanisation_level = Field(attribute='structure_urbanisation_level', column_name=_('Urbanisation'))
    structure_practice_operating_until_1_2025 = Field(attribute='structure_practice_operating_until_1_2025', column_name=_('Operation until 1/2025 ensured'))

    # Accreditation Status export fields added manually with custom properties to get status
    accreditation_status_RP = Field(column_name=_('Accreditation FP'))
    accreditation_status_RP_PLUS = Field(column_name=_('Accreditation FP PLUS'))

    @staticmethod
    def _get_accreditation_status(practice, accreditation_type):
        for accreditation in practice.accreditations.all():
            if accreditation.accreditation_type.name == accreditation_type:
                return accreditation.get_status_display()
        return f"---"

    def dehydrate_accreditation_status_RP(self, practice):
        return self._get_accreditation_status(
            practice=practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )

    def dehydrate_accreditation_status_RP_PLUS(self, practice):
        return self._get_accreditation_status(
            practice=practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS
        )

    class Meta:
        model = Practice
        fields = []

class PracticeStructureDataResource(resources.ModelResource):
        
    structure_practice_type = Field(attribute='structure_practice_type', column_name=_('Practice Type'))
    structure_urbanisation_level = Field(attribute='structure_urbanisation_level', column_name=_('Urbanisation'))
    structure_full_care_mandate = Field(attribute='structure_full_care_mandate', column_name=_('Full care mandate'))
    structure_bills_per_quarter = Field(attribute='structure_bills_per_quarter', column_name=_('Treatment certificates per quarter'))
    structure_at_least_35_hours_openend = Field(attribute='structure_at_least_35_hours_openend', column_name=_('At least 35 hours opened per week'))
    structure_gp_range_of_services = Field(attribute='structure_gp_range_of_services', column_name=_('GP range of services more than 50%'))
    structure_practice_operating_until = Field(attribute='structure_practice_operating_until', column_name=_('Operation is guaranteed until'))
    structure_number_of_doctors_that_are_owners = Field(attribute='structure_number_of_doctors_that_are_owners', column_name=_('Number of owners working as doctors'))
    structure_number_of_doctors_that_are_employees = Field(attribute='structure_number_of_doctors_that_are_employees', column_name=_('Number of employed doctors'))
    structure_number_of_doctors_that_are_in_training = Field(attribute='structure_number_of_doctors_that_are_in_training', column_name=_('Number of doctors in training'))
    structure_full_time_equivalent_for_doctors = Field(attribute='structure_full_time_equivalent_for_doctors', column_name=_('Full-time equivalent (exclusive Doctors in further training)'))
    structure_full_time_equivalent_for_doctors_in_education = Field(attribute='structure_full_time_equivalent_for_doctors_in_education', column_name=_('Full-time equivalent doctors in training'))
    structure_number_of_non_gp_doctors_that_are_in_the_practice = Field(attribute='structure_number_of_non_gp_doctors_that_are_in_the_practice', column_name=_('Number of non-GP doctors in the practice'))
    structure_number_of_employed_medical_assistants = Field(attribute='structure_number_of_employed_medical_assistants', column_name=_('Number of employed MFAs'))
    structure_mfa_research_project_interest = Field(attribute='structure_mfa_research_project_interest', column_name=_('Are MFAs interested in participating in research projects'))
    structure_number_of_employed_administrative_assistants = Field(attribute='structure_number_of_employed_administrative_assistants', column_name=_('Number of administrative employees'))
    structure_number_of_medical_assistants_in_training = Field(attribute='structure_number_of_medical_assistants_in_training', column_name=_('Number of MFAs in training'))
    structure_full_time_equivalent_for_assistants = Field(attribute='structure_full_time_equivalent_for_assistants', column_name=_('Full-time equivalent of assistants'))
    structure_non_gp_doctors_are_working_in_the_practice = Field(attribute='structure_non_gp_doctors_are_working_in_the_practice', column_name=_('Non-GP doctors in the practice'))
    services_home_visits = Field(attribute='services_home_visits', column_name=_('Regular home visits'))
    services_nursing_home_visits = Field(attribute='services_nursing_home_visits', column_name=_('Regular nursing home visits'))

    total_number_of_doctors = Field(attribute='total_number_of_doctors', column_name=_('Total number of doctors'))
    total_number_of_staff = Field(attribute='total_number_of_staff', column_name=_('Total number of staff'))

    class Meta:
        model = Practice
        fields = []

class PersonResource(resources.ModelResource):
    title = Field(attribute='title', column_name=_('Title'))
    first_name = Field(attribute='first_name', column_name=_('First name'))
    last_name = Field(attribute='last_name', column_name=_('Last name'))
    former_name = Field(attribute='former_name', column_name=_('Former name'))
    year_of_birth = Field(attribute='year_of_birth', column_name=_('Year of birth'))
    gender = Field(attribute='gender', column_name=_('Gender'))
    email = Field(attribute='email', column_name=_('Email'))
    phone = Field(attribute='phone', column_name=_('Phone'))
    weekly_working_hours = Field(attribute='weekly_working_hours', column_name=_('Weekly working hours'))

    practice = Field(attribute='practice__name', column_name=_('Practice name'))
    is_owner = Field(attribute='is_owner', column_name=_('Is owner'))
    is_doctor = Field(attribute='is_doctor', column_name=_('Is doctor'))
    is_tandem_doctor = Field(attribute='is_tandem_doctor', column_name=_('Is tandem doctor'))
    is_assistant = Field(attribute='is_assistant', column_name=_('Is assistant'))
    is_tandem_assistant = Field(attribute='is_tandem_assistant', column_name=_('Is tandem assistant'))

    full_name = Field(attribute='full_name_with_title', column_name=_('Full name'))

    class Meta:
        model = Person
        fields = []
