from django.test import TestCase
from django.urls import reverse

from utils.utils_test import setup_test_basics


class ExportViewTest(TestCase):

    def setUp(self) -> None:
        self.institute, self.group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()
        self.client.login(username="tester", password="password")

    def test_exporter_practices_uses_correct_template(self):
        response = self.client.get(reverse('exporter:practices'))
        self.assertTemplateUsed(response, "exporter/forms/practices.html")

    def test_exporter_persons_uses_correct_template(self):
        response = self.client.get(reverse('exporter:persons'))
        self.assertTemplateUsed(response, "exporter/forms/persons.html")

