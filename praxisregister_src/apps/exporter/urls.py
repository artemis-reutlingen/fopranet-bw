from django.urls import path

from .views import *

app_name = 'exporter'

urlpatterns = [
    path('', ExporterIndexView.as_view(), name='index'),
    path('practices/', export_practices, name='practices'),
    path('practices/structure', export_practice_structure_data, name='practices-structure'),
    path('persons/', export_persons, name='persons'),
]
