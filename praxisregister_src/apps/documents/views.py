import os

from django.core.exceptions import PermissionDenied
from django.dispatch import Signal
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from documents.forms import DocumentUploadForm
from documents.models import Document
from practices.models import Practice
from utils.view_mixins import ManagePracticePermissionRequired

documents_log=Signal()

class DocumentCreate(ManagePracticePermissionRequired, CreateView):
    """
    View for creating new Documents for a Practice.
    """
    model = Document
    form_class = DocumentUploadForm
    template_name = 'documents/forms/create_document.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        self.document = form.save(commit=False)
        self.document.practice = self.get_practice_object()
        self.document.uploaded_by = self.request.user
        self.document.save()

        return super().form_valid(form)

    def get_success_url(self):
        documents_log.send(sender=self.__class__, username=self.request.user.username, document_id=self.document.id, practice_id=self.get_practice_object().id)
        return reverse_lazy('practices:document-list', kwargs={'pk': self.kwargs['pk']})


class DocumentEdit(ManagePracticePermissionRequired, UpdateView):
    """
    View for Editing existing Documents for a Practice.
    """
    model = Document
    form_class = DocumentUploadForm
    template_name = 'documents/forms/create_document.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return self.get_object().practice

    def get_success_url(self):
        documents_log.send(sender=self.__class__, username=self.request.user.username, document_id=self.object.id, practice_id=self.get_practice_object().id)
        return reverse_lazy('practices:document-list', kwargs={'pk': self.get_practice_object().id})


class DocumentDelete(ManagePracticePermissionRequired, DeleteView):
    """
    View for deleting a certain Document, protected by ManagePracticePermission.
    """
    model = Document
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return self.get_object().practice

    def get_success_url(self):
        practice_id = self.get_practice_object().id
        documents_log.send(sender=self.__class__, username=self.request.user.username, document_id=self.object.id, practice_id=self.get_practice_object().id)
        return reverse_lazy('practices:document-list', kwargs={'pk': practice_id})


class PracticeDocuments(ManagePracticePermissionRequired, ListView):
    """
    Lists all Document objects for a given Practice.
    """
    model = Document
    paginate_by = 15
    template_name = 'practices/documents/practice_document_list.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        return self.get_practice_object().document_set.all().order_by('-created_at')


def download_document_file(request, pk):
    """
    Allows downloading uploaded Document files.
    Actual download is handled by nginx, django is checking permissions.
    """
    document = Document.objects.get(id=pk)
    practice = document.practice

    if not request.user.has_perm("practices.manage_practice", practice):
        raise PermissionDenied

    file = document.file

    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={os.path.basename(file.name)}"
    response['X-Accel-Redirect'] = f"{file.url}"
    class DownloadDocumentFile: pass
    documents_log.send(sender=DownloadDocumentFile, username=request.user.username, document_id=document.id, practice_id=practice.id)
    return response
