from django.core.exceptions import PermissionDenied, ViewDoesNotExist
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import FormView, UpdateView, DeleteView
from django_filters.views import FilterView
from django.dispatch import Signal

from contactnotes.filters import ContactNotesFilter
from contactnotes.forms import ContactNoteForm
from contactnotes.models import ContactNote
from practices.models import Practice
from utils.view_mixins import ManagePracticePermissionRequired

contactnotes_log=Signal()

class ContactNoteCreate(ManagePracticePermissionRequired, FormView):
    form_class = ContactNoteForm
    template_name = 'contact_notes/forms/create_note.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        self.contact_note = form.save(commit=False)
        self.contact_note.practice = self.get_practice_object()
        self.contact_note.author = self.request.user
        self.contact_note.save()
        

        form.save_m2m()  # Apparently django-taggit (for tags attribute) needs us do perform model creation like this.

        return super().form_valid(form)

    def get_success_url(self):
        contactnotes_log.send(sender=self.__class__, username=self.request.user.username, contact_note_id=self.contact_note.id, practice_id=self.get_practice_object().id)
        return reverse_lazy('practices:contactnote-list', kwargs={'pk': self.kwargs['pk']})

    def get_form_kwargs(self):
        person_names = [
            person.full_name_with_title for person in self.get_practice_object().persons.all().order_by('last_name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['person_data_list'] = person_names
        return kwargs


class ContactNoteEdit(ManagePracticePermissionRequired, UpdateView):
    model = ContactNote
    form_class = ContactNoteForm
    template_name = 'contact_notes/forms/create_note.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return self.get_object().practice

    def get_success_url(self):
        contactnotes_log.send(sender=self.__class__, username=self.request.user.username, contact_note_id=self.object.id, practice_id=self.get_practice_object().id)
        return reverse_lazy('practices:contactnote-list', kwargs={'pk': self.get_practice_object().id})

    def get_form_kwargs(self):
        person_names = [
            person.full_name_with_title for person in self.get_practice_object().persons.all().order_by('last_name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['person_data_list'] = person_names
        return kwargs


class PracticeContactNotes(ManagePracticePermissionRequired, FilterView):
    """
    Lists all ContactNote objects for a given Practice.
    """
    model = ContactNote
    paginate_by = 10
    template_name = 'practices/contactnotes/practice_contactnote_list.html'
    filterset_class = ContactNotesFilter

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        return self.get_practice_object().contact_notes.order_by('-created_at')

class ContactNoteDelete(ManagePracticePermissionRequired, DeleteView):
    model = ContactNote
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        contactnotes_log.send(sender=self.__class__, username=self.request.user.username, contact_note_id=self.object.id, practice_id=self.get_object().practice.id)
        return reverse_lazy('practices:contactnote-list', kwargs={'pk': self.object.practice.id})

def download_contact_note_attachment_file(request, pk, attachment_id):
    """
    Allows downloading uploaded ContactNote attachments.
    Actual download is handled by nginx, django is checking permissions.
    """
    contact_note = ContactNote.objects.get(id=pk)
    practice = contact_note.practice

    if not request.user.has_perm("practices.manage_practice", practice):
        raise PermissionDenied

    file = None

    if attachment_id == 1 and contact_note.attachment_1:
        file = contact_note.attachment_1
    elif attachment_id == 2 and contact_note.attachment_2:
        file = contact_note.attachment_2
    elif attachment_id == 3 and contact_note.attachment_3:
        file = contact_note.attachment_3
    elif attachment_id == 4 and contact_note.attachment_4:
        file = contact_note.attachment_4
    else:
        raise ViewDoesNotExist  # TODO: Set proper exception / HTTP error

    file_extension = file.name.split(".")[-1]  # Probably not very robust
    download_filename = f"ContactNote_{contact_note.id}_attachment_{attachment_id}.{file_extension}"

    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={download_filename}"
    response['X-Accel-Redirect'] = file.url
    class DownloadContactNoteAttachmentFile: pass
    contactnotes_log.send(sender=DownloadContactNoteAttachmentFile, username=request.user.username, contact_note_id=contact_note.id, practice_id=practice.id)

    return response
