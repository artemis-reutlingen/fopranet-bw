import django_filters
from taggit.forms import TagField

from contactnotes.models import ContactNote


class TagFilter(django_filters.CharFilter):
    field_class = TagField

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('lookup_expr', 'in')
        super().__init__(*args, **kwargs)


class ContactNotesFilter(django_filters.FilterSet):
    tags = TagFilter(field_name='tags__slug')

    class Meta:
        model = ContactNote
        fields = ['tags']