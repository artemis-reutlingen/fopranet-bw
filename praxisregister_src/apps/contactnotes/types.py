from django.db import models
from django.utils.translation import gettext_lazy as _


class ContactMedium(models.TextChoices):
    EMAIL = 'EMAIL', _('Email')
    LETTER = 'LETTER', _('Letter')
    PHONE = 'PHONE', _('Phone')
    FAX = 'FAX', _('Fax')
    OTHER = 'OTHER', _('Other')


class ContactDirection(models.TextChoices):
    INCOMING = 'INCOMING', _('incoming')
    OUTGOING = 'OUTGOING', _('outgoing')