# Generated by Django 3.2.7 on 2021-10-11 16:54

import contactnotes.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactNote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('contact_person', models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='Contact person')),
                ('note', models.TextField(default='', verbose_name='Note')),
                ('contact_medium', models.CharField(choices=[('EMAIL', 'Email'), ('LETTER', 'Letter'), ('PHONE', 'Phone'), ('FAX', 'Fax'), ('OTHER', 'Other')], default='PHONE', max_length=128, verbose_name='Medium')),
                ('direction', models.CharField(choices=[('INCOMING', 'incoming'), ('OUTGOING', 'outgoing')], default='OUTGOING', max_length=128, verbose_name='Direction')),
                ('contact_time', models.TimeField(blank=True, null=True)),
                ('contact_date', models.DateField(blank=True, null=True)),
                ('attachment_1', models.FileField(blank=True, null=True, upload_to=contactnotes.models.generate_attachment_upload_path, verbose_name='File 1')),
                ('attachment_2', models.FileField(blank=True, null=True, upload_to=contactnotes.models.generate_attachment_upload_path, verbose_name='File 2')),
                ('attachment_3', models.FileField(blank=True, null=True, upload_to=contactnotes.models.generate_attachment_upload_path, verbose_name='File 3')),
                ('attachment_4', models.FileField(blank=True, null=True, upload_to=contactnotes.models.generate_attachment_upload_path, verbose_name='File 4')),
            ],
        ),
    ]
