# Generated by Django 3.2.7 on 2022-12-19 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contactnotes', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactnote',
            name='contact_date',
            field=models.DateField(blank=True, null=True, verbose_name='Contact date'),
        ),
        migrations.AlterField(
            model_name='contactnote',
            name='contact_time',
            field=models.TimeField(blank=True, null=True, verbose_name='Contact time'),
        ),
    ]
