from django.urls import path
from todos import views

app_name = 'todos'

urlpatterns = [
    path('', views.ToDoList.as_view(), name='list'),
    path('<int:pk>/mark-as-done/', views.ToDoMarkAsDone.as_view(), name='mark-as-done'),
    #path('<int:pk>/', views.ToDoDetail.as_view(), name='detail'),
    path('<int:pk>/edit', views.ToDoEdit.as_view(), name='edit'),
]
