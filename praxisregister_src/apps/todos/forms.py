from django.forms import CheckboxSelectMultiple, ModelForm, ModelMultipleChoiceField
from todos.models import ToDo
from users.models import User
from django.utils.translation import gettext_lazy as _

class ToDoForm(ModelForm):

    def __init__(self, *args, **kwargs):
        practice = kwargs.pop("practice")
        super().__init__(*args, **kwargs)
        self.fields["assignees"] = ModelMultipleChoiceField(
            queryset=User.objects.get_managing_users_for_practice(practice),
            label=_('Assignees'),
            widget=CheckboxSelectMultiple()
        )

    class Meta:
        model = ToDo
        fields = [
            'title',
            'description',
            'assignees'
        ]