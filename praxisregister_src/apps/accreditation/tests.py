import datetime
from django.test import TestCase
from django.urls import reverse

from accreditation.models import AccreditationTypeChoices, Accreditation
from accreditation.views import AccreditationCreateForm
from accreditation.types import AccreditationStatus
from institutes.models import Institute
from practices.models import Practice
from accreditation.types import AccreditationAttributeChoices
from users.models import User
from utils.utils_test import setup_test_basics


class ModelsTest(TestCase):
    def setUp(self) -> None:
        self.institute, self.group, self.user, [self.practice, self.practice_2] = setup_test_basics()

    def test_add_accreditations_to_practice_with_polymorphic_manager(self):
        self.practice.assigned_institute = self.institute
        self.practice.save()
        self.assertEqual(self.practice.accreditations.count(), 0)
        Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS
        )
        self.assertEqual(self.practice.accreditations.count(), 2)


class FormsTest(TestCase):
    def test_accreditation_create_form(self):
        form = AccreditationCreateForm(data={'accreditation_type': AccreditationTypeChoices.RESEARCH_PRACTICE})
        form.full_clean()
        self.assertTrue(form.is_valid())


class ViewsTest(TestCase):
    def setUp(self) -> None:
        institute = Institute.objects.create_institute_with_group("x", "x", "x")
        User.objects.create_user_for_institute(institute=institute, username="x", password="x")
        self.practice = Practice.objects.create_practice_for_institute(institute=institute, practice_name="x")
        self.client.login(username="x", password="x")

    def test_create_accreditation_for_practice(self):
        self.client.post(
            reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}),
            data={"accreditation_type": AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS}
        )
        self.assertEqual(self.practice.accreditations.count(), 1)

    def test_create_accreditation_view_is_filtering_already_instantiated_accreditation_type_choices(self):
        response = self.client.get(reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}))

        # Initially all choices are available
        for choice in AccreditationTypeChoices.choices:
            self.assertIn(choice, response.context['form'].fields['accreditation_type'].choices)

        self.client.post(
            reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}),
            data={"accreditation_type": AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS}
        )
        response = self.client.get(reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}))

        # RESEARCH_PRACTICE_PLUS now shouldn't be an available choice in the form
        self.assertNotIn(
            AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS,
            response.context['form'].fields['accreditation_type'].choices
        )

        self.client.post(
            reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}),
            data={"accreditation_type": AccreditationTypeChoices.RESEARCH_PRACTICE}
        )
        response = self.client.get(reverse('practices:accreditation-add', kwargs={'pk': self.practice.id}))

        # Choices should be empty by now
        self.assertEqual(
            [],
            response.context['form'].fields['accreditation_type'].choices
        )

    def test_accredited_at_date_gets_set_in_edit_view_when_set_to_finished(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.client.post(
            reverse('accreditations:edit', kwargs={'pk': accreditation.id}),
            data={"status": AccreditationStatus.ACCREDITED}
        )
        accreditation.refresh_from_db()
        self.assertIsNotNone(accreditation.accredited_at)

    def test_accredited_at_date_gets_set_in_edit_view_when_set_to_pending(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.client.post(
            reverse('accreditations:edit', kwargs={'pk': accreditation.id}),
            data={"status": AccreditationStatus.PENDING}
        )
        accreditation.refresh_from_db()
        self.assertIsNotNone(accreditation.accredited_at)

    def test_accredited_at_date_gets_not_set_in_edit_view_when_set_to_not_accredited(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.client.post(
            reverse('accreditations:edit', kwargs={'pk': accreditation.id}),
            data={"status": AccreditationStatus.NOT_ACCREDITED}
        )
        accreditation.refresh_from_db()
        self.assertIsNone(accreditation.accredited_at)

    def test_edit_date(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.client.post(
            reverse('accreditations:edit', kwargs={'pk': accreditation.id}),
            data={"status": AccreditationStatus.ACCREDITED}
        )
        response_post = self.client.post(
            reverse('accreditations:edit-accredited-at', kwargs={'pk': accreditation.id}),
            data={"accredited_at": "2020-01-01"}
        )
        self.assertEqual(response_post.status_code, 302)
        self.assertRedirects(response_post, reverse('accreditations:detail', kwargs={'pk': accreditation.id}))
        accreditation.refresh_from_db()
        self.assertEqual(accreditation.accredited_at, datetime.date(2020, 1, 1))

    def test_delete_accreditation(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.assertEqual(self.practice.accreditations.count(), 1)
        self.client.post(
            reverse('accreditations:delete', kwargs={'pk': accreditation.id}),
        )
        self.assertEqual(self.practice.accreditations.count(), 0)

    def test_detail_checklist(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        response = self.client.get(reverse('accreditations:detail', kwargs={'pk': accreditation.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['attr_dict']), 0)

    #creates a new accreditation with a checklist and adds attributes to it
    def test_detail_checklist_edit(self):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        response = self.client.get(reverse('accreditations:edit-checklist', kwargs={'pk': accreditation.id}))
        self.assertEqual(response.status_code, 200)

        #request will succeed
        response_post = self.client.post(reverse('accreditations:edit-checklist', kwargs={'pk': accreditation.id}),
            data={
                "attribute": [
                    AccreditationAttributeChoices.HAS_ANY_DMP_IMPLEMENTED,
                ],
                "practice_has_to_operate_until": "2020-01-01"
            }
        )
        self.assertEqual(response_post.status_code, 302)
        from accreditation.models import AccreditationAttribute
        attributes = AccreditationAttribute.objects.filter(accreditation_type=accreditation.accreditation_type).values_list("name", flat=True)
        self.assertEqual(attributes.count(), 2)
