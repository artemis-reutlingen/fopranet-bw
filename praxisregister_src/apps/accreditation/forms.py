from django import forms
from django.forms.widgets import CheckboxSelectMultiple
from django.utils.translation import gettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML
from accreditation.models import AccreditationAttributeChoices, Accreditation, AccreditationType
from events.models import EventInfo

class AccreditationEditChecklistForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['attribute'].choices = self.get_filtered_attribute_choices()
        self.fields['attribute'].initial = list(self.instance.accreditation_type.accreditationattribute_set.values_list("name", flat=True))
        self.fields['event_participiation'].queryset = EventInfo.objects.get_events_for_user(user)
        self.fields['event_participiation'].initial = self.instance.required_events.all()

        # Create a crispy form helper to collapse the event selection
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.trans_required_events = _("Required events")
        self.helper.layout = Layout(
            Field('attribute'),
            Field('practice_has_to_operate_until'),
            HTML('<button class="btn btn-primary mb-3" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEvents" aria-expanded="false" aria-controls="collapseEvents"><i class="fa-solid fa-angle-down"></i> {{ trans_required_events }}</button>'),
            HTML("""<div class="collapse" id="collapseEvents">"""),
            Field('event_participiation'),
            HTML("""</div><br>"""),
        )

    attribute = forms.MultipleChoiceField(
        widget=CheckboxSelectMultiple(),
        required=False,
        choices=[], # will be set in __init__
        label= _("Select the attributes that are necessary for the accreditation type.")
    )

    practice_has_to_operate_until = forms.DateField(required=False, label=_('Operation must be guaranteed until'),
        widget=forms.DateInput(
            format='%Y-%m-%d',
            attrs={
                'class': 'form-control',
                'placeholder': 'Select a date',
                'type': 'date',
            }
        )
    )

    event_participiation = forms.ModelMultipleChoiceField(
        queryset=None, # will be set in __init__
        required=False,
        label=_("Select the required events."),
        widget=CheckboxSelectMultiple()
    )

    # remove fields that require separate user input
    def get_filtered_attribute_choices(self):
        remove_list = [AccreditationAttributeChoices.OPERATES_UNTIL, AccreditationAttributeChoices.PARTICIPATED_IN_EVENTS]
        filtered = list(filter(lambda x: x[0] not in remove_list , AccreditationAttributeChoices.choices))
        return filtered

    def clean(self):
        cleaned_data = super().clean()
        attributes = cleaned_data.get('attribute')
        if attributes:
            if cleaned_data.get('practice_has_to_operate_until'):
                attributes.append(AccreditationAttributeChoices.OPERATES_UNTIL)
            if cleaned_data.get('event_participiation'):
                attributes.append(AccreditationAttributeChoices.PARTICIPATED_IN_EVENTS)
        return cleaned_data
    
    def save(self, commit = True):
        instance = super().save(commit)
        instance.accreditation_type.practice_has_to_operate_until = instance.practice_has_to_operate_until
        instance.accreditation_type.save()
        return instance
    
    class Meta:
        model = Accreditation
        fields = ['attribute', 'practice_has_to_operate_until']

