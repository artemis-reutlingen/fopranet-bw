from django import template
from django.utils.safestring import mark_safe
from accreditation.models import AccreditationAttributeChoices

register = template.Library()


@register.filter
def checkmark(value):
    """
    Creates an HTML icon checkmark (using fontawesome) from a boolean value.
    """
    returned_html_snippet = ''
    if value == 'YES' or value == True or value == "true":
        returned_html_snippet = '<i class="fa fa-check-circle text-success float-end"></i>'
    elif value == 'NO' or value == False or value == "false":
        returned_html_snippet = '<i class="fa fa-times-circle text-danger float-end"></i>'
    elif value == 'UNKNOWN' or value == None :
        returned_html_snippet = '<i class="fa-solid fa-circle-question text-secondary float-end"></i>'
    else:
        returned_html_snippet = f'<span class="float-end">{value}</span>'
    return mark_safe(returned_html_snippet)

@register.filter
def get_value(accreditation_criteria_dict, attribute):
    return accreditation_criteria_dict[attribute.name]

@register.filter
def get_display_name(accreditation_attribute):
    return AccreditationAttributeChoices(accreditation_attribute.name).label