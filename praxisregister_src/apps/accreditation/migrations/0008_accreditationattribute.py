# Generated by Django 4.1.6 on 2023-03-28 13:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accreditation', '0007_alter_accreditation_polymorphic_ctype'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccreditationAttribute',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attribute', models.CharField(blank=True, choices=[('HAS_FULL_CARE_MANDATE', 'Full care mandate'), ('HAS_MORE_THAN_500_TREATMENT_CERTIFICATES_PER_QUARTER', 'At least 500 treatment certificates per quarter'), ('IS_OPEN_AT_LEAST_35_HOURS_PER_WEEK', 'At least 35 hours opened per week'), ('OFFERS_HOME_VISITS', 'Regular home visits'), ('HAS_ANY_DMP_IMPLEMENTED', 'Disease Management Programs'), ('NUMBER_OF_IMPLEMENTED_DMPS', 'Number of implemented Disease Management Programs'), ('OPERATES_UNTIL', 'Operation of practice ensured until'), ('HAS_AUTOMATED_BACKUPS', 'Backup processes established'), ('HAS_WORKSTATION_AVAILABLE', 'Workstation with access to PDMS and internet'), ('IS_USING_EPA', 'EPA is used for medical documentation'), ('IS_OPEN_FOR_DATA_EXPORT', 'Open for automated data export'), ('TANDEM_DOCTOR_IS_AVAILABLE', 'Research doctor available'), ('TANDEM_DOCTOR_HOURS_GTE_15', 'Research doctor works more than 15h per week'), ('TANDEM_ASSISTANT_IS_AVAILABLE', 'Research assistant available'), ('TANDEM_ASSISTANT_HOURS_GTE_15', 'Research assistant works more than 15h per week'), ('HAS_GP_SERVICE_SPECTRUM', 'GP range of services more than 50%'), ('HAS_SIGNED_CONTRACT', 'Signed'), ('TANDEM_DOCTOR_ATTENDED_TRAINING', 'Research assistant attended training'), ('TANDEM_ASSISTANT_ATTENDED_TRAINING', 'Research doctor attended training')], max_length=128, verbose_name='Accreditation attribute')),
                ('accreditation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accreditation.accreditation')),
            ],
        ),
    ]
