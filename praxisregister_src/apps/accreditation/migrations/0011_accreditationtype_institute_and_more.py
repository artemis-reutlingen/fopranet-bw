# Generated by Django 4.1.6 on 2023-03-29 12:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('institutes', '0001_initial'),
        ('accreditation', '0010_accreditationtype_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='accreditationtype',
            name='institute',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='institutes.institute', verbose_name='Institute'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='accreditationtype',
            name='name',
            field=models.CharField(choices=[('RESEARCH_PRACTICE', 'Research practice'), ('RESEARCH_PRACTICE_PLUS', 'Research practice plus')], max_length=128, verbose_name='Type'),
        ),
    ]
