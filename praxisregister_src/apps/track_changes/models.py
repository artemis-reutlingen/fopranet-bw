from typing import Any

from dirtyfields import DirtyFieldsMixin
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone


class ChangedField(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    modified_at = models.DateTimeField(default=timezone.now)
    is_created = models.BooleanField()
    field_name = models.CharField(max_length=256)
    field_type = models.CharField(max_length=256)
    old_value = models.TextField(null=True, blank=True)
    new_value = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.content_type} - {self.object_id}'

class TrackChangesMixin(DirtyFieldsMixin, models.Model):

    updated_at = models.DateTimeField(default=timezone.now, editable=False)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    
    def save(self, *args, **kwargs):
        is_created = self.pk is None
        #Get the changed data fields only
        changed_data = self.get_dirty_fields(verbose=True, check_relationship=True)
        if not "updated_at" in changed_data:
            self.updated_at = timezone.now()

        super().save(*args, **kwargs)
        changed_fields = []
        if changed_data:
            for field_name, value in changed_data.items():
                changed_fields.append(ChangedField(content_object=self, is_created=is_created, field_name=field_name, field_type=type(value["current"]).__name__, old_value=value["saved"], new_value=value["current"]))
            ChangedField.objects.bulk_create(changed_fields)

    class Meta:
        abstract = True