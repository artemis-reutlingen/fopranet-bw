from django.test import TestCase
from django.urls import reverse

from practices.models import Practice
from processes.models import ProcessDefinition, Process, Step
from utils.utils_test import setup_test_basics


class StepManipulationTests(TestCase):
    def setUp(self) -> None:
        institute, group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()
        self.process_def = ProcessDefinition.objects.create_process_definition_for_user(
            user=self.user,
            title="ProcessDef Foo",
        )

        for step_number in range(1, 4):
            self.process_def.add_step_definition(
                step_name=f"#{step_number}",
                step_description="Foo",
            )

        self.client.login(username="tester", password="password")

    def test_step_mark_as_done(self):
        process = Process.objects.create(process_definition=self.process_def, practice=self.practice_1)
        first_step = process.steps[0]

        self.client.post(reverse('processes:step-mark-as-done', args=[first_step.id]))

        first_step.refresh_from_db()
        self.assertTrue(first_step.is_done)

    def test_step_reset(self):
        process = Process.objects.create(process_definition=self.process_def, practice=self.practice_1)
        first_step = process.steps[0]
        first_step.mark_as_done()

        self.client.post(reverse('processes:step-reset', args=[first_step.id]))

        first_step.refresh_from_db()
        self.assertFalse(first_step.is_done)

    def test_move_step_definition_up(self):
        step_defs = self.process_def.step_definitions
        move_up_id = step_defs[0].id

        response = self.client.post(
            reverse('processes:step-definition-move-up', args=[move_up_id]),
        )
        self.assertEqual(response.status_code, 302)  # successfull post followed by redirect
        # refresh
        step_defs = self.process_def.step_definitions
        self.assertEqual(step_defs[0].name, "#2")
        self.assertEqual(step_defs[1].name, "#1")

    def test_move_step_definition_down(self):
        step_defs = self.process_def.step_definitions
        move_down_id = step_defs[2].id

        response = self.client.post(
            reverse('processes:step-definition-move-down', args=[move_down_id]),
        )

        self.assertEqual(response.status_code, 302)  # successfull post followed by redirect

        # refresh
        step_defs = self.process_def.step_definitions
        self.assertEqual(step_defs[1].name, "#3")
        self.assertEqual(step_defs[2].name, "#2")


class ViewTests(TestCase):
    def setUp(self) -> None:
        institute, group, user, [self.practice_1, self.practice_2] = setup_test_basics()

        self.process_def = ProcessDefinition.objects.create_process_definition_for_user(
            user=user, title="ProcessDef Foo",
        )

        for step_number in range(0, 4):
            self.process_def.add_step_definition(
                step_name=f"FooBarStep#{step_number}",
                step_description="Lorem Ipsum",

            )
        self.process_def.is_draft = False
        self.process_def.save()
        self.client.login(username="tester", password="password")

    def test_can_delete_process_definition(self):
        """ Checks a regression bug fix """

        response = self.client.post(
            reverse('processes:definition-delete', args=[self.process_def.id])
        )
        self.assertRedirects(response, reverse('processes:definition-list'))

    def test_bulk_create_process_instances(self):
        self.assertEqual(Process.objects.count(), 0)
        practice_ids = [p.id for p in Practice.objects.all()]
        response = self.client.post(
            reverse('processes:definition-instantiate', args=[self.process_def.id]),
            {"selection": practice_ids}
        )
        self.assertEqual(Process.objects.count(), 2)

        # Check if steps have been created (2 processes * 4 steps = 8 total expected)
        self.assertEqual(Step.objects.count(), 8)

    def test_instantiated_processes_overview_is_rendered(self):
        # Create some process instances to be listed in the view
        Process.objects.create(process_definition=self.process_def, practice=self.practice_1)
        Process.objects.create(process_definition=self.process_def, practice=self.practice_2)

        # Check TemplateUsed doesn't raise any exceptions (implicit via assertTemplateUsed)
        response = self.client.get(reverse('processes:process-instances', kwargs={'pk': self.process_def.id}))
        self.assertTemplateUsed(
            response,
            template_name='processes/process_instance_list.html'
        )

    def test_process_completion_percentage_method(self):
        p1 = Process.objects.create(
            process_definition=self.process_def,
            practice=self.practice_1
        )
        p2 = Process.objects.create(
            process_definition=self.process_def,
            practice=self.practice_2

        )
        p2_steps = p2.steps
        for step in p2_steps:
            step.mark_as_done()

        self.assertEqual(p1.completed_in_percent, 0)
        self.assertEqual(p2.completed_in_percent, 100)

    def test_add_process(self):
        data = {
            "process_definition": self.process_def.id
        }
        self.assertEqual(Process.objects.count(), 0)
        self.client.post(reverse('practices:process-participation-create', kwargs={"pk": self.practice_1.id}), data=data)
        self.assertEqual(Process.objects.count(), 1)
