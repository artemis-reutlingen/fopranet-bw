from django import forms
from processes.models import Process, Step
from django.utils.translation import gettext_lazy as _

class ProcessDeadlineForm(forms.ModelForm):
    class Meta:
        model = Process
        fields = ['deadline']
        widgets = {
            'deadline': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': _('Select a date'),
                    'type': 'date'
                }
            ),
        }

class StepDateForm(forms.ModelForm):
    class Meta:
        model = Step
        fields = [
            'done_at',
        ]
        widgets = {
            'done_at': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': _('Select a date'),
                    'type': 'date'
                }
            ),
        }
