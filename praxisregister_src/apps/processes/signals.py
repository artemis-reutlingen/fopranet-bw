from processes.models import Process, Step, StepDefinition, ProcessDefinition
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

#This signal is called, when a process is added to a practice
@receiver(post_save, sender=Process)
def create_steps_for_process_instance(sender, instance: Process, created, **kwargs):
    if created:
        step_definitions = instance.process_definition.step_definitions
        for step_def in step_definitions:
            Step.objects.create(process=instance, step_definition=step_def)


#This signal is called, when a process step is deleted
@receiver(post_delete, sender=StepDefinition)
def update_position_indexes_of_step_definitions_after_delete(sender, instance: StepDefinition, **kwargs):
    process_definition = instance.process_definition
    process_definition.update_all_position_indexes()