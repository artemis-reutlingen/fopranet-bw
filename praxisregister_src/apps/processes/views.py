from typing import Any
from datetime import date
from django import forms, dispatch
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Exists, OuterRef
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DetailView, UpdateView, View, DeleteView, ListView, TemplateView, FormView
from django_tables2 import RequestConfig

from guardian.mixins import PermissionRequiredMixin

from practices.models import Practice
from practices.filters import PracticeFilter
from practices.tables import SelectPracticesTable
from processes.exceptions import StepDefinitionPositionError
from processes.filters import ProcessDefinitionFilter, ProcessDefinitionFilterFormHelper
from processes.models import ProcessDefinition, StepDefinition, Step, Process
from processes.tables import ProcessDefinitionTable, ProcessPracticeProgress
from processes.forms import StepDateForm, ProcessDeadlineForm
from utils.view_mixins import ManagePracticePermissionRequired, ModelRendererMixin

processes_log = dispatch.Signal()

def annotate_overdue_processes(processes = None):
    if processes is None:
        processes = Process.objects.all()
    undone_process_steps = Step.objects.filter(process = OuterRef("pk"), is_done = False)
    return processes.annotate(is_done = ~Exists(undone_process_steps))

def annotate_overdue_process_definitions(process_definitions = None):
    if process_definitions is None:
        process_definitions = ProcessDefinition.objects.all()
    processes = annotate_overdue_processes()
    overdue_process_instances = processes.filter(process_definition = OuterRef("pk"), deadline__lt = date.today(), is_done = False)
    process_definitions = process_definitions.annotate(is_overdue = Exists(overdue_process_instances))
    return process_definitions

class ProcessDefinitions(TemplateView):
    template_name = 'processes/definition_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        process_definitions = ProcessDefinition.objects.get_process_definitions_for_user(user=self.request.user)
        process_definitions = annotate_overdue_process_definitions(process_definitions)

        process_definition_filter = ProcessDefinitionFilter(
            self.request.GET,
            queryset = process_definitions
        )

        table = ProcessDefinitionTable(process_definition_filter.qs)

        RequestConfig(self.request).configure(table)

        context['filter'] = process_definition_filter
        context['table'] = table

        return context


class ProcessDefinitionCreate(CreateView):
    model = ProcessDefinition
    template_name = 'global/forms/generic.html'
    fields = ['name', 'description', ]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        # TODO: Switching to FormView would allow to utilize custom constructor from ProcessDefManager.
        processes_log.send(sender=self.__class__, username=self.request.user.username, process=self.object)
        self.object.assign_perms_for_author_and_institute(author=self.request.user)
        return reverse_lazy('processes:definition-detail', kwargs={'pk': self.object.id})


class ProcessDefinitionDelete(PermissionRequiredMixin, DeleteView):
    model = ProcessDefinition
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'processes.delete_processdefinition'
    return_403 = True

    def get_success_url(self):
        processes_log.send(sender=self.__class__, username=self.request.user.username, process=self.object)
        return reverse_lazy('processes:definition-list')


class ProcessInstances(PermissionRequiredMixin, ListView):
    model = Process
    template_name = 'processes/process_instance_list.html'
    permission_required = 'processes.view_processdefinition'
    return_403 = True
    context_object_name = 'processes'

    def get_queryset(self):
        process_definition = self.get_process_definition_object()
        return Process.objects.filter(process_definition=process_definition).order_by('created_at')

    def get_permission_object(self):
        return self.get_process_definition_object()

    def get_process_definition_object(self):
        return ProcessDefinition.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        process_definition = self.get_process_definition_object()

        actions = []
        if not process_definition.is_draft:
            actions.append("INSTANCIATION_ALLOWED")
            actions.append("VIEW_INSTANCES")

        table = ProcessPracticeProgress(process_definition, request = self.request)
        processes = annotate_overdue_processes(process_definition.processes)

        view_context = {
            "actions": actions,
            "process_definition": process_definition,
            "is_practice_overdue": processes.filter(general = False, deadline__lt = date.today(), is_done = False).exists(),
            "table": table
        }
        context.update(view_context)

        return context


class ProcessDefinitionDetail(PermissionRequiredMixin, DetailView):
    model = ProcessDefinition
    template_name = 'processes/definition_detail.html'
    permission_required = 'processes.view_processdefinition'
    return_403 = True
    context_object_name = 'process_definition'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        actions = []

        if self.request.user.has_perm('processes.change_processdefinition', self.object):
            actions.append("EDIT_ALLOWED")
        if not self.object.is_draft:
            actions.append("VIEW_INSTANCES")
        else:
            actions.append("FINALIZE")

        processes = annotate_overdue_processes(self.object.processes)

        context['actions'] = actions
        context['step_definitions'] = self.object.step_definitions
        context['process'] = self.object.general_process
        context['linked_practices'] = Practice.objects.filter(pk__in=Process.objects.filter(process_definition=self.object.id, general=False).values_list('practice', flat=True)).values_list('name', flat=True)
        context['is_practice_overdue'] = processes.filter(general = False, deadline__lt = date.today(), is_done = False).exists()
        context['confirm_url'] = reverse_lazy('processes:definition-delete', kwargs={'pk': self.object.id})

        return context


class ProcessDefinitionUpdate(PermissionRequiredMixin, UpdateView):
    model = ProcessDefinition
    template_name = 'processes/forms/definition_update.html'
    permission_required = 'processes.change_processdefinition'
    context_object_name = 'process_definition'
    return_403 = True
    fields = [
        'name',
        'description',
    ]

    def get_success_url(self):
        return reverse_lazy('processes:definition-detail', kwargs={'pk': self.object.id})
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        actions = []
        if self.request.user.has_perm('processes.change_processdefinition', self.object):
            actions.append("EDIT_ALLOWED")
        context['actions'] = actions
        context['step_definitions'] = self.object.step_definitions
        return context


class StepDefinitionCreate(PermissionRequiredMixin, CreateView):
    model = StepDefinition
    template_name = 'global/forms/generic.html'
    permission_required = 'processes.change_processdefinition'
    return_403 = True
    fields = [
        'name',
        'description',
    ]

    def get_permission_object(self):
        return ProcessDefinition.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        process_definition = ProcessDefinition.objects.get(pk=self.kwargs['pk'])
        form.instance.process_definition = process_definition

        # Manually call create logic on process defintion object (CreateView CBV is probably not the best choice here)
        process_definition.add_step_definition(
            step_name=form.cleaned_data['name'],
            step_description=form.cleaned_data['description']
        )
        success_url = reverse_lazy('processes:definition-detail', kwargs={'pk': process_definition.id})
        return HttpResponseRedirect(success_url)

class StepDefinitionDetail(ModelRendererMixin):
    model = StepDefinition
    fields = ["position_index", "name", "description"]

class StepDefinitionUpdate(PermissionRequiredMixin, UpdateView):
    model = StepDefinition
    template_name = 'global/forms/generic.html'
    permission_required = 'processes.change_processdefinition'
    return_403 = True
    fields = [
        'name',
        'description',
    ]

    def get_permission_object(self):
        return self.get_object().process_definition

    def get_success_url(self):
        return reverse_lazy('processes:definition-detail', kwargs={'pk': self.object.process_definition.id})


class StepDefinitionDelete(PermissionRequiredMixin, DeleteView):
    model = StepDefinition
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'processes.change_processdefinition'
    return_403 = True

    def get_permission_object(self):
        return self.get_object().process_definition

    def get_success_url(self):
        return reverse_lazy('processes:definition-detail', kwargs={'pk': self.object.process_definition.id})


class StepDefinitionMoveDown(PermissionRequiredMixin, View):
    model = StepDefinition
    permission_required = 'processes.change_processdefinition'
    return_403 = True

    def get_object(self):
        return StepDefinition.objects.get(id=self.kwargs.get('pk'))

    def get_permission_object(self):
        return self.get_object().process_definition

    def post(self, request, *args, **kwargs):
        step_definition = self.get_object()
        try:
            step_definition.move_down()
            messages.success(request, _('Step moved.'))
        except StepDefinitionPositionError:
            messages.error(request, _('Step cannot be moved further.'))
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))  # previous page


class StepDefinitionMoveUp(PermissionRequiredMixin, View):
    model = StepDefinition
    permission_required = 'processes.change_processdefinition'
    return_403 = True

    def get_object(self):
        return StepDefinition.objects.get(id=self.kwargs.get('pk'))

    def get_permission_object(self):
        return self.get_object().process_definition

    def post(self, request, *args, **kwargs):
        step_definition = self.get_object()
        try:
            step_definition.move_up()
            messages.success(request, _('Step moved.'))
        except StepDefinitionPositionError:
            messages.error(request, _('Step cannot be moved further.'))
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))  # previous page


class ProcessDefinitionFinalize(SuccessMessageMixin, View):
    def post(self, request, *args, **kwargs):

        process_definition = ProcessDefinition.objects.get(id=kwargs.get('pk'))

        if not self.request.user.has_perm('processes.change_processdefinition', process_definition):
            raise PermissionDenied

        if process_definition.is_draft:
            process_definition.finalize(request.user)
            messages.success(request, _('Process definition was finalized.'))

        else:
            messages.error(
                request,
                _("Can't perform this action. You must be author and process definition must be draft.")
            )

        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


def bulk_create_processes(request, pk):
    """
    View for creating event participations from event context.
    Allowing user to bulk create for selected practices.
    Seems to be cleaner with function based view.
    TODO: Check 'processes.view_processdefinition' permission.
    """
    process_definition = ProcessDefinition.objects.get(pk=pk)

    # Gets all practices that don't already have this process.
    practices = Practice.objects.get_practice_objects_for_user(user=request.user) \
                                .exclude(process__process_definition = process_definition)

    practice_filter = PracticeFilter(request, queryset=practices)
    table = SelectPracticesTable(practice_filter.qs)

    if request.method == 'POST':
        deadline_form = ProcessDeadlineForm(request.POST)
        selected_practices = Practice.objects.filter(id__in=request.POST.getlist("selection"))

        if deadline_form.is_valid() and selected_practices:
            Process.objects.create_processes(
                process_definition=process_definition,
                practices=selected_practices,
                user=request.user,
                deadline=deadline_form.cleaned_data["deadline"]
            )
            return redirect('processes:process-instances', pk=pk)
    else:
        deadline_form = ProcessDeadlineForm()

    context = {
        "table": table,
        "filter": practice_filter,
        "process_definition": process_definition,
        "deadline_form": deadline_form
    }
    return render(request, 'processes/forms/process_instantiate_form_filterable.html', context)

def check_step_permission(user, step):
    if step.process.practice and not user.has_perm("practices.manage_practice", step.process.practice):
        raise PermissionDenied
    if step.process.general and not user.has_perm("processes.change_processdefinition", step.process.process_definition):
        raise PermissionDenied
    

def check_process_permission(user, process):
    if process.practice and not user.has_perm("practices.manage_practice", process.practice):
        raise PermissionDenied
    if process.general and not user.has_perm("processes.change_processdefinition", process.process_definition):
        raise PermissionDenied

class StepMarkAsDone(UserPassesTestMixin, View):
   
    def get_step_object(self):
        return Step.objects.get(id=self.kwargs.get('pk'))

    def test_func(self):
        return not self.get_step_object().is_done

    def post(self, request, *args, **kwargs):
        step = self.get_step_object()
        check_step_permission(request.user, step)
        step.mark_as_done(request.user)

        context = {'process' : self.get_step_object().process}
        return render(request, "practices/processes/process_step_table.html", context)

class StepReset(UserPassesTestMixin, View):

    def get_step_object(self):
        return Step.objects.get(id=self.kwargs.get('pk'))

    def test_func(self):
        return self.get_step_object().is_done

    def post(self, request, *args, **kwargs):
        step = self.get_step_object()
        check_step_permission(request.user, step)
        step.reset()

        context = {'process' : self.get_step_object().process}
        return render(request, "practices/processes/process_step_table.html", context)

    
class StepDateEdit(UpdateView):
    model = Step
    template_name = 'global/forms/generic.html'
    form_class = StepDateForm

    def get_step_object(self):
        return Step.objects.get(id=self.kwargs.get('pk'))

    def get_success_url(self) -> str:
        step = self.get_step_object()
        check_step_permission(self.request.user, step)

        response_url = reverse_lazy('processes:definition-detail', kwargs={'pk': self.get_step_object().process.process_definition.id})
        if step.process.practice:
            response_url = reverse_lazy('practices:process-list', kwargs={'pk': self.get_step_object().process.practice.id})
        return response_url


class ProcessDelete(ManagePracticePermissionRequired, DeleteView):
    model = Process
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        return reverse_lazy('practices:process-list', kwargs={'pk': self.object.practice.id})

class ProcessDeadlineEdit(UpdateView):
    model = Process
    template_name = 'global/forms/generic.html'
    form_class = ProcessDeadlineForm

    def get_process_object(self):
        return Process.objects.get(id=self.kwargs.get('pk'))

    def get_success_url(self) -> str:
        process = self.get_process_object()
        check_process_permission(self.request.user, process)

        response_url = reverse_lazy('processes:definition-detail', kwargs={'pk': self.get_process_object().process_definition.id})
        if process.practice:
            response_url = reverse_lazy('practices:process-list', kwargs={'pk': self.get_process_object().practice.id})
        return response_url

# class ProcessEdit(ManagePracticePermissionRequired, UpdateView):
#     model = Process
#     template_name = '' # TODO: fill
#     # permission_required?
#     context_object_name = 'process'
#     return_403 = True
#     fields = [
#         'deadline'
#     ]

#     def get_success_url(self):
#         # TODO: replace
#         return reverse_lazy('processes:definition-detail', kwargs={'pk': self.object.id})
    
#     def get_permission_object(self):
#         return self.get_object().process_definition
    
#     def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
#         context = super().get_context_data(**kwargs)
#         # ? Is all this actions stuff really needed if return_403 = True?
#         actions = []
#         if self.request.user.has_perm('processes.change_processdefinition', self.object):
#             actions.append("EDIT_ALLOWED")
#         context['actions'] = actions
#         return context
    

class PracticeProcessList(ManagePracticePermissionRequired, ListView):
    model = Process
    template_name = 'practices/processes/process_list.html'
    paginate_by = 10

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        queryset = super().get_queryset().filter(
            practice=self.get_practice_object()
        ).order_by('-created_at')
        return queryset


class CreateProcessDefinitionCopy(PermissionRequiredMixin, View):
    permission_required = 'processes.view_processdefinition'
    return_403 = True

    def get_process_definition(self):
        return ProcessDefinition.objects.get(id=self.kwargs.get('pk'))

    def get_permission_object(self):
        return self.get_process_definition()

    def post(self, request, pk):
        copy_object = self.get_process_definition().create_copy(author=request.user)
        return redirect('processes:definition-detail', pk=copy_object.id)

class ProcessParticipationCreate(ManagePracticePermissionRequired, CreateView):
    class ProcessParticipationForm(forms.ModelForm):
        class Meta:
            model = Process
            fields = [
                'process_definition',
            ]

    model = Process
    template_name = 'global/forms/generic.html'
    form_class = ProcessParticipationForm

    def get_practice(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_permission_object(self):
        return self.get_practice()

    def get_form_class(self):
        # List of IDs of process definitions that have already been added to the practice.
        # These processes can not be added a second time and are excluded from the dropdown.
        practice_process_ids = self.get_practice().process_set.values_list("process_definition_id", flat = True)

        valid_process_definitions = ProcessDefinition.objects.get_process_definitions_for_user(self.request.user) \
                                                             .filter(is_draft = False) \
                                                             .exclude(id__in = practice_process_ids)

        form_class = ProcessParticipationCreate.ProcessParticipationForm
        form_class.base_fields['process_definition'] = forms.ModelChoiceField(
            label = _('Select process'),
            queryset = valid_process_definitions,
            required = True,
        )
        return form_class

    def form_valid(self, form: ProcessParticipationForm):
        form.instance.practice_id = self.kwargs["pk"]
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('practices:process-list', kwargs={'pk': self.kwargs['pk']})
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_title"] = _("Add process to practice")
        return context

def process_step_select_option(request):
    option = request.GET.get('process__process_definition')
    step_definitions = ProcessDefinition.objects.none() #init empty
    if option:
        process_definition = ProcessDefinition.objects.get_process_definitions_for_user(request.user).get(id=option) #check Permission
        if process_definition != None and str(process_definition.pk) == option:
            step_definitions = ProcessDefinition.objects.get(id=option).step_definitions
    context = {'step_definitions' : step_definitions}
    return render(request, "practices/processes/process_step_options.html", context)
