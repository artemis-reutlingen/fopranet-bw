from django.db import models, transaction
from django.utils.translation import gettext_lazy as _

from processes.models.step import Step
from users.models import User

from datetime import date


class ProcessManager(models.Manager):
    def create_processes(self, process_definition, practices, user, deadline = None):
        """
        Bulk create Process instances for a given list of practices.
        """

        with transaction.atomic():
            for practice in practices:
                self.create(
                    process_definition=process_definition,
                    practice=practice,
                    created_by=user,
                    deadline = deadline
                )


class Process(models.Model):
    """
    Represents an instance of a ProcessDefinition. Has a foreign key practice model.
     Automatically generates child step instances on create via signal.
    """
    objects = ProcessManager()
    practice = models.ForeignKey('practices.Practice', null=True, blank=True, on_delete=models.CASCADE)
    general = models.BooleanField(default=False)
    
    process_definition = models.ForeignKey("processes.ProcessDefinition", null=True, blank=False,
                                           on_delete=models.CASCADE)
    
    deadline = models.DateField(verbose_name=_('Frist'), null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, verbose_name=_('Created by'), on_delete=models.SET_NULL, null=True)

    @property
    def steps(self):
        return Step.objects.filter(process=self).order_by('step_definition__position_index')

    @property
    def completed_in_percent(self):
        no_of_total_steps = self.steps.count()
        finished_steps = self.steps.filter(is_done=True)
        no_of_finished_steps = finished_steps.count()

        # casting to int, due to possible use inside html attrs later (problem with i18n)
        return 100 if no_of_total_steps==0 else int(round(float(no_of_finished_steps / no_of_total_steps), 2) * 100)

    @property
    def is_due(self):
        if self.deadline:
            return self.deadline < date.today() and not self.is_done
        else:
            return False
    
    @property
    def is_done(self):
        return not self.steps.filter(is_done = False).exists()

    def __str__(self) -> str:
        return f"{self.process_definition}"
    
    class Meta:
        verbose_name = _("Process")
        verbose_name_plural = _("Processes")
