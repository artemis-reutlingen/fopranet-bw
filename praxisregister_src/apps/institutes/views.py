from django import forms
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import ValidationError
from django.views.generic.edit import FormView

from institutes.models import Institute
from utils.view_mixins import AdminStaffRequiredMixin


class CreateInstituteFormView(AdminStaffRequiredMixin, SuccessMessageMixin, FormView):
    """
    This view represents a convenience action for usage in the admin panel.
    Creates a new institute from given organization name and name of the department/institute.
    Also automatically creates a corresponding django group for this institute.
    """

    class CreateInstituteForm(forms.Form):
        # TODO: Adapt variable names -> department vs. Institute.name
        organization = forms.CharField()
        department = forms.CharField()
        email_address = forms.EmailField(required=False)

        def clean(self):
            """
            Checks for naming conflicts with existing institutes.
            """
            cleaned_data = super().clean()
            organization = cleaned_data.get("organization")
            department = cleaned_data.get("department")

            # Prerequisite check if both fields are valid themselves
            if organization and department:
                # Check if an institute with identical naming exists
                if Institute.objects.filter(organisation=organization, name=department).exists():
                    raise ValidationError(
                        "Naming conflict. This combination of organization name and department name already exists!"
                    )

    template_name = 'institutes/create_institute.html'
    form_class = CreateInstituteForm
    success_url = '/headquarters/'  # TODO: proper redirect with reverse func
    success_message = "New institute created successfully"

    def form_valid(self, form):
        # Generate a group name from given names in the form
        group_name = f"{form.cleaned_data['organization']}_{form.cleaned_data['department']}"

        # Create the objects
        Institute.objects.create_institute_with_group(
            group_name=group_name,
            organisation=form.cleaned_data['organization'],
            name=form.cleaned_data['department'],
            email_address=form.cleaned_data['email_address']
        )
        # TODO: add success message?
        return super().form_valid(form)
