from django import forms
from django.contrib import admin
from institutes.models import Institute
from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from institutes.forms import CreateInstituteForm

class InstituteAdmin(GuardedModelAdmin):

    form = CreateInstituteForm
    model = Institute
    list_display = ['organisation', 'name', 'group', 'email_address']
    fieldsets = (
        (None, {
            'fields': ('organisation', 'name', 'email_address'),
        }),
    )



admin.site.register(Institute, InstituteAdmin)
