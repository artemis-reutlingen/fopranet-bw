from django.urls import path
from events.views import event_info_views, participation_views, date_views
app_name = 'events'

urlpatterns = [
     
     # Event Info links
     path('', event_info_views.EventList.as_view(), name='list'),
     path('new/', event_info_views.EventCreate.as_view(), name='create_event'),
     path('<int:pk>/', event_info_views.EventDetail.as_view(), name='detail'),
     path('<int:pk>/edit', event_info_views.EventEdit.as_view(), name='edit'),
     path('<int:pk>/delete', event_info_views.EventDelete.as_view(), name='delete'),

     # Invitation links
     path('invitation/<str:invitation_code>/', participation_views.eventParticipantLink, name='invitation-link'),
     path('invitation/confirm/<str:invitation_code>/', participation_views.eventParticipantLinkConfirm, name='invitation-link-confirm'),
     path('invitation/cancel/<str:invitation_code>/', participation_views.eventParticipantLinkCancel, name='invitation-link-cancel'),

     # Participation links
     path('<int:pk>/participations/', participation_views.EventParticipationList.as_view(), name='participation-list'),
     path('<int:pk>/participations/practices/bulk/', participation_views.bulk_create_event_participations_for_practices, name='participation-practice-bulk-create'),
     path('<int:pk>/participations/persons/bulk/', participation_views.bulk_create_event_participations_for_persons, name='participation-persons-bulk-create'),
     path('participations/<int:pk>/delete', participation_views.EventParticipationDelete.as_view(), name='participation-delete'),
     path('participations/<int:pk>/persons/add', participation_views.AddParticipatingPerson.as_view(), name='participation-add-person'),
     path('participations/<int:event_participation_id>/persons/<int:pk>/edit', participation_views.EditParticipatingPerson.as_view(), name='participation-edit-person'), 
     path('participations/<int:event_participation_id>/persons/<int:participating_person_id>/delete', participation_views.RemoveParticipatingPerson.as_view(), name='participation-remove-person'),    

     # HTMX links
     path('htmx/event-table/', event_info_views.event_table, name='event_table'),
     path('<int:pk>/dates/', date_views.create_date, name='add_dates'),
     path('htmx/add-date', date_views.create_date_form, name='add-date'),
     path('htmx/date/<int:pk>/', date_views.detail_date, name="detail-date"),
     path('htmx/date/<int:pk>/update/', date_views.update_date, name="update-date"),
     path('htmx/date/<int:pk>/delete/', date_views.delete_date, name="delete-date"),

     # Modal links
     path('<int:pk>/participations/invite', participation_views.EventParticipationsInvited.as_view(), name='participation-invite-all'),
     path('<int:pk>/participations/sign-up', participation_views.EventParticipationsSignedIn.as_view(), name='participation-sign-up-all'),
     path('<int:pk>/participations/participate', participation_views.EventParticipationsParticipated.as_view(), name='participation-participate-all'),

]
