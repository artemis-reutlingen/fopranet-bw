from django.test import TestCase
from django.urls import reverse

from events.models import EventInfo, EventParticipation, DateModel, ParticipatingPerson
from institutes.models import Institute
from persons.models import Person
from practices.models import Practice
from events.types import Status, EventType
from users.models import User
from utils.utils_test import setup_test_basics

def basic_event_test_set_up(self):
    institute, group, user, [practice_1, practice_2] = setup_test_basics()
    self.practice1=practice_1

    practice_3 = Practice.objects.create(name="Practice_C")
    practice_3.identification_number = "ABC-3"
    practice_3.save()

    self.person1 = Person.objects.create(practice=practice_1)
    self.person2 = Person.objects.create(practice=practice_2)

    # practice_3 should not be visible for our user
    self.event = EventInfo.objects.create(title="Testpraxis")
    self.date = DateModel.objects.create(date="2021-10-01", start_time="12:00", end_time="14:00", event=self.event)

    self.event.assign_perms(user=user)

    self.client.login(username="tester", password="password")

class EventParticipationCreateRegressionTest(TestCase):
    def setUp(self) -> None:
        basic_event_test_set_up(self)

    def test_redirect_to_login_if_not_logged_in(self):
        self.client.logout()
        request_url = reverse('events:participation-practice-bulk-create', args=[self.event.id])
        response = self.client.get(request_url)
        self.assertRedirects(response, f"{reverse('users:login')}?next={request_url}")

        response = self.client.post(
            request_url,
            {"practices": [1, 2]}
        )
        self.assertRedirects(response, f"{reverse('users:login')}?next={request_url}")

    def test_bulk_create_event_practice_participation(self):
        self.assertEqual(EventParticipation.objects.count(), 0)
        self.client.post(
            reverse('events:participation-practice-bulk-create', args=[self.event.id]),
            {"practices": [1, 2]}
        )

    def test_bulk_create_event_practice_already_participating(self):
        self.client.post(
            reverse('events:participation-practice-bulk-create', args=[self.event.id]),
            {"selection": [1,2]}
        )
        self.assertEqual(EventParticipation.objects.count(), 2)
        self.client.post(
            reverse('events:participation-practice-bulk-create', args=[self.event.id]),
            {"selection": [1]}
        )
        self.assertEqual(EventParticipation.objects.count(), 2)

    def test_bulk_create_event_practice_participation_without_permission(self):
        self.assertEqual(EventParticipation.objects.count(), 0)
        self.client.post(
            reverse('events:participation-practice-bulk-create', args=[self.event.id]),
            {"practices": [3]}
        )

        self.assertEqual(EventParticipation.objects.count(), 0)

    def test_bulk_create_event_person_participation(self):
        self.assertEqual(EventParticipation.objects.count(), 0)
        self.assertEqual(ParticipatingPerson.objects.count(), 0)
        #Create person that belongs to same practice
        Person.objects.create(practice=self.practice1)
        self.client.post(
            reverse('events:participation-persons-bulk-create', args=[self.event.id]),
            {"selection": [1,2,3]}
        )
        self.assertEqual(EventParticipation.objects.count(), 2)
        self.assertEqual(ParticipatingPerson.objects.count(), 3)

    def test_bulk_create_event_person_already_assigned(self):
        self.client.post(
            reverse('events:participation-persons-bulk-create', args=[self.event.id]),
            {"selection": [1,2]}
        )
        self.assertEqual(EventParticipation.objects.count(), 2)
        self.assertEqual(ParticipatingPerson.objects.count(), 2)
        self.client.post(
            reverse('events:participation-persons-bulk-create', args=[self.event.id]),
            {"selection": [1,2]}
        )
        self.assertEqual(EventParticipation.objects.count(), 2)
        self.assertEqual(ParticipatingPerson.objects.count(), 2)

class ViewsTests(TestCase):
    def setUp(self) -> None:
        self.institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )

        self.user = User.objects.create_user_for_institute(username="tester0", password="x", institute=self.institute)

        self.event = EventInfo.objects.create(title="Testpraxis")
        self.date = DateModel.objects.create(date="2021-10-01", start_time="12:00", end_time="14:00", event=self.event)

        self.event.assign_perms(user=self.user)

        self.client.login(username='tester0', password='x')

    def test_event_detail_page_gets_rendered_correctly(self):
        response = self.client.get(reverse('events:detail', kwargs={'pk': self.event.id}))

        self.assertTemplateUsed(
            response,
            template_name='events/detail.html'
        )
    
    def test_create_event(self):
        response = self.client.get(reverse('events:create_event'))
        self.assertEqual(response.status_code, 200)
        number_of_events = EventInfo.objects.count()
        response_post = self.client.post(reverse('events:create_event'), data={
            'title': 'Mittagspause',
            'description': 'Mittagspause weil wir Hunger haben',
            'status': Status.PLANNED,
            'takes_place': EventType.ONLINE
        })
        self.assertEqual(response_post.status_code, 302)
        self.assertRedirects(response_post, reverse('events:add_dates', kwargs={'pk': EventInfo.objects.last().id}))
        self.assertEqual(EventInfo.objects.count(), number_of_events + 1)

    def test_create_date(self):
        response = self.client.get(reverse('events:add_dates', kwargs={'pk': self.event.id}))
        self.assertEqual(response.status_code, 200)
        number_of_dates = DateModel.objects.count()
        response_post = self.client.post(reverse('events:add_dates', kwargs={'pk': self.event.id}), data={
            'date': '2021-10-01',
            'start_time': '12:00',
            'end_time': '14:00'
        })
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(DateModel.objects.count(), number_of_dates + 1)

class ModelsTest(TestCase):
    def setUp(self) -> None:
        self.institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )
        self.practice = Practice.objects.create_practice_for_institute(self.institute, "RandomPractice")

        self.person_a = Person.objects.create_person_for_practice("Max", "Mustermann", self.practice)
        self.person_b = Person.objects.create_person_for_practice("Hans", "Mayer", self.practice)

        self.event = EventInfo.objects.create(title="Testpraxis")
        self.date = DateModel.objects.create(date="2021-10-01", start_time="12:00", end_time="14:00", event=self.event)

    def test_can_create_practice_participation_for_event(self):
        self.assertEqual(self.event.eventparticipation_set.count(), 0)
        EventParticipation.objects.create(practice=self.practice, event=self.event)
        self.assertEqual(self.event.eventparticipation_set.count(), 1)

    def test_can_add_and_remove_persons_to_practice_participation(self):
        practice_participation = EventParticipation.objects.create(practice=self.practice, event=self.event)
        self.assertEqual(practice_participation.participatingperson_set.count(), 0)
        practice_participation.add_participating_person(self.person_a)
        practice_participation.add_participating_person(self.person_b)
        self.assertEqual(practice_participation.participatingperson_set.count(), 2)
        practice_participation.remove_participating_person(self.person_a)
        self.assertEqual(practice_participation.participatingperson_set.count(), 1)
