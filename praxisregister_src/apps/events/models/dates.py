from django.db import models
from django.utils.translation import gettext_lazy as _
from importer.models import ImportableMixin

class DateModel(ImportableMixin, models.Model):

    date = models.DateField(verbose_name=_("Date"))
    start_time = models.TimeField(verbose_name=_("Start time"))
    end_time = models.TimeField(verbose_name=_("End time"))

    event = models.ForeignKey('events.EventInfo', verbose_name=_('Event'),
                                           on_delete=models.CASCADE,
                                           null=True)