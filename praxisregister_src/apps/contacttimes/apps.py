from django.apps import AppConfig


class ContacttimesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contacttimes'
