from django import forms
from django.forms import TimeInput

from contacttimes.models import DayContactTimeSlot

contact_time_input_widget = TimeInput(
    format='%H:%M',
    attrs={
        'type': 'time',
        'class': 'form-control',
    },
)


class TimeSlotForm(forms.ModelForm):
    early_start = forms.TimeField(
        widget=contact_time_input_widget,
        label="",
        required=False
    )
    early_end = forms.TimeField(
        widget=contact_time_input_widget,
        label="",
        required=False

    )
    late_start = forms.TimeField(
        widget=contact_time_input_widget,
        label="",
        required=False

    )
    late_end = forms.TimeField(
        widget=contact_time_input_widget,
        label="",
        required=False
    )

    class Meta:
        model = DayContactTimeSlot
        fields = ('early_start', 'early_end', 'late_start', 'late_end',)
