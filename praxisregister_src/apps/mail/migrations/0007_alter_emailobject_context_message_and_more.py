# Generated by Django 4.2.7 on 2024-02-13 08:22

from django.db import migrations
import django_ckeditor_5.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0006_alter_emailcontextfield_key'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailobject',
            name='context_message',
            field=django_ckeditor_5.fields.CKEditor5Field(verbose_name='Message'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='context_message',
            field=django_ckeditor_5.fields.CKEditor5Field(verbose_name='Context message'),
        ),
        migrations.AlterField(
            model_name='recipient',
            name='received_email_message',
            field=django_ckeditor_5.fields.CKEditor5Field(verbose_name='Received email message'),
        ),
    ]
