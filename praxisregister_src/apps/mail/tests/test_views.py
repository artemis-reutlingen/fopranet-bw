from unittest import skip
from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse
from django.core import mail
from django.contrib.auth.models import Group
from django.utils import translation
from mail.views.email_views import *
from mail.models import EmailObject
from mail.helper import emailhelper
from events.models.events import EventInfo, EventParticipation
import re

from institutes.models import Institute

import logging
logger = logging.getLogger(__name__)

@override_settings(EMAIL_SENDERS = ["sender@example.com"])
class TestEmailViews(TestCase):

    def setUp(self):
        group = Group.objects.create(name="Group")
        institute = Institute.objects.create(
            organisation="Organization X",
            name="institute_name",
            group=group
        )
        user = User.objects.create(username="tester")
        user.set_password("password")
        user.assigned_institute = institute
        user.email = 'max.mustermann@institut.de'
        user.groups.add(group)
        user.save()
        self.user = user
        self.client.login(username="tester", password="password")
        
        self.new_url = reverse('mail:emailobject-new')

        self.email_object = EmailObject.objects.create(subject='Test', context_message='Test', from_email='sender@example.com', reply_to='max.mustermann@institut.de', draft=True)
        self.email_object.assign_perms(user)

        self.detail_url = reverse('mail:emailobject-detail', args=[self.email_object.pk])
        self.delete_url = reverse('mail:emailobject-delete', args=[self.email_object.pk])
        self.send_detail_url = reverse('mail:emailobject-send-detail', args=[self.email_object.pk])

    def test_email_object_list_GET(self):
        response = self.client.get(reverse('mail:emailobject-list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, 'mails/tables/email_object_list.html')

    def test_email_object_draft_list_GET(self):
        response = self.client.get(reverse('mail:emailobject-draft-list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mails/tables/email_draft_list.html')

    def test_email_object_new_GET(self):
        response = self.client.get(self.new_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mails/forms/email_object.html')

    def test_email_object_new_POST(self):
        data = {
            'from_email': 'sender@example.com',
            'reply_to': 'max.mustermann@institut.de',
            'subject': 'test_email_object_new_POST',
            'context_message': 'Test'
        }
        response = self.client.post(self.new_url, data)
        logger.info(response.content)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(EmailObject.objects.filter(subject="test_email_object_new_POST", context_message="Test").exists())
    
    def test_email_object_new_with_not_user_mail_POST(self):
        data = {
            'from_email': 'sender@example.com',
            'reply_to': 'wrong@institut.de',
            'subject': 'not_user_mail_POST',
            'context_message': 'Test'
        }
        response = self.client.post(self.new_url, data)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(EmailObject.objects.filter(subject="not_user_mail_POST", context_message="Test").exists())

    def test_email_object_new_with_invalid_sender_POST(self):
        data = {
            'from_email': 'wrong.sender@example.com',
            'reply_to': 'max.mustermann@institut.de',
            'subject': 'invalid_sender_POST',
            'context_message': 'Test'
        }
        response = self.client.post(self.new_url, data)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(EmailObject.objects.filter(subject="invalid_sender_POST", context_message="Test").exists())

    def test_email_object_detail_GET(self):
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mails/forms/email_object.html')

    def test_email_object_detail_POST(self):
        data = {
            'from_email': 'sender@example.com',
            'reply_to': 'max.mustermann@institut.de',
            'subject': 'Changed',
            'context_message': 'Test'
        }
        response = self.client.post(self.detail_url, data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(EmailObject.objects.filter(subject="Changed", context_message="Test").exists())

    def test_email_object_delete_POST(self):
        number_of_email_objects = EmailObject.objects.count()
        response = self.client.post(self.delete_url)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(EmailObject.objects.count() == number_of_email_objects - 1)

    def test_email_object_send_detail_GET(self):
        response = self.client.get(self.send_detail_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mails/email_object_detail.html')

    def test_send_email_to_recipients(self):
        rg = RecipientGroup.objects.create(email_object=self.email_object)
        Recipient.objects.create(recipient_group=rg, recipient_email_address="testmail@bla.de", received_email_message=self.email_object.context_message)
        CarbonCopyRecipient.objects.create(recipient_group=rg, recipient_email_address="testmail@bla.de")
        BlindCarbonCopyRecipient.objects.create(recipient_group=rg, recipient_email_address="testmail@bla.de")
        message_dic = emailhelper.prepare_messages(self.email_object)
        emailhelper.open_connection_to_send_multiple_emails(message_dic, self.email_object)
        self.assertTrue(Recipient.objects.filter(sent_time__isnull=False).exists())
        self.assertTrue(CarbonCopyRecipient.objects.filter(sent_time__isnull=False).exists())
        self.assertTrue(BlindCarbonCopyRecipient.objects.filter(sent_time__isnull=False).exists())

    def test_send_email_to_messages(self):
        rg = RecipientGroup.objects.create(email_object=self.email_object)
        rg2 = RecipientGroup.objects.create(email_object=self.email_object)
        Recipient.objects.create(recipient_group=rg, recipient_email_address="testmail@bla.de", received_email_message=self.email_object.context_message)
        Recipient.objects.create(recipient_group=rg2, recipient_email_address="testmail@bla.de", received_email_message=self.email_object.context_message)
        message_dic = emailhelper.prepare_messages(self.email_object)
        emailhelper.open_connection_to_send_multiple_emails(message_dic, self.email_object)
        # Test that both messages has been sent.
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, self.email_object.subject)

    
    def test_update_recipient_data(self):
        #Create person that is participating in an event.
        person = Person.objects.create(first_name="Max", last_name="Maxmann")
        event = EventInfo.objects.create(title="Event")
        event_participation = EventParticipation.objects.create(event=event)
        participating_person = ParticipatingPerson.objects.create(person=person, event_participation=event_participation)

        self.assertFalse(participating_person.invitation_sent)

        #Perpare recipient group and recipient.
        obj, created = EmailContextField.objects.get_or_create(key=EmailContextFieldChoices.EVENT_INVITATION_LINK)
        self.email_object.context_fields.add(obj)
        self.email_object.event = event
        self.email_object.save()
        rg = RecipientGroup.objects.create(email_object=self.email_object)
        Recipient.objects.create(recipient_group=rg, recipient_person=person, recipient_email_address="testmail@bla.de", received_email_message=self.email_object.context_message)
        
        emailhelper.update_recipient_data(rg)
        participating_person.refresh_from_db()
        self.assertTrue(participating_person.invitation_sent)

    @override_settings(LANGUAGE_CODE='en-US', LANGUAGES=(('en', 'English'),))
    def test_message_types_translations_for_special_characters_english(self):
        test_message_types_translations_for_special_characters(self)
    
    @override_settings(LANGUAGE_CODE='de-de', LANGUAGES=(('de', 'German'),))
    def test_message_types_translations_for_special_characters_german(self):
        test_message_types_translations_for_special_characters(self)
        
def test_message_types_translations_for_special_characters(test_class):
    regex_matches = []
    for context_field in EmailContextFieldChoices.labels:
        field = str(context_field)
        matches = re.findall(r"[^\w\s\dÄÖÜäöüß]", field)
        if matches:
            regex_matches.append(field)
    for viloation in regex_matches:
        # This assert will fail if there are any special characters in the translation.
        # Keep it to see the exact character that is causing the problem.
        test_class.assertEqual(viloation, "")
    test_class.assertTrue(len(regex_matches) == 0)
    translation.activate(settings.LANGUAGE_CODE)

def test_AdminTestMail_view(self):
    # Create a new email object.
    os.environ['ADMINS_EMAIL'] = ''
    response = self.client.get(reverse('mail:emailobject-send-test-email'))
    self.assertEqual(response.status_code, 403)
    os.environ['ADMINS_EMAIL'] = "valid"
    response = self.client.get(reverse('mail:emailobject-send-test-email'))
    self.assertEqual(response.status_code, 200)
