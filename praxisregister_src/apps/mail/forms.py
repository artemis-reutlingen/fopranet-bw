from django.forms import ModelForm, Select, ValidationError
from django import forms
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from mail.models import EmailObject, EmailAttachement, EmailContextText, Recipient, EmailTemplate
from events.models import EventInfo
from mail.types import EmailRecipientChoices, EmailContextFieldChoices
from mail.validators import get_valid_context_fields_from_text
from guardian.shortcuts import get_objects_for_user

class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True

class EmailObjectForm(ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        event = kwargs.pop('event', None)
        super().__init__(*args, **kwargs)
        print(self.data)
        #TODO maybe add extra selection page. Documentation says it should be avoided to have more than 100 entries in a ModelChoiceField 
        self.fields['event'].queryset = EventInfo.objects.get_events_for_user(user)
        self.fields['event'].initial = event
        self.fields['context_message'].required = False # ck5editor requirement
        # self.set_up_email_choices(user, 'from_email')
        self.set_up_sender_choices(user, 'from_email')
        self.set_up_email_choices(user, 'reply_to')
        self.fields['custom_context_texts'].choices = get_objects_for_user(user, 'mail.view_emailcontexttext').values_list('value', 'key')

    def set_up_sender_choices(self, user, field_name):
        choices = [(address, address) for address in settings.EMAIL_SENDERS]
        if len(choices) == 0:
            choices.append(("", _("No email address available")))
        self.fields[field_name].choices = choices
        self.fields[field_name].required = True

    def set_up_email_choices(self, user, field_name):
        institute_email_address = user.assigned_institute.email_address if user.assigned_institute and user.assigned_institute.email_address else None
        user_email_adress = user.email if user.email else None
        choices = []
        if institute_email_address:
            choices.append((institute_email_address, institute_email_address))
        if user_email_adress:
            choices.append((user_email_adress, user_email_adress))
        if len(choices) <= 1:
            self.fields[field_name].help_text = _("Please contact an administrator to add an email address to your user account or to your assigned institute.")
            if len(choices) == 0:
                choices.append(("", _("No email address available")))
        self.fields[field_name].choices = choices
        self.fields[field_name].required = True
    
    from_email = forms.ChoiceField(choices=[("", _("No email address available"))], label=_("From email"))
    reply_to = forms.ChoiceField(choices=[("", _("No email address available"))], label=_("Reply to"))

    #Queryset is set in __init__ because it needs the user
    event = forms.ModelChoiceField(queryset=None, required=False, label=_("Event"), help_text=_("If an event is selected, a invitation link can be added though the system context fields."))

    attachements = forms.FileField(label=_("Add attachement"), required=False, widget=MultipleFileInput())

    internal_context_fields = forms.ChoiceField(choices=EmailContextFieldChoices.choices, label=_("System context fields"), required=False, widget=Select(attrs={'id': 'id_context_fields', 'onchange': 'setContextValue("id_context_fields")'}))
    custom_context_texts = forms.ChoiceField(label=_("Text modules"), required=False, widget=Select(attrs={'id': 'id_context_texts', 'onchange': 'setContextValue("id_context_texts")'}))

    def clean_context_message(self):
        #Check if used context_fields are valid
        message = self.cleaned_data.get("context_message")
        if message is None or message.strip() == "":
            raise ValidationError(_("Context message is empty."))
        else:
            for field in get_valid_context_fields_from_text(message):
                if field not in EmailContextFieldChoices.labels:
                    raise ValidationError(_("Invalid context field used: ") + field)
        return message

    class Meta:
        model = EmailObject
        fields = ['from_email', 'reply_to', 'subject', 'context_message', 'attachements', 'event']

class RecipientMessageForm(ModelForm):

    class Meta:
        model = Recipient
        fields = ['received_email_message']

class AttachementForm(ModelForm):

    file = forms.FileField(label=_("Files"), widget=MultipleFileInput())

    class Meta:
        model = EmailAttachement
        fields = ['file']

class AddCCOrBCCForm(forms.Form):
    cc_or_bcc = forms.ChoiceField(choices=EmailRecipientChoices.choices, label=_("CC or BCC"))

class EmailContextTextForm(ModelForm):

    class Meta:
        model = EmailContextText
        fields = ['key', 'value']

class EmailTemplateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['custom_context_texts'].choices = get_objects_for_user(user, 'mail.view_emailcontexttext').values_list('value', 'key')
        self.fields['context_message'].required = False # ck5editor requirement

    attachements = forms.FileField(label=_("Add attachement"), required=False, widget=MultipleFileInput())

    internal_context_fields = forms.ChoiceField(choices=EmailContextFieldChoices.choices, label=_("System context fields"), required=False, widget=Select(attrs={'id': 'id_context_fields', 'onchange': 'setContextValue("id_context_fields")'}))
    custom_context_texts = forms.ChoiceField(label=_("Text modules"), required=False, widget=Select(attrs={'id': 'id_context_texts', 'onchange': 'setContextValue("id_context_texts")'}))

    class Meta:
        model = EmailTemplate
        fields = ['template_name', 'subject', 'context_message', 'attachements']

class AdminEmailObjectForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['context_message'].required = False # ck5editor requirement


    attachements = forms.FileField(label=_("Add attachement"), required=False, widget=MultipleFileInput())

    class Meta:
        model = EmailObject
        fields = ['from_email', 'reply_to', 'subject', 'context_message', 'attachements']
