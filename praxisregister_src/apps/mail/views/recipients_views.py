import logging
from django.conf import settings
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django_tables2 import RequestConfig, SingleTableMixin
from django.views.generic import UpdateView, ListView, DeleteView, TemplateView
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from guardian.mixins import PermissionRequiredMixin
from mail.models import *
from mail.tables import *
from mail.forms import *
from mail.validators import get_valid_context_fields_from_text
from mail.helper import recipienthelper

from persons.models.person import Person
from persons.filters import PeopleFilter
from persons.tables import SelectPeopleTable

user_logger=logging.getLogger('user_activity')

# ----------------- Recipients -----------------

class RecipientMessageUpdate(PermissionRequiredMixin, UpdateView):
    model = Recipient
    form_class = RecipientMessageForm
    template_name = 'mails/forms/email_object_update_message.html'
    permission_required = "mail.change_emailobject"
    return_403 = True

    def form_valid(self, form):
        #Check if user manually removed the context fields
        if len(get_valid_context_fields_from_text(form.cleaned_data["received_email_message"])) == 0:
            form.instance.unfullfilled_context_fields = False
            form.save()
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context["email_object"] = EmailObject.objects.get(id=self.kwargs['email_object_id'])
            context["recipient"] = Recipient.objects.get(id=self.kwargs['pk'])
            return context

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['email_object_id'])

    def get_success_url(self):
        user_logger.info(f"{self.request.user} updated message of recipient with id {self.object.id}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk': self.kwargs['email_object_id']})

class RecipientMessage(PermissionRequiredMixin, TemplateView):
    template_name = 'mails/email_object_send_message.html'
    permission_required = "mail.view_emailobject"
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['email_object_id'])
    
    def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context["email_object"] = EmailObject.objects.get(id=self.kwargs['email_object_id'])
            context["recipient"] = Recipient.objects.get(id=self.kwargs['pk'])
            return context
          
class RecipientGroupRemove(PermissionRequiredMixin, DeleteView):
    model = RecipientGroup
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.change_emailobject'
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['email_object_id'])

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted RecipientGroup with id {self.object.id} from EmailObject with id {self.kwargs['email_object_id']}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk':self.kwargs['email_object_id']})

class CarbonCopyRecipientRemove(PermissionRequiredMixin, DeleteView):
    model = CarbonCopyRecipient
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.change_emailobject'
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['email_object_id'])

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted CarbonCopyRecipient with {recipienthelper.get_recipient_info(self.get_object())}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk':self.kwargs['email_object_id']})
    
class BlindCarbonCopyRecipientRemove(PermissionRequiredMixin, DeleteView):
    model = BlindCarbonCopyRecipient
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.change_emailobject'
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['email_object_id'])

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted BlindCarbonCopyRecipientRemove with {recipienthelper.get_recipient_info(self.get_object())}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk':self.kwargs['email_object_id']})

class RecipientDraftList(PermissionRequiredMixin, SingleTableMixin, ListView):
    model = RecipientGroup
    paginate_by = 15
    template_name = 'mails/tables/email_object_recipient_draft_group_list.html'
    table_class = RecipientGroupDraftTable
    permission_required = "mail.change_emailobject"
    return_403 = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['email_object'] = EmailObject.objects.get(pk=self.kwargs['pk'])
        #Check if recipients have all context fields filled with correct data
        context['messages_error_free'] = Recipient.objects.filter(recipient_group__email_object=EmailObject.objects.get(id=self.kwargs['pk'])).filter(unfullfilled_context_fields=True).count() == 0
        context['recipients_have_an_email_address'] = recipienthelper.check_if_all_recipients_have_an_email_address(EmailObject.objects.get(id=self.kwargs['pk']))
        context['email_enable'] = settings.EMAIL_ENABLE
        return context
    
    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['pk'])

    def get_queryset(self):
        return RecipientGroup.objects.filter(email_object__id=self.kwargs['pk']).order_by('-recipient__recipient_person__first_name') 

class RecipientList(PermissionRequiredMixin, SingleTableMixin, ListView): 
    model = RecipientGroup
    paginate_by = 15
    template_name = 'mails/tables/email_object_recipient_group_list.html'
    table_class = RecipientGroupTable
    permission_required = "mail.view_emailobject"
    return_403 = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        email_object = EmailObject.objects.get(pk=self.kwargs['pk'])
        context['email_object'] = email_object
        recipient_sending_error = Recipient.objects.filter(recipient_group__email_object=email_object).filter(sent_time__isnull=True).count() > 0
        cc_sending_error = CarbonCopyRecipient.objects.filter(recipient_group__email_object=email_object).filter(sent_time__isnull=True).count() > 0
        bcc_sending_error = BlindCarbonCopyRecipient.objects.filter(recipient_group__email_object=email_object).filter(sent_time__isnull=True).count() > 0
        context['email_sending_error'] = email_object.sent == True and (recipient_sending_error or cc_sending_error or bcc_sending_error)
        return context
    
    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['pk'])

    def get_queryset(self):
        return RecipientGroup.objects.filter(email_object__id=self.kwargs['pk']).order_by('-recipient__recipient_person__first_name') 

def update_participant_list(request, email_object_id):
        if request.method == 'GET':
            email_object = EmailObject.objects.get(pk=email_object_id)
            if not request.user.has_perm('mail.change_emailobject', email_object):
                raise PermissionDenied
            event_participants = Person.objects.filter(participatingperson__event_participation__event__emailobject=email_object)
            possible_recipients = recipienthelper.get_persons_that_are_not_already_recipients(request.user, email_object)
            recipienthelper.add_recipients(request, email_object, possible_recipients, event_participants)
        return redirect('mail:emailobject-recipients', pk=email_object_id)

def add_recipients_from_person_selection_list(request, email_object_id):
    email_object = EmailObject.objects.get(pk=email_object_id)
    if not request.user.has_perm('mail.change_emailobject', email_object):
        raise PermissionDenied
    possible_recipients = recipienthelper.get_persons_that_are_not_already_recipients(request.user, email_object)
    people_filter = PeopleFilter(request.GET, queryset=possible_recipients, request=request)
    table = SelectPeopleTable(people_filter.qs)
    RequestConfig(request,paginate={"per_page": 12}).configure(table)
    if request.method == 'POST':
        # Get checked persons
        selectedPersons = Person.objects.filter(id__in=request.POST.getlist("selection"))
        if len(selectedPersons):
            # Only allow persons that user has permission to and that are not already in recipient list
            recipienthelper.add_recipients(request, email_object, possible_recipients, selectedPersons)
            return redirect('mail:emailobject-recipients', pk=email_object_id)
    return render(request, 'mails/forms/email_object_add_recipients.html', {'email_object': email_object, 'table': table, 'filter': people_filter})

def add_cc_or_bcc_to_recipient_group(request, email_object_id, recipient_group_id):
    email_object = EmailObject.objects.get(pk=email_object_id)
    recipient_group = RecipientGroup.objects.get(pk=recipient_group_id)
    form = AddCCOrBCCForm(request.POST or None)
    if not request.user.has_perm('mail.change_emailobject', email_object):
        raise PermissionDenied
    persons_user_has_permission_to = Person.objects.get_person_objects_for_user(user=request.user)
    #Filter for possible recipients
    recipient_groups = RecipientGroup.objects.filter(Q(email_object=email_object) & Q(carboncopyrecipient__isnull=False) & Q(blindcarboncopyrecipient__isnull=False))
    #Exclude persons that are already in cc or bcc list
    possible_recipients = persons_user_has_permission_to.exclude(id__in=recipient_groups.values_list('carboncopyrecipient__recipient_person__id', flat=True))
    possible_recipients = persons_user_has_permission_to.exclude(id__in=recipient_groups.values_list('blindcarboncopyrecipient__recipient_person__id', flat=True))
    people_filter = PeopleFilter(request.GET, queryset=possible_recipients, request=request)
    table = SelectPeopleTable(people_filter.qs)
    RequestConfig(request,paginate={"per_page": 12}).configure(table)
    if request.method == 'POST':
        with transaction.atomic():
            # Get checked persons
            selectedPersons = Person.objects.filter(id__in=request.POST.getlist("selection"))
            # Only allow persons that user has permission to and that are not already in cc or bcc
            filtered_persons = selectedPersons.intersection(possible_recipients)   
            if form.is_valid() and len(filtered_persons):
                if form.cleaned_data['cc_or_bcc'] == EmailRecipientChoices.CC:
                    bulk_cc_group_list = []
                    for person in filtered_persons:
                        bulk_cc_group_list.append(CarbonCopyRecipient(recipient_person=person, recipient_group=recipient_group))
                    CarbonCopyRecipient.objects.bulk_create(bulk_cc_group_list)
                    user_logger.info(f"{request.user} added CC_recipients to RecipentGroup with id {recipient_group.id}")
                elif form.cleaned_data['cc_or_bcc'] == EmailRecipientChoices.BCC:
                    bulk_bcc_group_list = []
                    for person in filtered_persons:
                        bulk_bcc_group_list.append(BlindCarbonCopyRecipient(recipient_person=person, recipient_group=recipient_group))
                    BlindCarbonCopyRecipient.objects.bulk_create(bulk_bcc_group_list)
                    user_logger.info(f"{request.user} added BCC_recipients to RecipentGroup with id {recipient_group.id}")
                return redirect('mail:emailobject-recipients', pk=email_object_id)
    return render(request, 'mails/forms/email_object_add_recipients.html', {'email_object': email_object, 'table': table, 'filter': people_filter, 'form' : form})