import os
from urllib.parse import urljoin

from django.conf import settings
from django.core.files.storage import FileSystemStorage


class CustomStorage(FileSystemStorage):
    """Custom storage for django_ckeditor_5 images."""

    location = settings.CKEDITOR_5_UPLOAD_PATH
    base_url = settings.CKEDITOR_5_UPLOAD_URL