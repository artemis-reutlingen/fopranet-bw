from django.urls import path
from mail.views import email_views, template_views, text_module_views, recipients_views

app_name = 'mail'

urlpatterns = [

    # ----------------- Template views -----------------

    path('emailtemplates', template_views.EmailTemplateList.as_view(), name='emailtemplate-list'),
    path('emailtemplates/new', template_views.EmailTemplateNew.as_view(), name="emailtemplate-new"),
    path('emailtemplates/<int:pk>/details', template_views.EmailTemplateDetail.as_view(), name="emailtemplate-detail"),
    path('emailtemplates/<int:pk>/use-for-email', template_views.use_template_for_email, name="emailtemplate-use-for-email"),
    path('emailtemplates/<int:pk>/delete', template_views.EmailTemplateDelete.as_view(), name="emailtemplate-delete"),
    path('emailtemplates/<int:pk>/copy', template_views.copy_email_template, name="emailtemplate-copy"),
    path('emailtemplates/<int:email_template_id>/attachements/<int:file_id>', template_views.download_attachement, name='template-attachement-download'),
    path('emailtemplates/attachement/<int:pk>/delete', template_views.AttachementDelete.as_view(), name='template-attachement-delete'),

    # ----------------- Text modules views -----------------

    path('textmodules', text_module_views.TextModuleList.as_view(), name="textmodule-list"),
    path('textmodules/new', text_module_views.TextModuleCreate.as_view(), name="textmodule-new"),
    path('textmodules/<int:pk>/delete', text_module_views.TextModuleDelete.as_view(), name="textmodule-delete"),

    # ----------------- Email objects views -----------------

    path('emailobjects', email_views.EmailObjectList.as_view(), name='emailobject-list'),
    path('emailobjects/drafts', email_views.EmailObjectDraftList.as_view(), name='emailobject-draft-list'),
    path('emailobjects/new', email_views.EmailObjectNew.as_view(), name="emailobject-new"),
    path('emailobjects/<int:pk>/details', email_views.EmailObjectDetail.as_view(), name="emailobject-detail"),
    path('emailobjects/<int:pk>/delete', email_views.EmailObjectDelete.as_view(), name="emailobject-delete"),
    path('emailobjects/<int:pk>/send-email/details', email_views.EmailObjectSendDetail.as_view(), name="emailobject-send-detail"),
    path('emailobjects/<int:pk>/send-email', email_views.send_email_to_recipients, name="emailobject-send-email"),
    # Attachements views
    path('emailobjects/<int:email_object_id>/attachements/<int:file_id>', email_views.download_attachement, name='attachement-download'),
    path('emailobjects/attachement/<int:pk>/delete', email_views.AttachementDelete.as_view(), name='attachement-delete'),

    # ----------------- Admin testing views -----------------

    path('test', email_views.AdminTestMail.as_view(), name="emailobject-send-test-email"),

    # ----------------- Recipients views -----------------

    path('emailobjects/<int:pk>/recipients', recipients_views.RecipientDraftList.as_view(), name="emailobject-recipients"),
    path('emailobjects/<int:pk>/send-email/recipients', recipients_views.RecipientList.as_view(), name="emailobject-send-recipients"),
    path('emailobjects/<int:email_object_id>/send-email/recipients/<int:pk>/message', recipients_views.RecipientMessage.as_view(), name="emailobject-send-recipient-message"),
    path('emailobjects/<int:email_object_id>/recipients/add', recipients_views.add_recipients_from_person_selection_list, name="emailobject-recipients-add"),
    path('emailobjects/<int:email_object_id>/recipients/update', recipients_views.update_participant_list, name="emailobject-update-recipients"),
    path('emailobjects/<int:email_object_id>/recipients/<int:pk>/message', recipients_views.RecipientMessageUpdate.as_view(), name="emailobject-recipient-message"),
    path('emailobjects/<int:email_object_id>/recipients/<int:pk>/remove', recipients_views.RecipientGroupRemove.as_view(), name="emailobject-recipients-remove"),
    path('emailobjects/<int:email_object_id>/recipients/<int:recipient_group_id>/cc-bcc/add', recipients_views.add_cc_or_bcc_to_recipient_group, name="emailobject-recipients-add-cc-bcc"),
    path('emailobjects/<int:email_object_id>/recipients/<int:recipient_group_id>/cc/<int:pk>/remove', recipients_views.CarbonCopyRecipientRemove.as_view(), name="emailobject-recipients-remove-cc"),
    path('emailobjects/<int:email_object_id>/recipients/<int:recipient_group_id>/bcc/<int:pk>/remove', recipients_views.BlindCarbonCopyRecipientRemove.as_view(), name="emailobject-recipients-remove-bcc"),

]
