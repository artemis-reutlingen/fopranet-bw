from typing import Any
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.dispatch import Signal
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, UpdateView
from practices.forms import forms
from practices.models import Practice
from utils.view_mixins import ManagePracticePermissionRequired
import os

practice_contract_log=Signal()

def download_contract_file(request, pk):
    """
    Allows downloading uploaded contract files.
    Actual download is handled by nginx, django is checking permissions.
    """
    practice = Practice.objects.get(id=pk)

    if not request.user.has_perm("practices.manage_practice", practice):
        raise PermissionDenied
    file = practice.contract_document
    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={os.path.basename(file.name)}"
    response['X-Accel-Redirect'] = file.url

    return response

def delete_contract_file(request, pk):
    practice = Practice.objects.get(id=pk)
    if not request.user.has_perm("practices.manage_practice", practice):
        raise PermissionDenied
    practice.contract_document.delete()
    messages.success(request, _("File successfully deleted."))

    return redirect('practices:accreditations', pk=pk)

class ContractDetail(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/contract/detail.html'
    context_object_name = 'practice'

class ContractEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    context_object_name = 'practice'
    form_class = forms.ContractForm
    template_name = 'practices/contract/forms/edit.html'

    def get_success_url(self):
        if self.get_form().has_changed():
            practice_contract_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        return reverse('practices:accreditations', kwargs={"pk": self.object.id})
