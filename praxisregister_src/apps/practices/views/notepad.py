from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView
from django.dispatch import Signal

from practices.models import Practice
from utils.view_mixins import ManagePracticePermissionRequired

practice_notepad_log=Signal()

class PracticeNotepadEdit(ManagePracticePermissionRequired, SuccessMessageMixin, UpdateView):
    model = Practice
    template_name = 'practices/notepad/forms/practice_notepad.html'
    success_message = _('Practice Notepad was updated.')
    fields = ['notepad']

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_notepad_log.send(sender=self.__class__, username=self.request.user.username, id=practice_id)
        return reverse_lazy('practices:edit-notepad', kwargs={'pk': practice_id})
