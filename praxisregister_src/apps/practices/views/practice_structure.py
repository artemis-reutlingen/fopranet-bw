from django import forms
from django.contrib import messages
from django.dispatch import Signal
from django.forms import modelformset_factory
from django.forms.models import BaseModelForm
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DeleteView, DetailView, UpdateView
from persons.models.additionals import AssistantAdditionalQualification
from persons.types import AssistantQualificationStatusType
from practices.forms.practice_structure_forms import (
    PracticeAdditionalFunctionsForm,
    PracticeAssistantsAdditionalQualificationForm,
    PracticeStructureGeneralForm, PracticeStructureServicesForm,
    PracticeStructureStaffEditForm, PracticeTreatmentByAgeGroupPerQuarterForm)
from practices.models import (DiseaseManagementProgramEntry, Practice,
                              PracticeEngagement, PracticeEngagementOption)
from practices.models.additional_qualification import \
    PracticeAssistantsAdditionalQualification
from practices.models.engagement import ServicesPracticeOffers
from practices.models.practice_structure import \
    PracticeTreatmentByAgeGroupPerQuarter
from practices.views.utils import get_save_or_save_and_continue_url
from utils.constants import PracticeType
from utils.view_mixins import ManagePracticePermissionRequired

practice_structure_data_edit_log=Signal()
practice_structure_data_add_delete_log=Signal()

class PracticeStructureDetail(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/practice_structure/practice_structure.html'
    context_object_name = 'practice'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        assistant_added_qualifications = AssistantAdditionalQualification.objects.filter(assistant_role__person__practice=self.object).exclude(status=AssistantQualificationStatusType.IN_TRAINING)
        quali_types = assistant_added_qualifications.values_list('type', flat=True).distinct()
        #Prefer showing the qualifications that are added directly to a person
        added_general = PracticeAssistantsAdditionalQualification.objects.filter(practice=self.object).exclude(type__in=quali_types)
        context["assistant_qualifications_in_general"] = added_general
        context["assistant_added_qualifications_to_person"] = assistant_added_qualifications
        return context


class PracticeStructureDoctorsEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/practice_structure/forms/practice_structure_generic.html'
    fields = [
        'structure_number_of_doctors_that_are_owners',
        'structure_number_of_doctors_that_are_employees',
        'structure_number_of_doctors_that_are_in_training',
        'structure_full_time_equivalent_for_doctors',
        'doctor_has_education_authorization',
        'structure_non_gp_doctors_are_working_in_the_practice',
        'structure_number_of_non_gp_doctors_that_are_in_the_practice',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = context['practice'].pk
        return context

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Doctors successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:structure-staff-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeStructureStaffEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/practice_structure/forms/practice_structure_generic.html'
    form_class = PracticeStructureStaffEditForm
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        practice_id = context['practice'].pk
        context['additional_form'] = PracticeAssistantsAdditionalQualificationForm(**{'practice_id' : practice_id})
        context['practice_id'] = practice_id
        return context

    def form_valid(self, form):
        self.object = form.save()
        addQualiForm = PracticeAssistantsAdditionalQualificationForm(self.request.POST, **{'practice_id': self.object.pk})
        if addQualiForm.is_valid() and addQualiForm.instance.type:
            if PracticeAssistantsAdditionalQualification.objects.filter(practice__id=self.object.id , type=addQualiForm.instance.type.strip()).exists():
                messages.error(self.request, _("This qualification already exists!"))
                return super().form_invalid(form)
            else:
                addQualiForm_instance = addQualiForm.save(commit=False)
                addQualiForm_instance.practice = self.object
                addQualiForm_instance.save()
        return super().form_valid(form)
    
    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Staff successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:it-pdms-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeStructureServicesEdit(ManagePracticePermissionRequired, UpdateView):

    model = Practice
    template_name = 'practices/practice_structure/forms/practice_structure_generic.html'
    form_class = PracticeStructureServicesForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = context['practice'].pk
        return context
    
    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            service_name = form.cleaned_data["service"]
            if service_name:
                ServicesPracticeOffers.objects.create(service_name=service_name, practice=form.instance)
        return super().form_valid(form)

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Range of services successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:structure-treatment-by-age-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeEngagementDelete(ManagePracticePermissionRequired, DeleteView):
    model = PracticeEngagement
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        practice_structure_data_add_delete_log.send(sender=self.__class__, username=self.request.user.username, type=self.object.type, id=self.object.practice.id)
        return reverse_lazy('practices:structure-detail', kwargs={'pk': self.get_object().practice.id})

class PracticeStructureGeneralEdit(ManagePracticePermissionRequired, UpdateView):
    
    model = Practice
    template_name = 'practices/practice_structure/forms/practice_structure_generic.html'
    form_class = PracticeStructureGeneralForm

    def get_form_kwargs(self):
        type_choices = [type_.value for type_ in PracticeType]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = type_choices
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        practice_id = context['practice'].pk
        context['additional_form'] = PracticeAdditionalFunctionsForm(instance=self.object, **{'practice_id': practice_id, 'functions_list': [option.name for option in PracticeEngagementOption.objects.all().order_by('name')]})
        context['practice_id'] = practice_id
        return context

    def form_valid(self, form):
        self.object = form.save()
        addFuncForm = PracticeAdditionalFunctionsForm(self.request.POST, **{'practice_id': self.object.pk ,'functions_list': [option.name for option in PracticeEngagementOption.objects.all().order_by('name')]})
        if addFuncForm.is_valid() and addFuncForm.cleaned_data["practice_role"]:
            addFuncForm.instance.type = addFuncForm.cleaned_data["practice_role"].strip()
            addFuncForm.instance = addFuncForm.save(commit=False)
            addFuncForm.instance.practice = self.object
            addFuncForm.instance.save()
        return super().form_valid(form)
    
    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Basic structure data successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:structure-services-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class DiseaseManagementProgramEntryEdit(ManagePracticePermissionRequired, UpdateView):
    """
    UpdateView using formset for editing all related DMPEntry objects for a practice at the same time.
    Probably room for improvement for idiomatic clean code / readability.
    """
    model = Practice
    form_class = forms.modelform_factory(model=Practice, fields=[])
    formset_class = modelformset_factory(DiseaseManagementProgramEntry, fields=('status',))
    template_name = 'practices/practice_structure/forms/practice_structure_services.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        dmp_entries = self.get_object().disease_management_programs.all()
        formset = self.formset_class(queryset=dmp_entries)

        dmp_entries_and_formset = zip(dmp_entries, formset)
        context['dmp_entries_and_formset'] = dmp_entries_and_formset
        context['management_form'] = formset.management_form
        context['practice_id'] = context['practice'].pk
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        qs = self.get_object().disease_management_programs.all()
        formsets = self.formset_class(self.request.POST, queryset=qs)

        if form.is_valid():
            for fs in formsets:
                if fs.is_valid():
                    fs.save()
            return self.form_valid(form)
        return self.form_invalid(form)
    
    def form_valid(self, form):
        #do not save the form, as the formset is already saved
        self.object = form.instance
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Disease Management Programs successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:structure-doctors-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeAssistantsAdditionalQualificationDelete(ManagePracticePermissionRequired, DeleteView):
    model = PracticeAssistantsAdditionalQualification
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        practice_structure_data_add_delete_log.send(sender=self.__class__, username=self.request.user.username, type=self.object.type, id=self.object.practice.id)
        return reverse_lazy('practices:structure-detail', kwargs={'pk': self.get_object().practice.id})
    
class ServicesPracticeOffersDelete(ManagePracticePermissionRequired, DeleteView):
    model = ServicesPracticeOffers
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        practice_structure_data_add_delete_log.send(sender=self.__class__, username=self.request.user.username, type=self.object.service_name, id=self.object.practice.id)
        return reverse_lazy('practices:structure-detail', kwargs={'pk': self.get_object().practice.id})
    
class PracticeTreatmentByAgeGroupPerQuarterEdit(ManagePracticePermissionRequired, UpdateView):

    model = Practice
    template_name = 'practices/practice_structure/forms/practice_structure_generic.html'
    form_class = PracticeTreatmentByAgeGroupPerQuarterForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = context['practice'].pk
        return context
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['practice'] = self.get_object()
        return kwargs

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            PracticeTreatmentByAgeGroupPerQuarter.objects.create(
                age_group = form.cleaned_data['age_group'],
                amount_of_threatments = form.cleaned_data['amount_of_threatments'],
                practice = self.get_object()
            )
        return super().form_valid(form)

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_structure_data_edit_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Treatment by age group per quarter successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:structure-detail', 'practices:dmp-entry-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeTreatmentByAgeGroupPerQuarterDelete(ManagePracticePermissionRequired, DeleteView):
    model = PracticeTreatmentByAgeGroupPerQuarter
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        practice_structure_data_add_delete_log.send(sender=self.__class__, username=self.request.user.username, type=self.object, id=self.object.practice.id)
        return reverse_lazy('practices:structure-detail', kwargs={'pk': self.get_object().practice.id})
