from rest_framework import serializers

from addresses.models import County
from institutes.models import Institute
from practices.models import Practice, Site


class InstituteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institute
        fields = ['full_name']


class CountySerializer(serializers.ModelSerializer):
    class Meta:
        model = County
        fields = ['full_name', 'federal_state']


class SiteSerializer(serializers.ModelSerializer):
    county = CountySerializer()

    class Meta:
        model = Site
        fields = ['identification_number', 'zip_code', 'city', 'county']


class PracticeSerializer(serializers.ModelSerializer):
    assigned_institute = InstituteSerializer()

    class Meta:
        model = Practice
        fields = ['id', 'name', 'assigned_institute']
