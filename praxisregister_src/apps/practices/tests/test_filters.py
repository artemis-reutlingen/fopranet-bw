from django.test import TestCase

from accreditation.models import Accreditation, AccreditationTypeChoices, AccreditationAttributeChoices
from institutes.models import Institute
from practices.filters import filter_practice_queryset_by_accreditation_type, filter_accreditation_criteria_by_completion
from practices.models import Practice


class PracticeFilterTest(TestCase):

    def setUp(self) -> None:
        institute = Institute.objects.create_institute_with_group(organisation="TestOrga", name="TestInstitute",
                                                                  group_name="test_orga_test_institute")

        # Create practice objects
        self.no_accreditation_practice = Practice.objects.create_practice_for_institute(practice_name="NoAcc",
                                                                                   institute=institute)
        self.rp_accreditation_practice = Practice.objects.create_practice_for_institute(practice_name="RP Acc",
                                                                                   institute=institute)
        self.rp_plus_accreditation_practice = Practice.objects.create_practice_for_institute(practice_name="RP Plus Acc",
                                                                                        institute=institute)
        # Add accreditations
        Accreditation.objects.create_accreditation_for_practice(
            practice=self.rp_accreditation_practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )

        Accreditation.objects.create_accreditation_for_practice(
            practice=self.rp_plus_accreditation_practice,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE_PLUS
        )
    
    def test_filter_practices_by_accreditation_types(self):
        queryset = Practice.objects.all()
        filtered = filter_practice_queryset_by_accreditation_type(
            queryset=queryset,
            accreditation_type=AccreditationTypeChoices.RESEARCH_PRACTICE
        )
        self.assertNotIn(self.no_accreditation_practice, filtered)
        self.assertNotIn(self.rp_plus_accreditation_practice, filtered)
        self.assertIn(self.rp_accreditation_practice, filtered)

    def test_filter_practices_by_accreditation_citeria_completion_zero_percent(self):
        queryset = Practice.objects.all()
        Accreditation.objects.create_accreditation_attribute_for_practice(self.rp_accreditation_practice, AccreditationTypeChoices.RESEARCH_PRACTICE, AccreditationAttributeChoices.HAS_FULL_CARE_MANDATE)
        filtered = filter_accreditation_criteria_by_completion(
            queryset=queryset,
            percentage='0.0'
        )
        self.assertEqual(filtered.count(), 1)
    
    def test_filter_practices_by_accreditation_citeria_completion_25_percent(self):
        Accreditation.objects.create_accreditation_attribute_for_practice(self.rp_accreditation_practice, AccreditationTypeChoices.RESEARCH_PRACTICE, AccreditationAttributeChoices.HAS_FULL_CARE_MANDATE)
        Accreditation.objects.create_accreditation_attribute_for_practice(self.rp_accreditation_practice, AccreditationTypeChoices.RESEARCH_PRACTICE, AccreditationAttributeChoices.HAS_AUTOMATED_BACKUPS)
        Accreditation.objects.create_accreditation_attribute_for_practice(self.rp_accreditation_practice, AccreditationTypeChoices.RESEARCH_PRACTICE, AccreditationAttributeChoices.HAS_SIGNED_CONTRACT)
        Accreditation.objects.create_accreditation_attribute_for_practice(self.rp_accreditation_practice, AccreditationTypeChoices.RESEARCH_PRACTICE, AccreditationAttributeChoices.IS_OPEN_FOR_DATA_EXPORT)
        self.rp_accreditation_practice.structure_full_care_mandate = "YES"
        self.rp_accreditation_practice.save()
        
        queryset = Practice.objects.all()

        filtered = filter_accreditation_criteria_by_completion(
            queryset=queryset,
            percentage='0.0'
        )
        self.assertEqual(filtered.count(), 0)

        filtered = filter_accreditation_criteria_by_completion(
            queryset=queryset,
            percentage='0.25'
        )
        self.assertEqual(filtered.count(), 1)




