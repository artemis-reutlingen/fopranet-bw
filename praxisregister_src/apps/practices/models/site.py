from django.db import models
from django.utils.translation import gettext_lazy as _
from practices.types import SiteType
from addresses.models import AddressMixin
from track_changes.models import TrackChangesMixin
from importer.models import ImportableMixin

class Site(TrackChangesMixin, ImportableMixin, AddressMixin, models.Model):
    """
    Site model which encapsulates address and identification number (BSNR) for practices.
    A single practice can have multiple sites.
    """
    practice = models.ForeignKey('practices.Practice', on_delete=models.CASCADE, null=False)
    
    identification_number = models.CharField(
        verbose_name=_('BSNR'),
        max_length=255,
        blank=True,
        default=""
    )
    type = models.CharField(
        verbose_name=_("Site type"),
        max_length=256,
        choices=SiteType.choices,
        default=SiteType.PRIMARY,
        blank=False,
    )
    
    class Meta:
        verbose_name = _("Site")

    def __str__(self):
        return f"{_('Site')}"

