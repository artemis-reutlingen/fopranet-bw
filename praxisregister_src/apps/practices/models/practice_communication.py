from django.db import models
from django.utils.translation import gettext_lazy as _
from practices.types import CommunicationWrittenWithColleaguesType

class PracticeCommunicationMixin(models.Model):

    communication_type_with_colleagues = models.CharField(
        verbose_name=_("Type of communication with colleagues"),
        max_length=256,
        choices=CommunicationWrittenWithColleaguesType.choices,
        default=CommunicationWrittenWithColleaguesType.UNKNOWN,
        )

    class Meta:
        abstract = True

class CommunicationSpokenLanguages(models.Model):

    com_spoken_language = models.CharField(verbose_name=_("Spoken language in practice"), max_length=128, blank=False, null=False)
    com_spoken_language_percentage = models.IntegerField(verbose_name=_("Percentage of spoken language in practice"), blank=True, null=True)
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.com_spoken_language
    
    class Meta:
        verbose_name = _("Spoken language in practice")
        verbose_name_plural = _("Spoken languages in practice")

class CommunicationInterpreter(models.Model):

    com_interpreter_language = models.CharField(verbose_name=_("Language for which an interpreter is used "), max_length=128, blank=False, null=False)
    com_number_of_patients = models.IntegerField(verbose_name=_("Number of patients"), blank=True, null=True)
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.com_interpreter_language
    
    class Meta:
        verbose_name = _("Interpreter language")
        verbose_name_plural = _("Interpreter languages")
    
class CommunicationDigitalWithColleagues(models.Model):

    com_type_of_communication = models.CharField(verbose_name=_("Type of communication"), max_length=256, blank=False, null=False)
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.com_type_of_communication
    
    class Meta:
        verbose_name = _("Digital communication with colleagues")
    
class CommunicationDigitalWithPatients(models.Model):

    com_type_of_communication = models.CharField(verbose_name=_("Type of communication"), max_length=256, blank=False, null=False)
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.com_type_of_communication
    
    verbose_name = _("Digital communication with patients")