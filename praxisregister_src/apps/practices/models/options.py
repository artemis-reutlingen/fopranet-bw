from django.db import models


class PracticeEngagementOption(models.Model):
    """
    Represents options for PracticeEngagement.type
    """
    name = models.CharField(max_length=512, blank=False, default="", unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class DiseaseManagementProgramOption(models.Model):
    """
    Represents options for DiseaseManagementProgramEntry.type
    """
    name = models.CharField(max_length=256, blank=False, default="", unique=True)
    sort_index = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort_index']


class PDMSSoftwareOption(models.Model):
    """
    Represents a single option for PDMS choices
    """
    name = models.CharField(max_length=256, blank=False, default="", unique=True)

    def __str__(self):
        return self.name


class OperatingSystemOption(models.Model):
    """
    Represents OS options for the many-to-many field on the practice/IT-infrastructure model.
    """
    name = models.CharField(max_length=256, blank=False, default="", unique=True)

    def __str__(self):
        return self.name
