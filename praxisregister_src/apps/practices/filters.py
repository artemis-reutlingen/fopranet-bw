from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column as FormCol, Submit, HTML
from django.forms import TextInput, Select
from django.utils.translation import gettext_lazy as _
from django_filters import CharFilter, ChoiceFilter, BooleanFilter, ModelChoiceFilter
from django.db.models import Q
from django.urls import reverse
from django_property_filter import PropertyFilterSet, PropertyCharFilter
from accreditation.models import AccreditationTypeChoices, Accreditation
from accreditation.types import AccreditationStatus
from practices.models import Practice, Site
from persons.models import options
from processes.models import ProcessDefinition
from addresses.models import County
from utils.fields import ListTextWidget



def filter_practice_queryset_by_accreditation_type(queryset, accreditation_type):
    """
    Filters a Practice queryset for objects that have related accreditation objects with the specified type.
    Separate function was created for isolated testing purposes, maybe moved into filter class eventually.
    """

    return queryset.filter(accreditations__accreditation_type__name__exact=accreditation_type)


def filter_practice_queryset_by_accreditation_status(queryset, accreditation_status):
    return queryset.filter(accreditations__status__exact=accreditation_status).distinct()


def filter_processStep_by_completion(queryset, processStep_pk, is_done):
    return queryset.filter(Q(process__step__step_definition=processStep_pk) & Q(process__step__is_done=is_done))


def filter_accreditation_criteria_by_completion(queryset, percentage):
    accreditations = Accreditation.objects.all()
    filtered_acc = []
    if accreditations.count() > 0:
        for acc in accreditations:
            criterias = acc.accreditation_type.accreditationattribute_set.all()
            if len(criterias) > 0:
                fullfilled_percentage = len(
                    [criteria for criteria in criterias if acc.accreditation_criteria_dict.get(criteria.name) == True or acc.accreditation_criteria_dict.get(criteria.name) == "YES" or acc.accreditation_criteria_dict.get(criteria.name) == "10 / 10"])/len(criterias)
                # The added value must correspond to the interval between the selection
                if fullfilled_percentage >= float(percentage) and fullfilled_percentage <= float(percentage) + 0.24:
                    filtered_acc.append(acc.pk)
    filtered_queryset = queryset.filter(
        accreditations__pk__in=filtered_acc).distinct()
    return filtered_queryset


class PracticeFilter(PropertyFilterSet):
    def __init__(self, request, *args, **kwargs):
        super().__init__(request.GET, *args, **kwargs)
        self.form.fields['process__process_definition'].widget.choices.extend([(choice.pk, choice.name) for choice in ProcessDefinition.objects.get_process_definitions_for_user(request.user).filter(is_draft=False)])
        self.form.fields['process__process_definition'].widget.attrs = {
                      'hx-get': reverse('practices:process-step-selection') , 'hx-trigger': 'change', 'hx-target': '#process_filters'}
        self.form.helper = PracticeFilterFormHelper()

    class Meta:
        model = Practice
        fields = ('name',
                  'owners_names_formatted',
                  'city',
                  'county__full_name',
                  'accreditation_type',
                  'accreditation_status',
                  'accreditation_critera_fullfilled',
                  'is_contract_signed',
                  'process__process_definition',
                  'process__step__step_definition__name',
                  'processStep_not_fullfilled',
                  'person__study_specific_trainings__type',
                  'is_active',
                  'tags__name',
                  'contact_method',
                  )

    yes_no_choice = [(None, "---------"),
                     (True, _('Yes')),
                     (False, _('No'))]
    
    contact_methods = [
        ("", "---------"),
        ("phone", _("Phone")),
        ("fax", _("Fax")),
        ("email", _("Email")),
        ("website", _("Website")),
    ]

    name = CharFilter(
        lookup_expr='icontains',
        label=_('Practice name'),
        widget=TextInput()
    )

    owners_names_formatted = PropertyCharFilter(
        lookup_expr='icontains',
        label=_('Owners'),
        field_name='owners_names_formatted',
    )

    city = CharFilter(
        lookup_expr='icontains',
        label=_('City'),
        widget=ListTextWidget(data_list=Site.objects.values_list('city', flat=True).distinct(), name="cities-list")

    )
    county__full_name = CharFilter(
        lookup_expr='icontains',
        label=_('County'),
        widget=ListTextWidget(data_list=County.objects.values_list('full_name', flat=True).distinct(), name="counties-list")
    )

    accreditation_type = ChoiceFilter(
        method='accreditation_type_filter',
        label=_('Accreditation Type'),
        choices=AccreditationTypeChoices.choices,
        widget=Select()
    )

    accreditation_status = ChoiceFilter(
        method='accreditation_status_filter',
        label=_('Accreditation status'),
        choices=AccreditationStatus.choices,
        widget=Select()
    )

    is_contract_signed = BooleanFilter(
        widget=Select(choices=yes_no_choice)
    )

    process__process_definition = CharFilter(
        label=_('Process'),
        widget=Select(choices=[(None, "---------")])
    )

    process__step__step_definition__name = CharFilter(
        method='processStep_fullfilled_filter',
        label=_('Process step fullfilled'),
        widget=Select(choices=[(None, "---------")])
    )

    processStep_not_fullfilled = CharFilter(
        method='processStep_not_fullfilled_filter',
        label=_('Process step not fullfilled'),
        widget=Select(choices=[(None, "---------")])
    )

    accreditation_critera_fullfilled = CharFilter(
        method='accreditation_criteria_fullfilled_filter',
        label=_('Accreditation criterias fullfilled'),
        # If the percentages of the selection are adjusted, the filter must be adjusted as well
        widget=Select(choices=[(None, "---------"),
                               (0.0, '0-24%'),
                               (0.25, '25-49%'),
                               (0.5, '50-74%'),
                               (0.75, '75-99%'),
                               (1.0, '100%'),
                               ])
    )

    person__study_specific_trainings__type = ModelChoiceFilter(
        label=_('Study-specific Trainings'),
        queryset=options.StudySpecificTrainingOption.objects.all(),
        distinct=True
    )

    is_active = BooleanFilter(
        widget=Select(choices=yes_no_choice)
    )

    tags__name = CharFilter(
        lookup_expr='iexact',
        label=_('Tags'),
        widget=TextInput(),
        distinct=True
    )

    contact_method = CharFilter(
        widget = Select(choices = contact_methods),
        label = _("Available contact method"),
        method = "contact_method_filter"
    )

    @staticmethod
    def accreditation_criteria_fullfilled_filter(queryset, name, value):
        return filter_accreditation_criteria_by_completion(
            queryset=queryset,
            percentage=value
        )

    @staticmethod
    def processStep_fullfilled_filter(queryset, name, value):
        return filter_processStep_by_completion(
            queryset=queryset,
            processStep_pk=value,
            is_done=True
        )

    @staticmethod
    def processStep_not_fullfilled_filter(queryset, name, value):
        return filter_processStep_by_completion(
            queryset=queryset,
            processStep_pk=value,
            is_done=False
        )

    @staticmethod
    def accreditation_status_filter(queryset, name, value):
        return filter_practice_queryset_by_accreditation_status(
            queryset=queryset,
            accreditation_status=value
        )

    @staticmethod
    def accreditation_type_filter(queryset, name, value):
        return filter_practice_queryset_by_accreditation_type(
            queryset=queryset,
            accreditation_type=value
        )
    
    @staticmethod
    def contact_method_filter(queryset, name, value):
        match value:
            case "phone":
                # Excludes all practices that have no value for contact_phone AND contact_phone_alt.
                # We need to check for NULL and "" separately, hence the complicated syntax.
                return queryset.exclude(
                    (Q(contact_phone__isnull = True) | Q(contact_phone__exact = "")) &
                    (Q(contact_phone_alt__isnull = True) | Q(contact_phone_alt__exact = ""))
                )
            case "fax":
                return queryset.filter(contact_fax__isnull = False).exclude(contact_fax__exact = "")
            case "email":
                return queryset.exclude(
                    (Q(contact_email__isnull = True) | Q(contact_email__exact = "")) &
                    (Q(contact_email_alt__isnull = True) | Q(contact_email_alt__exact = ""))
                )
            case "website":
                return queryset.filter(contact_website__isnull = False).exclude(contact_website__exact = "")
            case _:
                return queryset


class PracticeFilterFormHelper(FormHelper):
    form_tag = True
    form_method = 'GET'
    #Layout uses HTML and htmx elements, which are used in the view and the template 
    layout = Layout(
        HTML("""<div class="d-flex gap-2 mb-2">"""),
        HTML(f"""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#filter_base_data" aria-expanded="false" aria-controls="filter_base_data">{_('Base data')}</button>"""),
        HTML(f"""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#filter_accreditation" aria-expanded="false" aria-controls="filter_accreditation">{_('Accreditation')}</button>"""),
        HTML(f"""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#filter_processes" aria-expanded="false" aria-controls="filter_processes">{_('Processes')}</button>"""),
        HTML(f"""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#filter_others" aria-expanded="false" aria-controls="filter_others">{_('Others')}</button>"""),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse" id="filter_base_data">"""),
        Row(
            FormCol('name'),
            FormCol('owners_names_formatted'),
            FormCol('city'),
            FormCol('county__full_name')
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse" id="filter_accreditation">"""),
        Row(
            FormCol('accreditation_type'),
            FormCol('accreditation_status'),
            FormCol('accreditation_critera_fullfilled'),
            FormCol('is_contract_signed'),
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse" id="filter_processes">"""),
        Row(
            FormCol('process__process_definition'),
            HTML("""<span id='process_filters' class='col-md'>"""),
            Row(
                FormCol('process__step__step_definition__name'),
                FormCol('processStep_not_fullfilled'),
            ),
            HTML("""</span>"""),
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse" id="filter_others">"""),
        Row(
            FormCol('tags__name'),
            FormCol('person__study_specific_trainings__type'),
            FormCol('is_active'),
            FormCol('contact_method'),
        ),
        HTML("""</div>"""),
        Row(
            FormCol(Submit('submit', value=_('Apply Filter'))),
        )
    )



class SmallPracticeFilter(PropertyFilterSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form.helper = SmallPracticeFilterFormHelper()

    name = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('Practice name')})
    )

    identification_number = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('BSNR')})
    )
    owners_names_formatted = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('Owner')})
    )
    city = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('City')})
    )

    county__full_name = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('County')})
    )

    class Meta:
        model = Practice
        fields = ('name', 'identification_number', 'owners_names_formatted',
                  'city', 'county__full_name')


class SmallPracticeFilterFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Row(
            FormCol('name'),
            FormCol('identification_number'),
            FormCol('owners_names_formatted'),
            FormCol('city'),
            FormCol('county__full_name'),
            FormCol(Submit('submit', value=_('Apply Filter')))
        )
    )
