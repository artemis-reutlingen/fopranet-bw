from django.db import migrations

def transfer_federal_state_from_county_to_practice(apps, schema_editor):
    Practice = apps.get_model('practices', 'practice')
    County = apps.get_model('addresses', 'county')
    FederalState = apps.get_model('addresses', 'federalstate')
    practices = []
    for practice in Practice.objects.all():
        if practice.county is not None:
            county = County.objects.get(pk=practice.county.pk)
            federal_state = FederalState.objects.get(pk=county.federal_state.pk)
            practice.federal_state = federal_state
            practices.append(practice)
    Practice.objects.bulk_update(practices, ['federal_state'])

    Site = apps.get_model('practices', 'site')
    sites = []
    for site in Site.objects.all():
        if site.county is not None:
            county = County.objects.get(pk=site.county.pk)
            federal_state = FederalState.objects.get(pk=county.federal_state.pk)
            site.federal_state = federal_state
            sites.append(site)
    Site.objects.bulk_update(sites, ['federal_state'])

def reverse_transfer_federal_state_from_county_to_practice(apps, schema_editor):
    Practice = apps.get_model('practices', 'practice')
    County = apps.get_model('addresses', 'county')
    FederalState = apps.get_model('addresses', 'federalstate')

    counties = []
    for practice in Practice.objects.all():
        if practice.federal_state is not None:
            federal_state = FederalState.objects.get(pk=practice.federal_state.pk)
            if practice.county is not None:
                practice.county.federal_state = federal_state
                counties.append(practice.county)
            if practice.site_set.count() > 0:
                for site in practice.site_set.all():
                    if site.county is not None:
                        site.county.federal_state = federal_state
                        counties.append(site.county)
    County.objects.bulk_update(counties, ['federal_state'])

class Migration(migrations.Migration):

    dependencies = [
        ('practices', '0059_practice_federal_state_site_federal_state'),
    ]

    operations = [
        migrations.RunPython(transfer_federal_state_from_county_to_practice, reverse_code=reverse_transfer_federal_state_from_county_to_practice),
    ]
