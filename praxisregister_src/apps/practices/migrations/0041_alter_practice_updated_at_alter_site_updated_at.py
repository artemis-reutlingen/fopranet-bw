# Generated by Django 4.1.7 on 2023-07-18 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('practices', '0040_remove_practice_data_collection_time_site_created_at_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='practice',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
