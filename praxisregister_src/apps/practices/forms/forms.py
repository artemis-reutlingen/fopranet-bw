from django import forms
from django.utils.translation import gettext_lazy as _
from institutes.models import Institute
from practices.models import Practice, Site
from utils.fields import ListTextWidget
from addresses.models import County, FederalState

class SiteForm(forms.ModelForm):

    federal_state = forms.ModelChoiceField(label=_('Federal state'), queryset=FederalState.objects.all())
    county_name = forms.CharField(required=False, label=_('County'), widget=ListTextWidget(data_list=County.objects.values_list("full_name", flat=True).distinct(), name='county-choices'))

    def __init__(self, *args, **kwargs):
        county = kwargs.pop('county', None)
        super().__init__(*args, **kwargs)
        if county:
            self.fields['county_name'].initial = county.full_name

    class Meta:
        model = Site
        fields = [
            'identification_number',
            'street',
            'house_number',
            'zip_code',
            'city',
            'county_name',
            'federal_state',
            'type'
        ]

class ContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['contract_document'].required = False

    class Meta:
        model = Practice
        fields = [
            'is_contract_signed',
            'contract_signed_date',
            'contract_document',
        ]
        widgets = {
            'contract_signed_date': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'contract_document' : forms.FileInput()
        }

    def clean(self):
        cleaned_data = super().clean()
        contract_signed = cleaned_data.get("is_contract_signed")
        contract_signed_date = cleaned_data.get("contract_signed_date")
        contract_document = cleaned_data.get("contract_document")
        #A signed contract always should have a signed date
        if (contract_signed and not contract_signed_date) or (not contract_signed and contract_signed_date):
            msg = _("The entry of the contract signature and the date must be filled in together")
            self.add_error("contract_signed_date", msg)
        #document gets removed -> also clear other form inputs
        if self.initial and self.initial["contract_document"] and not contract_document:
            cleaned_data["is_contract_signed"] = False
            cleaned_data["contract_signed_date"] = None

class AllowedInstitutesForm(forms.ModelForm):
    allowed_institutes = forms.ModelMultipleChoiceField(
        queryset=Institute.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False)

    def __init__(self, *args, **kwargs):
        # custom init, in order to filter institute queryset (assigend institute of currect practice is excluded)
        institutes = kwargs.pop('institutes')
        super().__init__(*args, **kwargs)
        self.fields['allowed_institutes'].queryset = institutes

    class Meta:
        model = Practice
        fields = [
            'allowed_institutes',
        ]

class PracticeSelectBaseForm(forms.Form):
    practices = forms.ModelMultipleChoiceField(
        widget=forms.MultipleHiddenInput(attrs={'disabled': "true"}),
        label="practices",
        queryset=Practice.objects.all(),  # gets "overwritten" in view by setting initial on Form object
    )